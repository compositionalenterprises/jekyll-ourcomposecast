---
title: OurComposeCast
feature_text: |
  ## OurComposeCast
  _Open Source Productivity in the real world_
feature_image: "https://wallpaperplay.com/walls/full/4/5/1/13886.jpg"
layout: page
aside: True
---

Free, Libre, and Open Source Software has long been stigmatized as a never-ending series of one compromise after another. We're here to change that perspective.

From server tech to workflow methodology, and desktop applications to budgeting, we share our own tips and tricks on how to make it in the modern world without compromising _anything_.
