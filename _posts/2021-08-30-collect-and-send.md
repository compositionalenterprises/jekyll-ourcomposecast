---
title: Collect and Send
service: Bitwarden
tagline: With the help of a rational bit of Scrumban, OurCompose gets new features that even high-budget startups are looking for, like the dashboard, and it's own Ansible collection!
chapters:
  - start: '00:00:49'
    title: 'Introduction'
  - start: '00:23:19'
    title: 'News/Community Updates'
  - start: '00:34:54'
    title: 'OurCompose Developments'
  - start: '00:40:51'
    title: 'Integration Discussion - Bitwarden - Folders, Organizations, and Send'
  - start: '01:03:54'
    title: 'Grab Bag - What is Scrumban'
---

## Intro

- [These People Who Work From Home Have a Secret: They Have Two Jobs - WSJ](https://www.wsj.com/articles/these-people-who-work-from-home-have-a-secret-they-have-two-jobs-11628866529?mod=djemalertNEWS)
- [1Password 8 will be subscription only and won’t support local vaults](https://news.ycombinator.com/item?id=28145247)
- [Why is it so hard to be Rational](https://www.newyorker.com/magazine/2021/08/23/why-is-it-so-hard-to-be-rational)
- [Congrats on your $2m seed round...](https://twitter.com/anothercohen/status/1427753293900500997)

## News / Community Updates

- [FireflyIII Moving off Reddit?](https://www.reddit.com/r/FireflyIII/comments/p7mexf/reddit_alternative/)
- [Rundeck 3.4.3](https://docs.rundeck.com/docs/history/3_4_x/version-3.4.3.html#security-fixes)

## OurCompose Developments

- [Portal Dashboard](https://gitlab.com/compositionalenterprises/portal/-/tags/1.9.2)
- [OurCompose Collection 3.0](https://gitlab.com/compositionalenterprises/ansible-collection-compositionalenterprises.ourcompose)
    - [Docker Hub](https://hub.docker.com/r/compositionalenterprises/commands_receivable)
    - [Ansible Galaxy](https://galaxy.ansible.com/compositionalenterprises/ourcompose)

## Integration Discussion - Bitwarden - Folders, Organizations, and Send

- [Folders and Search](https://compositionalenterprises.ourcompose.com/bookstack/books/bitwarden/page/folders-and-search)
- [Organizations](https://compositionalenterprises.ourcompose.com/bookstack/books/bitwarden/page/organizations-and-collections)
- [Send](https://compositionalenterprises.ourcompose.com/bookstack/books/bitwarden/page/send)

## Grab Bag - What is Scrumban?

> Scrumban is a hybrid of Scrum and Kanban.
> It combines the project management and product delivery focus of Scrum with the pull systems, workflow visualization, and process improvement of Kanban

> The term "Scrumban" was originally coined by Corey Ladas in his 2008 paper, Scrum-ban, and expanded upon in his 2009 book _Scrumban_.
> The original goal of this methodology was to use Kanban to improve Scrum.

#### Problems needed to solved

- The rules of Scrum are not always enough to help [team members] get past their particular challenges
- Too many new ideas
    - The "pre-backlog" keeps growing without resolution
- Product owners appeared to be be performing insufficiently well
    - Frequently were being rotated, delegating/splitting responsibility, or being replaced by committees.

### Recap

- Scrum is a product development framework
    - Roles:
        - Product Owner
        - Scrum Master
        - Team Member
    - Scrum story format:
        - _"As a <type of user> I want to <specific action I'm taking> so that <What I want to happen as a result>."_
    - Time-Boxed Iterations
- Kanban is a method for changing and improving a team's process
    - Start with what you're doing now
        - Evolve using small incremental changes based on experimentation and data
    - Set WIP limits for each stage in the process
        - Detect when the workflow is overloaded and make adjustments to address the constraints that are the root cause of the overload.
    - Establish a pull system based on kanban signals
        - Teams self-organize around available cycles and self-assign tasks
        - _"Just as an unregulated index card on a cork board is not a kanban, time-boxed iteration planning is not pull"_

### What actually changes?

> Team members use Kanban utilize _systems thinking_ to analyze and understand their workflows.

#### Optimize Flow

> Kanban's visualization tools provide transparency that exposes "bottlenecks, queues, variability and waste"

Metrics can expose where the system is overloaded but the team has not discovered the source of the problem.

- Lead Time
    - Concept to Cash
- Cycle Time
    - Hands on keyboard
- WIP Limits
    - Task Count
    - Complexity
    - Story Points
    - Time Tracking
- Throughput
    - Improvement over time
    - Performance Reality
    - Stability of workflow
    - Predictable Process

If the product owner can show the stakeholders that this new rule could lead to faster development, and lower lead times, stakeholders will be happy to accept their decisions.
Alternatively, if lead times do NOT decrease, the experiment could be perceived as a hindrance rather than a boon.
Remember, these metrics are only useful if they translate into a perceived faster development turnaround.

#### Better Backlog Management

A perfect example of an ever-growing product backlog issue can be found in: [Agile Product Ownership in a Nutshell](https://www.youtube.com/watch?v=502ILHjX9EE)

A very easy fix for a feeling of being overloaded and facing increasing pressure to multitask and cut corners, is to visualize where the work is piling up.
Adding a pre-backlog "Feature Requests" column will expose that more feature requests are coming in than can be handled by the team.
This is a problem that can be addressed at the Product Owner level - saying "no" to additional work to meet the constraints of the new column.
Adjusting the WIP limit up or down based on experience will help to find the optimal limit that leads to the lowest average lead time per story.
A "Rejected" column is optional if it helps communicate to stakeholders better when it's visualized.

This is also known as "Level 2 Scrumban"

> Once you've broken up the timebox, you can start to get leaner about the construction of the backlog.
> Agility implies an ability to respond to demand.
> The backlog should reflect the current understanding of business circumstances as often as possible.
> In other words, the backlog should be event-driven.
> Timeboxed backlog planning is just that, where the event is a timer, and once we see it that way, we can imagine other sorts of events that allow us to respond more quickly to emerging priorities.
> Since our system already demonstrates pull and flow, that increased responsiveness should come at no cost to our current efficiency.

### Common Misconceptions About Scrumban

#### Scrumban is simply a tool to organize work and make it more efficient

> Scrum is focused on project management and product delivery; Kanban is focused on process improvement

If Scrumban is adopted because a team is overloaded, it is likely to succeed in changing the way [the team] works, but unlikely to fix the problems that caused the overload.

> After the evolution stops, [the team] is no longer following Kanban or any other method for improvement.

#### Scrumban is essentially "Iteration-less" Scrum

> Scrum teams are attracted to the idea of Scrumban because it provides a convenient excuse to remove the "hard" part.
> Iteration is not just a way to break projects up into phases; it is a tool for accountability, and that accountability can be uncomfortable.
> This is one of the most important features of Scrum because it helps teams make decisions based on experience and actual, known facts from their projects.

Things that need to happen with or without iterations:
- Team demonstrates working software
- Stakeholders refine requirements
- Team reviews process obstacles
- Team agrees on process improvements
- Team agrees on what work to prioritize

### How Scrumban teams can move beyond their misconceptions

#### How to replace iterations

An effective Scrumban implementation must include practices that provide the empirical process control benefits of iterations.

> _INCLUDE PRACTICES THAT PROVIDE THE EMPIRICAL PROCESS CONTROL BENEFITS OF ITERATIONS_

- Planning
- Predictability
- Accountability
- Ceremonies for Reflection

"Cadences" are triggered by milestones or external events rather than a time-boxed iteration.
Each work item can be considered part of the reflection process, and no work item is considered complete until it's been included in a retrospective.

#### How to implement process improvement

- Step 1
    - Start with what you do now
    - Agree to pursue incremental, evolutionary change
    - Initially, respect current roles, responsibilities, and job titles
- Step 2
    - Visualize the Workflow
    - Limit WIP
    - Manage Flow
    - Make Process Policies Explicit
    - Implement Feedback Loops
    - Improve collaboratively, evolve experimentally
