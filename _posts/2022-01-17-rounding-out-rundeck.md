---
title: Rounding Out Rundeck
service: Rundeck
tagline: This week we wrap up Rundeck, discuss maintenance and burnout, and wrap up with Goals, what they look like setting them and following through with them.
chapters:
  - start: '00:00:00'
    title: 'Intro - News - Updates'
  - start: '00:32:24'
    title: 'Integration Discussion - Rundeck - CLI and FAQ'
  - start: '00:54:40'
    title: 'Grab Bag - Goals, What they look like, Setting Them, Following Through with Them'
---

## Intro

- [Burden of the Open Source maintainer](https://www.jeffgeerling.com/blog/2022/burden-open-source-maintainer)
- [4 Day Workweek?](https://www.bolt.com/blog/switching-four-day-workweek/#)
- [LastPass confirms credential stuffing attack against some of its users](https://therecord.media/lastpass-confirms-credential-stuffing-attack-against-some-of-its-users/)

## News / Community Updates

- [Dolibarr - 2021 Retrospective](https://www.dolibarr.org/2021-retrospective-dolibarr.php)
- [Dolibarr ERP CRM v14.0.5 Maintenance Branch](https://www.dolibarr.org/dolibarr-erp-crm-v1405-maintenance-release-for-branch-140-is-available.php)
- [Firefly-III 5.6.10](https://github.com/firefly-iii/firefly-iii/releases/5.6.10)

## OurCompose Developments

### Instance Features
- Migrate data from local to remote
  - Include_role issue
- Application View Status

### Service Resiliancy
- Remove minio auth password after using
- Add single volume without inputting droplet ID
- Block Volume maintenance

### Engagement

### Administrative

## Integration Discussion - [Rundeck - CLI](https://compositionalenterprises.ourcompose.com/bookstack/books/rundeck-d07/page/cli)

### FAQ
- Why do you call this an Automation Frontend?
  - Not an orchestration tool
  - Replaces butts in seats
  - C&C was already taken
  - Accepts network requests as well communicates outbound
- Don't you use just a small subset of Rundeck's functionality?
  - YES!
  - Plugins
  - Nodes
  - Execute Jobs Within Jobs
- What were the alternatives that you considered?
  - [Ansible Tower/AWX](https://www.ansible.com/community/awx-project)
  - [Jenkins](https://www.jenkins.io)

## Grab Bag - Goals, What they look like, Setting Them, Following Through with Them

### What is goal setting?

Some people may have trouble sticking to goals because they don’t distinguish their goals from more casual, everyday self-improvement efforts. Just because you decide to start running every day doesn’t necessarily make that a conscious goal. So let’s revisit what goal setting means.

Goal setting is a purposeful and explicit process that starts with identifying a new objective, skill, or project you want to achieve. Then, you make a plan for achieving it, and you work to complete it.

### [SMART Goals](https://www.lucidchart.com/blog/setting-smart-goals)

- Specific
- Measurable
- Attainable
- Realistic
- Time-bound


### [Where?](https://www.mindtools.com/page6.html)

Lifetime Goals and Areas:
- Career
- Financial
- Education
- Family
- Artistic
- Attitude
- Physical
- Pleasure
- Public Service

