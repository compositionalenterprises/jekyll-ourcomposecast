---
title: Neapolitan Board Views
service: Kanboard
tagline: Andrew and Jack are back at it talking about public companies, how to make ChatOps work, and even more Kanboard customization.
chapters:
  - start: '00:00:00'
    title: 'Introduction'
  - start: '00:07:30'
    title: 'News/Community Updates'
  - start: '00:25:00'
    title: 'OurCompose Developments'
  - start: '00:36:10'
    title: 'Integration Discussion - Kanboard - Dashboard/Board View Customization'
  - start: '00:54:55'
    title: 'Grab Bag - ChatOps'
---

## Intro

### [Common Anti-Patterns in Python](https://deepsource.io/blog/8-new-python-antipatterns/)

### [If you can't get an SM7b to sound great...](https://www.reddit.com/r/audioengineering/comments/l4ukov/if_you_cant_get_an_sm7b_to_sound_great/)

## News / Community Updates

### [PagerDuty Intro to Rundeck](https://university.pagerduty.com/intro-to-rundeck)

### [Pip Drops Support for Python 2, 3.5](https://pip.pypa.io/en/stable/news/)

### [GitLab, Valued at $6B, Eyes Public Listing](https://www.thetechee.com/2021/01/gitlab-valued-at-6b-eyes-public-listing.html)

### [Bitcoincore.org removes bitcoin.pdf from their homepage](https://github.com/bitcoin-core/bitcoincore.org/pull/740)

- [Bitcoin Websites Asked to Remove White Paper After Craig Wright Claims Copyright Infringement](https://news.bitcoin.com/bitcoin-websites-asked-to-remove-white-paper-after-craig-wright-claims-copyright-infringement/)

## OurCompose Developments

## [Integration Discussion - Kanboard - Dashboard/Board View Customization](https://compositionalenterprises.ourcompose.com/bookstack/books/kanboard/page/dashboardboard-view-customization)

## Grab Bag - ChatOps

What is Chatops?
- ChatOps is a collaboration model that connects people, tools, process, and automation into a transparent workflow.
- Its not a product or a tool, it's a way of working.

Why Chatops?
- "IM enabled Devops"
- You can do any type of work inside the chat, including server deployment, maintenance tasks, and simple reboots. As long as the API of a platform is available, ChatOps services will allow the functionality of the desired application.
- All the necessary tools are at their fingertips and they can now be operated from within the chat windows using bots and other commands instead of opening a dedicated application window or console.

### [Incident Management Toolchain](https://www.ibm.com/cloud/architecture/architectures/incidentManagementDomain/overview)

ChatOps doesn’t replace the available tool, it simply makes them more accessible to everyone involved. The benefit of ChatOps comes by reducing the context switches between the tools and allowing you to work with one central tool. Why open a browser when you can pull the dashboard into the chatroom? Why be alerted by (yet another) e-mail when you can join the incident chatroom?

- [Better Living Through Chatops](https://medium.com/ibm-garage/better-living-through-chatops-df66872893e7)
- [PagerDuty - What is Chatops](https://www.pagerduty.com/blog/what-is-chatops/)

### Integrations

If you have an interest in getting chatops setup

- [Gitlab](https://docs.gitlab.com/ee/user/project/integrations/)
- [Matrix](https://matrix.org/)
