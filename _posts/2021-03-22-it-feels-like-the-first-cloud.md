---
title: It Feels Like the First Cloud
service: Nextcloud
tagline: Andrew learns that the V in MVC does not stand for "Vanilla", and that fires can melt both ice cream and datacenters, but not if you've got backups!
chapters:
  - start: '00:00:00'
    title: 'Introduction'
  - start: '00:19:55'
    title: 'OurCompose Developments'
  - start: '00:29:21'
    title: 'Integration Discussion - Nextcloud - Built-In Apps and Settings'
  - start: '01:04:43'
    title: 'Grab Bag - Rails and MVC Frameworks'
---

## Intro

### [OVH data center burns down knocking major sites offline](https://www.bleepingcomputer.com/news/technology/ovh-data-center-burns-down-knocking-major-sites-offline/)
- [OVH founder says UPS fixed up day before blaze is early suspect as source of data centre destruction](https://www.theregister.com/2021/03/12/ovh_restoration_roadmap/)

### [Ever wondered why the big beasts in software all suddenly slapped an 'I heart open-source' badge on?](https://www.theregister.com/2021/03/04/red_hat_eos/)

## OurCompose Developments

### [Compositional v2.6.4 Published](https://gitlab.com/compositionalenterprises/role-compositional/-/commit/264b8977ddec0e2df223be3806da19c57c8a943c)
- Bitwarden 1.18
- Nextcloud 20
- Pipefail for quicker failures
- Disable Maintenance Mode for Nextcloud after migrations

### [Reproduce DPKG Errors](https://andrewcz.com/jekyll/2021/reproduce-dpkg-errors/)

## [Integration Discussion - Nextcloud - Built-In Apps and Settings](https://compositionalenterprises.ourcompose.com/bookstack/books/nextcloud/chapter/initial-configuration)

## Grab Bag - Rails and MVC Frameworks

### Model View Controller
1) User interacts with a view
2) View alerts the controller of an event
3) Controller updates the model
4) Model alerts view that it has changed
5) View grabs the model data and updates itself

Model: includes all the data and related logic
View: Presents the data to the user and handles interaction
Controller: An interface between Model and View Components

### Ruby on Rails

[Rails is a web application development framework written in the Ruby programming language.](https://guides.rubyonrails.org/getting_started.html#what-is-rails-questionmark)

[Rails Doctrine](https://rubyonrails.org/doctrine/)
- Optimize for programmer happiness
- Convention over Configuration
- Don't repeat yourself
- Value Integrated systems
  > Rails can be used in many contexts, but its first love is the making of integrated systems: Majestic monoliths! A whole system that addresses an entire problem. This means Rails is concerned with everything from the front-end JavaScript needed to make live updates to how the database is migrated from one version to another in production.

[Rails APIs](https://guides.rubyonrails.org/api_app.html#why-use-rails-for-json-apis-questionmark)
- For very simple APIs, this may be true. However, even in very HTML-heavy applications, most of an application's logic lives outside of the view layer.
