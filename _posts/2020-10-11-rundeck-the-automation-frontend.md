---
title: Rundeck, The Automation Frontend
service: Rundeck
tagline: After unearthing a hidden gem about last episode's Firefly-III topic, Andrew gushes about their Rundeck implementation, and Jack goes into what DevOps _really_ means.
chapters:
  - start: '00:00:00'
    title: 'Introduction'
  - start: '00:08:40'
    title: 'News/Community Updates'
  - start: '00:19:55'
    title: 'OurCompose Developments'
  - start: '00:21:28'
    title: 'Integration Discussion - Rundeck - Overview'
  - start: '00:50:03'
    title: 'Grab Bag - DevOps'
---

## Intro

### [Firefly III password security](https://docs.firefly-iii.org/about-firefly-iii/security)

> When registering, Firefly III uses the Have I Been Pwnd password API (v3) to verify if you're using a secure password. You can disable this when registering an account.

## News / Community Updates

### [Welcoming Gitter to Matrix](https://matrix.org/blog/2020/09/30/welcoming-gitter-to-matrix)

![Why SMS Refuses to die](https://imgs.xkcd.com/comics/messaging_systems.png)

### [Todoist takes a shot a Trello with Kanban-style 'boards'](https://thenextweb.com/plugged/2020/09/23/todoist-takes-a-shot-at-trello-with-kanban-style-boards/)

### Ohio LinuxFest

> If you haven’t heard yet, OLF is a go for 2020 and we’ll be virtual! In order to keep everyone updated, here are a few points and links:
>
> - Save the dates for November 5th - 7th. Our content has not yet been solidified, but these are the dates we are targeting.
>
> - Call for Presentations is open! If you have a talk you’d like to provide, please submit by October 10th. [Link](https://ohiolinux.org/2020-call-for-presentations-is-open/)
>
> - Registration for OLF 2020 will be opened on Friday 10/2. Please watch your email and social media for more information!
>
> - OLF Institute programming is actively being worked on. We will update everyone as the content is being added.
>
> Subscribe to the [ohiolinux-attendees mailing list](https://pairlist3.pair.net/mailman/listinfo/ohiolinux-attendees)

### [Podcasts on IPFS](https://pod.casts.io/)

## OurCompose Developments

- [Nginx Hardening](https://gitlab.com/compositionalenterprises/role-compositional/-/merge_requests/45)

## [Integration Discussion - Rundeck - Overview](https://compositionalenterprises.ourcompose.com/bookstack/books/rundeck-d07)

## Grab Bag - DevOps

> DevOps represents a change in IT culture, focusing on rapid IT service delivery through the adoption of agile, lean practices in the context of a system-oriented approach. DevOps emphasizes people (and culture), and it seeks to improve collaboration between operations and development teams. DevOps implementations utilize technology — especially automation tools that can leverage an increasingly programmable and dynamic infrastructure from a life cycle perspective.
> -[Gartner](https://www.gartner.com/en/information-technology/glossary/devops)

> DevOps is the practice of operations and development engineers participating together in the entire service lifecycle, from design through the development process to production support. 


### You build it, you run it.

Key point being it brings developers in contact with the day-to-day operation of their software.

Practices in the Context of Devops:
- Infrastructure Automation - describing your infrastructure and its configuration as a script or set of scripts so that environments can be replicated in a much less error-prone manner
- Continuous Integration - focus on merging changes into master branch using automated tooling and checks to validate code
- Continuous Delivery - an extension of CI that focus's on the release
- Continuous Monitoring - collect and analyze data from the infrastructure (hardware, network, application) and to leverage the data to improve business results

There becomes a breakdown of the "Silos" between the operations team and the development team. Usually we see the dev team as the "makers" and the operations team as "the people who deal with creation after birth."

> DevOps is also characterized by operations staff making use many of the same techniques as developers for their systems work. 


### One Word, Velocity.

The main idea behind velocity is to help teams estimate how much work they can complete in a given time period based on how quickly similar work was previously completed.

### More Info
- [Atlassian - What is Devops](https://www.atlassian.com/devops/what-is-devops)
- [Atlassian - You Build it You Run It](https://www.atlassian.com/incident-management/devops/you-built-it-you-run-it)
- [Atlassian - CI CD](https://www.atlassian.com/continuous-delivery/principles/continuous-integration-vs-delivery-vs-deployment)
- [IBM - Infrastructure Automation](https://www.ibm.com/developerworks/library/a-devops2/index.html)
- [AWS - What is devops](https://aws.amazon.com/devops/what-is-devops/)
- [New Relic - What is Devops](https://newrelic.com/devops/what-is-devops)
- [Guru99 - Agile vs Devops](https://www.guru99.com/agile-vs-devops.html)
- [Buzzword Abuse](https://devops.com/buzzword-abuse-anatomy-devops-engineer/)
