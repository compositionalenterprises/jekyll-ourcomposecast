---
title: Shelving OurCompose
service: Firefly-III
tagline: This week we discuss our thoughts on OurCompose, where we succeeded and failed, and where we are moving forward.
chapters:
  - start: '00:00:00'
    title: 'Shelving OurCompose'
---

This post goes into detail the shelving of the OurCompose Project. It encompasses some final thoughts on the project.

Major Thoughts (from Jack):

# What's the most important part of every joke? Timing

Well.. Maybe if this were mid-2000's or Early 2010's there would be a different story. 
There are a few projects that already exist within the space that are struggling in the same ways we are. 
Fortunately or Unfortunately, some of these projects have packed up in search for better things because they stumbled into the same results we did.
Or I should say, we stumbled into the same results they did.

# Results Driven?

No. We never monetized from the beginning focusing on paying customers. I guess looking back we were results driven towards our own results, instead of customer's results. 

"Do things that don't scale." -> I think we went out and quite literally did the exact opposite. We built something that could scale. (You can put a star by that statement, but given the correct number of signups for the paid service, we could have scaled with ease. Expanding instances may have been a different story, but still..)


# 10x better for the Customer to Move

Unfortunately, we were still working towards feature parity with some of the major services.

PaaS/IaaS/SaaS is getting to be a bloated and oversaturated market. 

It is hard to compete in a market already dominated by major/medium sized players.

“You have to be 10 times better than second best.” - Peter Thiel

It would have taken us moving a mountain to get to 10x the second best. Teams of engineers are working towards enterprise solutions in the cloud space. 


# The Project From Here

The project/collection/portal/commandcenter are all MIT Licensed. They won't be going anywhere.

Support will likely be on an as requested basis via gitlab issues.



# Where do you go from here?

Evaluation first, then action. 

