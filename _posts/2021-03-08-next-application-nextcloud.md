---
title: Next Application NextCloud
service: Nextcloud
tagline: Jack has a next-level meeting with Andrew about kicking off the Nextcloud track, after learning they've got Zoom Fatigue from being night owls
chapters:
  - start: '00:00:00'
    title: 'Introduction'
  - start: '00:22:10'
    title: 'News/Community Updates'
  - start: '00:43:50'
    title: 'OurCompose Developments'
  - start: '00:44:30'
    title: 'Integration Discussion - Nextcloud - Overview'
  - start: '01:05:45'
    title: 'Grab Bag - What the Heck is EOS?'
---

## Intro

### [Night Owls May Be Twice as Likely as Early Rises to Underperform at Work](https://www.msn.com/en-us/news/us/night-owls-may-be-twice-as-likely-as-early-risers-to-underperform-at-work-study-suggests/ar-BB1dXcsD)

### [Four Causes for 'Zoom Fatigue'](https://news.stanford.edu/2021/02/23/four-causes-zoom-fatigue-solutions/)

## News / Community Updates

### [Bitwarden 1.19.0](https://github.com/dani-garcia/bitwarden_rs/releases)

### [Firefly-III Releases Application Architecture Documentation](https://docs.firefly-iii.org/firefly-iii/about-firefly-iii/architecture/)

### [Nextcloud Hub version 21](https://nextcloud.com/blog/nextcloud-hub-21-out-with-up-to-10x-better-performance-whiteboard-and-more-collaboration-features/)

### [Digital Ocean Files for IPO](https://news.crunchbase.com/news/digitalocean-files-for-ipo-among-a-sea-of-spacs/)

## OurCompose Developments

### [Kanboard - Search Documentation (1505)](https://compositionalenterprises.ourcompose.com/bookstack/books/kanboard/page/search)

## Integration Discussion - [Nextcloud - The all in one internet application](https://compositionalenterprises.ourcompose.com/bookstack/books/nextcloud/page/data-at-your-control)

## Grab Bag - [What the Heck is EOS?](https://theprocesshacker.com/blog/what-the-heck-is-eos-book-summary/)

### Chapter 6 - Why Do We Have to Have Meetings?

> Meetings are not a waste of time, it's what you _do_ in meetings that's a waste of time.

#### The stated goal: One meeting that eliminates all your other meetings that week.

Caveats:
- Internal non-management team meetings only
- Extensive use of:
    - Drop it Down: During status reports, any item that is off track or that anyone has a question about gets dropped down to a later section of the meeting
    - Tangent Alert: When people start getting off the subject and going down rabbit holes, you don't have to sit there helplessly, simply say "tangent alert!" to alert your team to the fact that the conversation has started to go sideways.

#### Meeting Pulse

Procrastination is the antithesis to productivity:

> 'Tis not difficult to conquer, once understood. No man willingly permits the thief to rob his bins of grain. Nor does any man willingly permit an enemy to drive away his customers and rob him of his profits. When once I did recognize that such acts as these my enemy was committing, with determination I conquered him. So must every man master his own spirit of procrastination before he can expect to share in the rich treasures of Babylon.
>
> - [The Richest Man in Babylon](http://www.ccsales.com/the_richest_man_in_babylon.pdf)

In other words:

- Normally, people are assigned tasks in your meeting
- People delay taking action on these tasks until just before the next meeting
- This causes a spike in productivity right before the meeting
- Therefore, to the degree you increase the meeting frequency, you create that spike of activity more often.

The frequency is the correct timing and consistent agenda to accomplish 4 things:

- Make sure everything is on-track
- Stay on the same page with your team
- Hold each other accountable
- Solve issues

#### Meeting Roles

- Meeting Faciliator
    - Runs the meeting
    - Move team through agenda
    - Keep everything on track
- Document Manager/Scribe
    - Manages the documents
    - Maintains lists of tasks/projects
    - Documents any decisions agreed-upon
    - Notes any blocking requirements
    - Marks down any commitments made to accomplish tasks

**The two above roles must be assumed by 2 different people**

#### Meeting Sections

- **Intro & Segue**
    - Start on time
    - Take the first 5 minutes to Segue
        - Transition away from day-to-day battles
        - It is suggested to share good news, personal and/or professional
        - Helps humanize the process
        - Allows team members to get to know each other better
        - starts the meeting off with a positive vibe
- **Conclusion**
    - HARD stop with 5 minutes left to complete remaining sections:
        - Recap the TODO list that resulted from the meeting discussion
        - Decide the direction for follow-up outside of the team that is required
            - What the message is
            - Who will deliver it
        - Rate the meeting on a scale of 1-10 based on:
            - Did it start and end on time?
            - Did you follow the agenda?
            - Is everyone on the same page?
            - Did 90% or more of the TODOs get done?
            - Did you solve the most important issues?

### Chapter 7 - What's My Number?

> Imaging yourself playing a sport and you can't see the scoreboard. You don't know the score; the referees will not tell you when you commit a foul or penalty; you don't know how much time is left on the clock. You're not sure if you're winning or losing, and the frustrated coach keeps telling you, "Just play harder."

Consequences:
- Wasted Time
- Poor Resource Allocation
- Weak Overall Performance
- Terrible Results

_Why should every employee have a number?_
- Numbers cut through murky subjective communication between manager and employee
- Numbers create accountability
- Accountable people appreciate numbers
- Numbers create clarity and commitment
- Numbers create competition
- Numbers produce results
- Numbers create teamwork
- Numbers solve problems faster


#### Metrics

Aliases:
- Dashboard
- Flash Report
- Pulse Report
- KPI (Key Performance Indicators)
- Scorecards

**Different Levels Have Different Amounts of Metrics**
- Company: 5-15
- Department: 3-5

#### What Gets Measured, Gets Done

Random Rationale:
- Metrics should be on activity-based leading indicators. That means they measure current activity that produces future results.
- Scorecards should display the last 13 weeks-worth of data at a glance in order to identify patterns and trends.
- The individual and the department metrics should have the same measurables reported on
    - The department metrics should simply be the aggregate of the individuals' that comprise it.
