---
title: Minimal Viable Portal
service: Portal
tagline: The boys take shots at big tech, and chat about more homegrown tech, and wrap it up with some philosophy about the software development lifecycle.
chapters:
  - start: '00:00:00'
    title: 'Introduction'
  - start: '00:10:36'
    title: 'News/Community Updates'
  - start: '00:25:25'
    title: 'OurCompose Developments'
  - start: '00:26:35'
    title: 'Integration Discussion - Portal - Overview'
  - start: '00:34:58'
    title: 'Grab Bag - The Lean Startup'
---

## Intro

### Finding files in a more simple way by sorting by 'Date Modified'

- [3 Tips to Organise Your Dropbox Folders](https://www.lifehack.org/528359/3-tips-organise-your-dropbox-folders)
- [Use 'Date Modified' Sorting to Simplify File Selection](https://www.pcworld.com/article/165527/Use_Date_Modified_Sorting_to_Simplify_File_Selection.html)
- [How to Find and Sort Files Based on Modification Date and Time in Linux](https://www.tecmint.com/find-and-sort-files-modification-date-and-time-in-linux/)

## News / Community Updates

### [Python overtakes Java to become the second-most popular programming language](https://www.techrepublic.com/article/python-overtakes-java-to-become-the-second-most-popular-programming-language/)
- [Stack Overflow's Most Loved, Dreaded, and Wanted Programming Languages](https://fossbytes.com/stack-overflow-most-loved-dreaded-wanted-programming-languages-in-2020/)

### [Google Photos will end its free unlimited storage on June 1st, 2021](https://www.theverge.com/2020/11/11/21560810/google-photos-unlimited-cap-free-uploads-15gb-ending)

### [YouTube went down](https://www.engadget.com/youtube-outage-003536470.html)

> ...YouTube is under no obligation to host or serve Content...

_[Youtube Terms of Service](https://www.youtube.com/static?template=terms)_

[Four Nines of Uptime](https://uptime.is/four-nines)

## OurCompose Developments

### Instance Initialization Emails

### CommandCenter Anti-Spam Measures

## [Integration Discussion - Portal - Overview](https://compositionalenterprises.ourcompose.com/bookstack/books/portal)

## Grab Bag - [The Lean Startup](http://theleanstartup.co)

### Vision
#### Start
- Lean Startup uses a different unit of progress, called validated learning.
- The Lean Startup asks people to start measuring their productivity differently to figure out the right thing to build as quickly as possible.
- Instead of making complex plans that are based on a lot of assumptions, you can make constant adjustments with a steering wheel called the Build-Measure-Learn feedback loop.
#### Define
- The theories of management treat innovation like a “black box” by focusing on the structures companies need to put in place to form internal startup teams. But find yourself working inside the black box and you will be in need of guidance.
- Once a team is set up, what should it do? What process should it use? How should it be held accountable to performance milestones?
#### Learn
##### VALUE VS WASTE
- Which of our efforts are value-creating and which are wasteful?
##### VALIDATED LEARNING:
- Positive changes in metrics became the quantitative validation that our learning was real.
- Everything a startup does is understood to be an experiment designed to achieve validated learning
- If the numbers from such early experiments don’t look promising, there is clearly a problem with the strategy. That doesn't mean it's time to give up; on the contrary, it means it's time to get some immediate qualitative feedback about the program.
- Value in a startup is not the creation of stuff, but rather validated learning about how to build a sustainable business.
#### Leap
- These two hypotheses represent two of the most important leap-of-faith questions any new startup faces:
  - rate of growth
  - retention rate (come-back rate)
##### STRATEGY IS BASED ON ASSUMPTIONS
- The first step in understanding a new product or service is to figure out if it is fundamentally value-creating or value-destroying. I use the language of economics in referring to value.
#### Test
- Unlike a prototype or concept test, an MVP is designed not just to answer product design or technical questions. Its goal is to test fundamental business hypotheses.
##### WHY FIRST PRODUCTS AREN'T MEANT TO BE PERFECT
- Before new products can be sold successfully to the mass market, they have to be sold to early adopters. These people are a special breed of customer. They accept—in fact prefer—an 80 percent solution; you don’t need a perfect solution to capture their interest
- Early adopters use their imagination to fill in what a product is missing.
##### THE ROLE OF QUALITY AND DESIGN IN AN MVP
- Most modern business and engineering philosophies focus on producing high-quality experiences for customers as a primary principle; These discussions of quality presuppose that the company already knows what attributes of the product the customer will perceive as worthwhile.
- _If we do not know who the customer is, we do not know what quality is._
#### Measure
- A startup’s job is to:
  - Rigorously measure where (the product) is right now, confronting the hard truths that assessment reveals, and then
  - Devise experiments to learn how to move the real numbers closer to the ideal reflected in the business plan.
##### WHY SOMETHING AS SEEMINGLY DULL AS ACCOUNTING WILL CHANGE YOUR LIFE
- I make a habit of asking startups whenever we meet: are you making your product better? They always say yes. Then I ask: how do you know?
- To answer these kinds of questions, startups have a strong need for a new kind of accounting geared specifically to disruptive innovation. That’s what innovation accounting is.
##### HOW INNOVATION ACCOUNTING WORKS—THREE LEARNING MILESTONES
- Innovation accounting works in three steps:
  - First, use a minimum viable product to establish real data on where the company is right now.
  - Second, startups must attempt to tune the engine from the baseline toward the ideal.
  - Third: a decision needs to be made whether to pivot or persevere
##### OPTIMIZATION VERSUS LEARNING
- A startup has to measure progress against a high bar: evidence that a sustainable business can be built around its products or services.
##### VANITY METRICS: A WORD OF CAUTION
- Innovation accounting will not work if a startup is being misled by these kinds of vanity metrics: gross number of customers and so on. The alternative is the kind of metrics we use to judge our business and our learning milestones, what I call actionable metrics.
##### THE VALUE OF THE THREE A’S
- Actionable
  - When cause and effect is clearly understood, people are better able to learn from their actions. Human beings are innately talented learners when given a clear and objective assessment.
- Accessible
  - Cohort-based reports are the gold standard of learning metrics: they turn complex actions into people-based reports.
- Auditable
  - Instead of housing the analytics or data in a separate system, reporting data and its infrastructure should be considered part of the product itself and owned by the product development team.
##### INNOVATION ACCOUNTING LEADS TO FASTER PIVOTS
### Accelerate
#### Batch
##### SMALL BATCHES IN ENTREPRENEURSHIP
- The theory that is the foundation of Toyota’s success can be used to dramatically improve the speed at which startups find validated learning.
- In the Lean Startup the goal is not to produce more stuff efficiently. It is to, as quickly as possible, learn how to build a sustainable business.
- By reducing batch size, we can get through the Build-Measure-Learn feedback loop more quickly than our competitors can.
##### PULL, DON’T PUSH
- This creates a “hole”, which automatically causes a signal to be sent
- This is the famous Toyota just-in-time production method
- Our goal in building products is to be able to run experiments that will help us learn how to build a sustainable business.
- The right way to think about the product development process in a Lean Startup is that it is responding to pull requests in the form of experiments that need to be run.
- Thus, it is not the customer, but rather our hypothesis about the customer, that pulls work from product development and other functions. Any other work is waste.
#### Grow
##### WHERE DOES GROWTH COME FROM?
- The engine of growth is the mechanism that startups use to achieve sustainable growth.
- Sustainable growth is characterized by one simple rule: New customers come from the actions of past customers.
- There are four primary ways past customers drive sustainable growth:
  - Word of mouth
  - As a side effect of (public) product usage.
  - Through funded advertising
  - Through repeat purchase or use
##### THE THREE ENGINES OF GROWTH
- One of the most expensive forms of potential waste for a startup is spending time arguing about how to prioritize new development once it has a product on the market.
Startups have to focus on the big experiments that lead to validated learning. The engines of growth framework helps them stay focused on the metrics that matter.
- Sticky
  - rely on having a high customer retention rate
  - track their attrition rate or churn rate very carefully
  - if the rate of new customer acquisition exceeds the churn rate, the product will grow
  - In the sticky engine of growth, they are better suited to testing the value hypothesis: that customers find the product valuable
- Viral
  - depend on person-to-person transmission as a necessary consequence of normal product use
- Paid
  - you need to know only one thing: how much it costs to sign up a new customer
- more than one engine of growth can operate in a business at a time
##### MARKET FIT
- Describes the moment when a startup finally finds a widespread set of customers that resonate with its product
- A startup can evaluate whether it is getting closer to product/market fit as it tunes its engine by evaluating each trip through the Build-Measure-Learn feedback loop using innovation accounting.
##### WHEN ENGINES RUN OUT
- every engine of growth eventually runs out of gas. Every engine is tied to a given set of customers and their related habits, preferences, advertising channels, and interconnections. At some point, that set of customers will be exhausted.
#### Adapt
- Having no system at all is not an option.
##### THE WISDOM OF THE FIVE WHYS
- The core idea of Five Whys is to tie investments directly to the prevention of the most problematic symptoms.
- The system takes its name from the investigative method of asking the question “Why?” five times to understand what has happened (the root cause).
- At the root of every seemingly technical problem is a human problem.
- By asking and answering “why” five times, we can get to the real cause of the problem, which is often hidden behind more obvious symptoms.
##### THE CURSE OF THE FIVE BLAMES
- When the Five Whys approach goes awry, I call it the Five Blames.
- I ask teams to adopt these simple rules:
  - Be tolerant of all mistakes the first time.
  - Never allow the same mistake to be made twice.

> I have noticed this pattern every time: switching to validated learning feels worse before it feels better. That’s the case because the problems caused by the old system tend to be intangible, whereas the problems of the new system are all too tangible. Having the benefit of theory is the antidote to these challenges. If it is known that this loss of productivity is an inevitable part of the transition, it can be managed actively.
