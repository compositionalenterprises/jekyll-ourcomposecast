---
title: Static Jekyll For Your Site
service: Jekyll
tagline: There's a lot going on in the community this time! Introducing a couple of platforms, Jack provides some insight into payment processors, and Andrew goes over Jekyll.
chapters:
  - start: '00:00:00'
    title: 'Introduction'
  - start: '00:00:29'
    title: 'News/Community Updates'
  - start: '00:08:30'
    title: 'OurCompose Developments'
  - start: '00:15:04'
    title: 'Integration Discussion - Jekyll - Overview'
  - start: '00:43:35'
    title: 'Grab Bag - Stripe at Ourcompose'
---

## News / Community Updates

### [GitLab.com changes CI/CD minute limits for free accounts](https://about.gitlab.com/pricing/faq-consumption-cicd/)

[Subscription Plans](https://about.gitlab.com/pricing/)

### [Jekyll 3.9.0 Released](https://jekyllrb.com/news/2020/08/05/jekyll-3-9-0-released/)

### [August 30th 2020: Analysis of CenturyLink/Level(3) Outage](https://blog.cloudflare.com/analysis-of-todays-centurylink-level-3-outage/)

## OurCompose Developments

### Tribe Updates

- [Locals.com](https://ourcompose.locals.com)
- [Donations Page Live](https://ourcompose.com/jekyll/donate)
- [Mailing List](http://eepurl.com/hb8LLv)

### Compositional Role

- [v2.4.16 Release](https://gitlab.com/compositionalenterprises/role-compositional/-/tags/v2.4.16)
    - WordPress Bump to 5.4.X Release Train
- [README Documentation Updates](https://gitlab.com/compositionalenterprises/role-compositional/-/blob/master/README.md)
- [Available on Ansible Galaxy](https://galaxy.ansible.com/smacz42/role_compositional)

## [Integration Discussion - Jekyll - Overview](https://compositionalenterprises.ourcompose.com/bookstack/books/jekyll-69a/page/overview)

## Grab Bag - Stripe at OurCompose

As an online service provider we need a way to accept payments across the internet. 
We built our application from the ground up and use Stripe as our payment processing provider. 

We evaluated other payment methods such as PayPal and Shopify, but these didn't meet our needs and we were unimpressed with the integration abilities.
Most of these other payment processing platforms redirect users away from our site, and they don't have the customization ability that Stripe offers.

### What is Stripe?

Stripe is a ["fully integrated suite of payment products."](https://stripe.com)
A lot of what we use at OurCompose is the Billing API. 
The Stripe Billing API is easy to integrate with existing websites, mobile apps, and CMS systems. 
It is a technology-first approach to payments and finance.

> We believe that payments is a problem rooted in code, not finance. We obsessively seek out elegant, composable abstractions that enable robust, scalable, flexible integrations. Because we eliminate needless complexity and extraneous details, you can get up and running with Stripe in just a couple of minutes.

One of the features I really like is the granular control over pricing plans. 
With Stripe's flexibility we are able to add one time charges, recurring charges, usage-based charges, tiered pricing, promotions and trial periods, and adjustable billing frequency. 

Check out [Stripe billing](https://stripe.com/billing) for more information.

### OurComposeCast Donations - Memberful powered by Stripe

[Memberful](https://memberful.com/) is a tool we use to accept monthly and one-time donations on our jekyll (static) site.

Memberful integrates with our already existing Stripe account and does all the legwork for us. 
I would describe it as a tool built on Stripe for CMS's (Content Management Systems) and static sites.


### CommandCenter - Stripe Embedded

Commandcenter is a React/Ruby on Rails application we use to manage customers and billing.
Ruby uses "gems" to manage dependencies, and [Stripe has a gem](https://github.com/stripe/stripe-ruby) that gives our application the ability to handle payments within the site using Stripe's billing and payment services. 

We also use React to customize our front end and the [react-stripe-elements library](https://github.com/stripe/react-stripe-elements) to make the payment form you see pretty. 

Check out [Accepting a Payment](https://stripe.com/docs/payments/accept-a-payment) and the [Integration Builder](https://stripe.com/docs/payments/integration-builder) for more information on how Stripe handles payments.
