---
title: Dolibarr for This, Dolibarr for That
service: Dolibarr
tagline: This week we discuss DDOS attacks via Gitlab Servers, more cloud services being down, dolibarr and why every programmer aspires to be Linus Torvalds.
chapters:
  - start: '00:00:00'
    title: 'Intro - News - Updates'
  - start: '00:30:15'
    title: 'Integration Discussion - Dolibarr - Overview'
  - start: '00:42:52'
    title: 'Grab Bag - All Programmers Aspire to be Linus Torvalds'
---

## Intro

- [GitLab servers are being exploited in DDoS attacks in excess of 1Tbps](https://therecord.media/gitlab-servers-are-being-exploited-in-ddos-attacks-in-excess-of-1-tbps/)
- [Google Cloud Down!](https://status.cloud.google.com/)
  - [Down Detector Tweet](https://twitter.com/CabacoD/status/1460671936409837573?s=20)
- [You Can Be Directive Without Being a Jerk](https://larahogan.me/blog/be-directive-without-being-a-jerk/)

> When your team faces any hurdle or stuck moment, you’ll need to decide how to jump in.
> You might choose to adopt an empowering approach, viewing your role more as a facilitator and support structure as the team plots the path forward.
> But there will still likely be moments where you need to be directive, making decisions and tackling aspects of the work yourself.
> Don’t worry: we can still approach this in a way that drives buy-in and helps your teammates feel heard, and know they have autonomy.

> When we as managers are being directive on a project, we’re deciding on the who, what, when, and how, and then we’re communicating that information to the rest of the team.
> - Describe your role in outcomes, rather than describing how you’ll do your work. This way, the “how” can iterate over time, but your role’s success criteria remains the same.

> There will be times when folks’ reactions will get in the way of that forward progress, though.
> When people are frustrated with being told what to do, they might slow down/fight/check out (which are [classic signs of amygdala hijacking](https://larahogan.me/blog/five-common-forms-of-resistance/); no judgment here!).

> Naturally, when you choose the directive route to assigning roles and responsibilities, there will be folks who aren’t thrilled about your decisions.
> That’s okay.
> It can be hard to remember, but you’re not optimizing for avoiding grumpiness here.
> Try spending five minutes to understand where they are coming from:
> - What feels most important to you about this?
> - What is your gut telling you?
> - What one thing do you wish you could change about this?

> By using a coaching approach here, you’re actually operating from the empowerment end of the spectrum.
> In a lot of directive settings, you will still have an opportunity to leverage empowering skills to help others feel seen and heard, and to help them grow.
> That’s why it’s important to continue adapting your approach as the context evolves!

> But the bigger win?
> Asking these open questions and reflecting on what you heard them say will make your teammate feel heard and seen, which speeds up their ability to recover from their amygdala hijack.

## News / Community Updates

- [Wordpress 5.8.2 Security and Maintenance Release](https://wordpress.org/news/2021/11/wordpress-5-8-2-security-and-maintenance-release/)

## OurCompose Developments

- How to pass raw tags to a Jekyll post
- How Cloud-Init can fail a deploy
- [Dolibarr as an ERP](https://gitlab.com/compositionalenterprises/ansible-collection-compositionalenterprises.ourcompose/-/merge_requests/31)
- [Retire Instagram Account](https://www.instagram.com/p/CV1jW1dIWsH/)

## Integration Discussion - [Dolibarr - Overview](https://compositionalenterprises.ourcompose.com/bookstack/books/dolibarr/page/overview)

## Grab Bag - All Programmers Aspire to Put Themselves in the Position of Linus Torvalds

### Established Presence in a Large Project

- Large project benefits
  - Established in market
  - Established norms
  - Work-life balance
    - ["I'm not a programmer anymore"](https://www.zdnet.com/article/linus-torvalds-im-not-a-programmer-anymore/https://www.zdnet.com/article/linus-torvalds-im-not-a-programmer-anymore/)
- Established Presence benefits
  - Know the project inside and out
  - Being able to reference documented decisions
  - Intimately familiar with the project
  - You're a guy from "that project"

### BDFL in a Project

- Respect
  - Legacy
  - Perks
  - Opportunities
  - Not-insignificant amount of money
- Proteges
  - Right-hand men
  - Support hierarcy
  - Delegation of work and responsibilities
- Making decisions and setting policy
  - e.g. "Do not break userspace"
  - like using CNAMES as workarounds
    - document that as fix
    - not tech debt - not making additional work

### Tooling

- Hardest part about jumping into a project is figuring out how to set up a testing environment
  - Tooling here can be anywhere from virtualenvs, to complex test suite, to production deployment
- Linus wrote his own toolset
  - Miles ahead of market's toolset
  - [Yes, I'm aware he's not developing it now: "I maintained Git for six months, no more,"](https://www.techrepublic.com/article/linus-torvalds-git-proved-i-could-be-more-than-a-one-hit-wonder/)
- Everyone writes glue code
  - Yes, even in Ops and Infra 
  - Maturation process reduces glue code down to tool
    - systemitize tool
    - modular
    - modern
    - stable
- He took tool for highly successful project, and made that tool highly successful
  - Delegated the maintenance of his tool to the FLOSS community

### In Conclusion

What are developers looking to do
- Looking achieve long-running purpose that gives them meaning and successful work-life balance
- In a project that they are familiar with and scratches their particular itch which allows them a degree of authority
- Using the tools that they prefer and have crafted/tweaked/tailored to their own needs

### In the future
- Web3 can lean even harder in this direction
  - Linus doesn't actually sell his infrastructure maintenance
- In the heyday of The SAFE Network, I got to ask ESR a question...
