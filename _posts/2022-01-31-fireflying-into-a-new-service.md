---
title: Fireflying into a New Service
service: Firefly-III
tagline: This episode we discuss FireflyIII, Taking Your Thoughts Captive, and news around the community as well as updates with the OurCompose Suite. 
chapters:
  - start: '00:00:00'
    title: 'Intro - News - Updates'
  - start: '00:21:14'
    title: 'Integration Discussion - FireflyIII - Re-introduction and Accounts'
  - start: '00:38:08'
    title: 'Grab Bag - Taking your thoughts captive'
---

## Intro

## News / Community Updates
- [Dolibarr Now on Reddit](https://reddit.com/r/Dolibarr_ERP_CRM)


## OurCompose Developments

### Instance Features

### Service Resiliancy

### Engagement

### Administrative
- [Q1 2022 Quarterly Meeting](https://compositionalenterprises.ourcompose.com/kanboard/public/board/245d06ce519a4deba6131ea58a65afa9efc0e6de584e04b08a81c4115322)

## Integration Discussion - [FireflyIII - Re-introduction and Accounts](https://compositionalenterprises.ourcompose.com/bookstack/books/firefly-iii/page/accounts)

## Grab Bag - Taking your thoughts captive

[2 Corinthians 10:5](https://biblia.com/bible/nkjv/2-corinthians/10/5)

### Navel-gazing
- Self-examination vs introspection and morbidity
- Ultimately it's very self-centered; egotistical

- Pros of letting your mind run wild
  - High Standards
  - Empathy
  - Social self-awareness
  - Self-improvement
- Drawbacks
  - Same "...the crap out of... " for everything else
  - Silencing my mind when it's time to be present/sleep
  - Taking breaks and/or knowing when to step away
  - Concentration on reading
  - Dwelling on past mistakes, and/or blowing them out of proportion
  - Bingeing

The only way that helped was to think about how I was interacting with these impulses/thoughts.
[Aware! Alert! Alive!](https://www.rifftrax.com/safety-harm-hides-at-home)

### Helps
- Meditiation to train my brain how to ignore thoughts
- Envisioning the thoughts as originating from a separate entity
  - Satan/Spirits
  - The Pig
- Know why certain thoughts are pervasive
- Have doctrine to combat doubt
- Talking to ourselves

### Our control over self
- Our Elephant can be reasonsoned with
- System 2 can influence System 1
- Prefrontal Cortex can influence Mamilian Brain

[Choose Again](https://novelasfreeonline.com/dan-simmons/56-152531-rise_of_endymion)
