---
title: Yet Another Kanboard Episode
service: Kanboard
tagline: Working on their development chops, Jack and Andrew discuss ditching Locals, and the additional content that we will be putting out for OurCompose.
chapters:
  - start: '00:00:00'
    title: 'Introduction'
  - start: '00:01:52'
    title: 'News/Community Updates'
  - start: '00:21:52'
    title: 'OurCompose Developments'
  - start: '00:33:07'
    title: 'Integration Discussion - Kanboard - Plugins'
  - start: '00:55:46'
    title: "Grab Bag - On Exercising, or I Swear We're Not Meatheads"
---

## Intro

## News / Community Updates

### [Podman 3.0 now supports Docker Compose](https://www.redhat.com/sysadmin/podman-docker-compose)

### [Locals.com switches to paid subscription communities only](https://support.locals.com/en/article/what-does-it-mean-that-locals-only-has-paid-subscription-communities-1da85h6/)

> Starting January 25th, all new communities on Locals will be paid communities. Any free communities started before January 25th will be converted to a paid community.

[Lunduke.com](http://lunduke.com), I'm taking my shot at you.

### [Nextcloud Podcast Manager](https://blog.project-insanity.org/2021/01/14/release-a-self-hosted-podcast-manager-for-your-personal-cloud/)

### [PeerTube v3 : it’s a live, a liiiiive !](https://framablog.org/2021/01/07/peertube-v3-its-a-live-a-liiiiive/)

## OurCompose Developments

### Q1 2021 Quarterly Planning
- Additional Content
- User Interaction
- Resiliency

## [Integration Discussion - Kanboard - Plugins](https://compositionalenterprises.ourcompose.com/bookstack/books/kanboard/page/plugins)

### Andrew's Custom CSS for Kanboard back in 2017

![Andrew's Custom CSS for Kanboard back in 2017](https://andrewcz.com/jekyll/images/kanboard-front.png)

## Grab Bag - On Exercising, or I Swear We're Not Meatheads

### Lift Heavy Things
- Weight room
    - [Starting strength](https://startingstrength.com/get-started/programs)

- Bodyweight
    - [Recommended Routine](https://www.reddit.com/r/bodyweightfitness/wiki/kb/recommended_routine)
    - [Endorphin Mainline](https://www.marksdailyapple.com/bodyweight-workout/)

### Move Around
- Sprinting
    - Hills
    - Accelerations

- Running
    - Miles
    - 5k
    - Longer

- Walking
    - Nature Walks
    - Urban Hiking

- Swimming
    - On/Off
    - Distance

### Playing Around
- Frisbee Golf
- Soccer
- Ultimate Frisbee

### Misc
- Leave the elliptical alone
- Rest in between sets
- Stay Hydrated!
- Form is more important than anything else
- Ain't nothing better than a cold shower after
- Music - Volume and Familiarity
