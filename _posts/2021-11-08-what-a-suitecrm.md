---
title: What A SuiteCRM
service: SuiteCRM
tagline: In this week's episode we discuss context switching, adding SuiteCRM to the Suite, and Pitching Anything.
chapters:
  - start: '00:00:00'
    title: 'Introduction'
  - start: '00:00:00'
    title: 'News/Community Updates'
  - start: '00:00:00'
    title: 'OurCompose Developments'
  - start: '00:35:59'
    title: 'Integration Discussion - SuiteCRM - Overview'
  - start: '00:48:04'
    title: 'Grab Bag - Pitch Anything'
---

## Intro

- [5 diagrams that show how context switching saps your productivity](https://www.linkedin.com/posts/ashleyfaus_5-diagrams-that-show-how-context-switching-activity-6859497194383319042-duBd)
- [Detect which CMS a site is using](https://whatcms.org)
- [Creating my personal cloud with HashiCorp](https://cgamesplay.com/post/2021/10/27/creating-my-personal-cloud-with-hashicorp/)

## News / Community Updates

- [Vaultwarden 1.23.0](https://github.com/dani-garcia/vaultwarden/releases)
- [Bookstack Security Release](https://www.bookstackapp.com/blog/bookstack-release-v21-10-3/)
- [Nextcloud Peer-to-Peer Backup App Now Available for Testing](https://nextcloud.com/blog/beta-of-peer-to-peer-nextcloud-backup-app-now-available-for-testing/)
- [SuiteCRM 7.12](https://suitecrm.com/suitecrm-7-12-has-launched/)

## OurCompose Developments

- [Bitcoind and fixes](https://gitlab.com/compositionalenterprises/ansible-collection-compositionalenterprises.ourcompose/-/merge_requests/30)
  - Working around [Ansible's Restarted Bug](https://github.com/ansible-collections/community.docker/issues/222)
  - Make Bind Mountpoints work with `/dev/mappper/devices`
  - [Bitcoin Cash is Bitcoin](https://www.bitcoin.com/bitcoin.pdf)
- [Compositional Enterprises Blog](https://compositionalenterprises.ourcompose.com/jekyll/posts/)

## [Integration Discussion - SuiteCRM - Overview](https://compositionalenterprises.ourcompose.com/bookstack/books/suitecrm/page/overview)

## Grab Bag - Pitch Anything

> Here's the "big idea": in 76 words: There is a fundamental disconnect between the way we pitch anything and the way it is received by our audience. As a result, at the crucial moment, when it is most important to be convincing, nine out of ten times we are not. Our most important messages have a suprisingly low chance of getting through.

> As it turns out, pitching is one of those business skills that depends heavily on the method you use and not how hard you try. 

> Our secret sauce is such-and-such technology. *No, that's not a secret sauce. That's ketchup.*

You have to own the room with *Frame Control*, drive emotions with *intrigue pings* and get to a *hookpoint* faily quickly.

Intrigue Ping - short but provactive piece of information that arouses curiosity
Hookpoint - the place in the presentation where your listeners become emotionally engaged (instead of giving information, they are asking you for more on their own.)

> If you have been in business for more than 10 minutes, you have figured this much out: The better you are at keeping someone's attention, the more likely the person will be to go for your idea.

> But what kind of advice is this really? Telling someone, “Keep the audience’s attention” is like telling someone learning to play tennis to “hit the ball with topspin when it comes.” *They know that!*

Our brain developed in three separate stages: 
- Crocodile Brain: responsible for the initial filtering of all incoming messages, it generates most survival fight-or-flight responses, and it produces strong, basic emotions
- Mid-Brain: determines the meaning of things and social situations
- Neocortex: evolved with a problem-solving ability and is able to think about complex issues and produce answers using reason.

Pitches are sent from the modern—and smart—part of the brain: the neocortex. But they are received by a part of the brain that is 5 million years older (and not as bright.)


Strong Acronym: <br />
S - Setting the Frame <br />
T - Telling the story <br />
R - Revealing the intrigue <br />
O - Offering the prize <br />
N - Nailing the hookpoint <br />
G - Getting a decision <br />


As I mentioned before, no situation has real meaning until you frame it.

People are always trying to impose frames on each other. The frame is like a picture of what you want the interaction to be about. And the most powerful thing about frames? There can be only one dominant frame during any interaction between two people.

Going into most business situations, there are three major types of opposing frames that you wil encounter: 
1. Power frame
2. Time frame
3. Analyst frame
<br/>

You have three major response frame types that you can use to meet these oncoming frames, win the initial col ision, and control the agenda:

1. Power-busting frame
2. Time constraining frame
3. Intrigue frame
There is a fourth frame you can deploy. It’s useful against al three of the opposing frames and many others you wil encounter: 4. Prize frame

Different Frames:
- The Power Frame: a status derived from the fact that others give this person honor and respect
- The Time Frame:Time frames are often used by your Target to rechal enge your frame by disrupting you and, in the moment of confusion, unwittingly take back control.
- The Intrigue Frame/Analyst Frame: How many times have you been giving a presentation when suddenly one or more people in the room take a deep dive into technical details? That's the Analyst Frame
- Prizing Frame: Prizing is a way to deal with threatening and fast-approaching frames that are likely to push you into a low-status position. Prize correctly and your target will be chasing you.

Prizing:
Let’s consider three of the most fundamental behaviors of human beings:
1. We chase that which moves away from us.
2. We want what we cannot have.
3. We only place value on things that are difficult to obtain. 


You’re going to make the pitch in four sections or phases:
1. Introduce yourself and the big idea: 5 minutes. 
- Start with a track record of success. 
- It’s vitally important that the target knows that your idea is new, emerging from current market opportunities and that it’s not some relic left over from bygone days
2. Explain the budget and secret sauce: 10 minutes.
- In phase 2, it gets harder to hold the target’s attention. Now you have to explain what problems the big idea real y solves and how it actual y works. The opportunities to scare the croc brain seriously multiply when you start to explain how stuff works. 
3. Offer the deal: 2 minutes.
- In phase 2, it gets harder to hold the target’s attention. Now you have to explain what problems the big idea real y solves and how it actual y works. The opportunities to scare the croc brain seriously multiply when you start to explain how stuff works. 
4. Stack frames for a hot cognition: 3 minutes. 
- Deciding that you like something before you fully understand it—that’s a hot cognition. 


In the end basically it boils down to telling a story that breaks down the analytical mindset, to keep the target intrigued and interested in what you have to say, to win respect so that they want you rather than the other way around, to get them emotionally engaged, and to get the deal at the end of your meeting.

So it’s not enough to be smart, or even to be right. In the end, what matters is how you get your point across to the other person. And there is a lot more to it than you think.

