---
title: Running to the Next Service
service: Rundeck
tagline: To cause to learn, the guys present 40,000 books to read, share some recent developments, and move on to talking about a new service, Rundeck!
chapters:
  - start: '00:00:45'
    title: 'Introduction'
  - start: '00:11:48'
    title: 'News/Community Updates'
  - start: '00:20:58'
    title: 'OurCompose Developments'
  - start: '00:27:36'
    title: 'Integration Discussion - Rundeck - Projects'
  - start: '00:52:47'
    title: 'Grab Bag - The Seven Laws of the Learner'
---

## Intro

- [40,000 HackerNews book recommendations identified using NLP and deep learning](https://hacker-recommended-books.vercel.app/category/0/all-time/page/0/0)
- [Intuit to Acquire Mailchimp](https://investors.intuit.com/news/news-details/2021/Intuit-to-Acquire-Mailchimp/default.aspx)
- [Manjaro Cinnamon Switches to Vivaldi As The Default Browser](https://fossbytes.com/manjaro-cinnamon-vivaldi-defau/)
- [OpenSSL 3.0 Has Been Released](https://www.openssl.org/blog/blog/2021/09/07/OpenSSL3.Final/)
- [PHP maintains an enormous lead in server-side programming languages](https://arstechnica.com/gadgets/2021/09/php-maintains-an-enormous-lead-in-server-side-programming-languages/)

## News / Community Updates

- [Rundeck 3.4.4](https://github.com/rundeck/rundeck/releases/tag/v3.4.4)
- [Jekyll - Goodbye Frank](https://jekyllrb.com/news/2021/09/14/goodbye-dear-frank/)
- [Wordpress - Road to 5.9](https://make.wordpress.org/core/2021/08/13/preliminary-road-to-5-9/)

## OurCompose Developments

- Portal with Rundeck Logs
- Prune MariaDB full-backups
- Bitwarden User Already Exists Check
- Grandfather keeps getting killed!!!

## Integration Discussion - [Rundeck - Projects](https://compositionalenterprises.ourcompose.com/bookstack/books/rundeck-d07/page/projects)

## Grab Bag - The Seven Laws of the Learner

### Hot Takes

- Came out the same year I was born
  - I think they might have been the first computer-generated diagrams ever created
- Came highly recommended
- Very regimented
- Almost over-informative and over-applicable
  - Sometimes you get that book where the author lays out something so methodically well that you find yourself not remembering any of the points because they flowed so logically from the conclusions?
- Two-chapter setup & knockout was interesting
  - Ultimately uneffective. It was less of a one-two punch, and more of a repetitive sequence of jabs
- Originally for seminary teachers (AFAICT)
- Just like How to win friends..." it always anchors its chapters by starting with a story

### Law 1: The Law of the Learner
- The teacher's responsibility is for the student's learning
  - The teacher's responsibility is not summed up in "covering the material".
  - In Hebrew the root of the word "teach" is the same as the word "learn"

#### Maxims
- Teachers are responsible because they control the presentation:
  - subject
  - style
  - speaker
- Teachers exist to serve the students
  - To overcome nervousness, don't be so proud and self-conscious!
  - Concern yourself with meeting the needs of your students.

#### Maximizers
- Alter your style regularly according to each situation
  - Approaching a slouched-over dejected young man vs a belligerent husband
- Don't let them be bored

> The essence of the Law of the Learner is these three words: "Cause to learn." The teacher should accept the responsibility of causing the student to learn.

### Law 3: The Law of Application

- The real value of an automobile is the application of its engine, not in the engine itself
  - Ritual of the Revving
  - Incredible surge of power
  - Inspiring and worth every drop of fuel
  - At that point, the automobile was an end in itself
- Application relates to wisdom, transformation, and maturity

#### Maxims
> The Bible wasn't given for our information but for our transformation.
>
> -- D. L. Moody

- Application is the responsibility of the teacher
  - Often the hope of the teach is to see the teaching applied, but less often do they take it upon themselves to detail out that self-same application
- Application and information should be balanced
  - Thorough analysis of several of the considered top preachers ever
  - Found "significantly more application than content"
  - New testament books never dropped below 50/50
  - Sermon on the Mount and the Upper Room Discourse were all very application-heavy
  - The take-away is to not let general principles morph into lists of rigid laws

> The essence of the Law of Application is these four words: "Apply for life change." The teacher should always teach for the purpose of life change.

### Law 5: The Law of Need

Turns out that fish _really do like to eat worms_.

> As the teacher I am responsible to help my students chase after my content.

Stories of the gospels show Christ addressing the needs of His hearers one of two types of ways:
- When the person's needs are obvious, immediately seek to meet them
- When the person is out of touch with their needs, seek to surface those needs, and then meet them

(In-depth example of the above with the Woman at the Well)

#### Maxims

- Need building is the responsibility of the teacher
- Need meeting is the teacher's primary calling
- Need building may be hindered by factors beyond the teacher's control
  - Lessons don't have needs, people have needs.

> The essence of the Law of Need is these three words: "Build the need." The teacher should build the need before teaching the content.
