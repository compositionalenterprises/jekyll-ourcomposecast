---
title: Nextcloud Dashboard
service: Nextcloud
tagline: After a large detour, Jack and Andrew how how and why Nextcloud has gone about winning you over with their dashboard.
chapters:
  - start: '00:00:00'
    title: 'Introduction'
  - start: '00:22:29'
    title: 'News/Community Updates'
  - start: '00:22:30'
    title: 'OurCompose Developments'
  - start: '00:32:00'
    title: 'Integration Discussion - Nextcloud - Dashboard'
  - start: '00:46:15'
    title: 'Grab Bag - How to Win People To Your Way of Thinking'
---

## Intro

### [Correction: Minnesota, not MIT trying to Posion the Linux Kernel](https://thenewstack.io/university-of-minnesota-researchers-tried-to-poison-the-linux-kernel-for-a-research-project/)

### [Clarity is an underrated skill](https://tomgamon.com/posts/clarity/)

### [Freenode IRC staff resign en masse, unhappy about new management](https://www.theregister.com/2021/05/19/freenode_staff_resigns/)

## News / Community Updates

## OurCompose Developments

- Services YT Video
- CommandCenter Front Page
- Portal Ability to modify environment
- Portal Nginx Logs

## [Integration Discussion - Nextcloud - Dashboard](https://compositionalenterprises.ourcompose.com/bookstack/books/nextcloud/page/dashboard)

## Grab Bag - Part Three: How to Win People To Your Way of Thinking

### You Can't Win an Argument

> Suppose you triumph over the other man and shoot his argument full of holes and prove that he is non compos mentis.
> You will feel fine.
> But what about him?
> You have made him feel inferior. (opposite of important)
> You have hurt his pride.
> He will resent your triumph, and:
>
> > A man convinced against his will,
> >
> > Is of the same opinion still.

**How to keep a disagreement from becoming an argument**:
- Welcome the disagreement
- Distrust your first instinctive impression
- Control your temper
- Listen first
- Look for areas of agreement
- Be honest
- Promise to think over your opponents' ideas and study them carefully
- Thank your opponents sincerely for their interest
- Postpone action to give both sides time to think through the problem

#### Principle 1: The only way to get the best of an argument is to avoid it.

### A Sure Way of Making Enemies; and How to Avoid It

> Men must be taught as if you taught them not,
> And things unknown proposed as things forgot.
>
> _Alexander Pope_

> Men You cannot teach a man anything; you can only help him to find it within himself.
>
> _Galileo_

> One thing only I know, and that is that I know nothing.
>
> _Socrates_

Instead of stating the fact of others' incorrectness, posit that the possibility exists of your mistakenness, and proceed from there.

#### Principle 2: Show respect for the other person's opinions. Never say, "You're wrong."

### If You're Wrong, Admit It

> If we know we are going to be rebuked anyhow, isn't it far better to beat the other person to it and do it ourselves? Isn't it much easier to listen to self-criticism than to bear condemnation from alien lips?

The best thing to do is to admit it quickly and emphatically.
The next best thing to do is to admit it emphatically.

#### Principle 3: If you are wrong, admit it quickly and emphatically.

### A Drop of Honey

> If you would win a man to your cause, first convince him that you are his sincere friend. Therein is a drop of honey that catches his heart; which, say what you will, is the great high road to his reason.
>
> _Abraham Lincoln_

> If I had tried the methods the other tenants were using, I am positive I should have met with the same failure they encountered. It was the friendly, sympathetic, appreciative approach that won.

> Remembering the principles, I decided that showing my anger would not serve any worthwhile purpose.

#### Principle 4: Begin in a friendly way

### The Secret of Socrates

> Once having said a thing, you feel you must stick to it. Hence it is of the very greatest importance that a person be started in the affirmative direction.
>
> The more "Yeses" we can get at the very outset, the more likely we are to succeed in capturing the attention of our ultimate proposal.

You can get people to agree fairly readily on one of two things
- Hard Mode: Common ground
- Easy Mode: Your representation of their point of view and/or interests
- Training Wheels: Objective facts

Old Chinese Proverb: "He who treads softly goes far."

#### Principle 5: Get the other person saying "yes, yes" immediately

### The Safety Valve in Handling Complaints

> Let the other people talk themselves out. They know more about their business and problems than you do.

You get to do this by asking clarifying questions. Keep in mind that the problems that you thought they had might not be the ones that they are most concerned with. By letting them talk themselves out, you are allowing them to dig deep and reveal their true concerns and cares.

#### Principle 6: Let the other person do a great deal of the talking.

### How to get cooperation

> Don't you have much more faith in ideas that you discover for yourself than in ideas that are handed to you on a silver platter?
> If so, isn't it bad judgement to try to ram your opinions down the throats of other people?
> Isn't it wiser to make suggestions, and let the other person think out the conclusion?

#### Principle 7: Let the other person feel that the idea is his or hers.

### A Formula That Will Work Wonders For You

> Remember that other people may be totally wrong.
> But they don't think so.
> Don't condemn them.
> Any fool can do that.
> Try to understand them.
> Only wise, tolerant, exceptional people even try to do that.
> 
> There is a reason why the other man thinks and acts as he does.
> Ferret out that reason, and you have the key to his actions, perhaps to his personality.
> 
> Try to put yourself in his place.

> Seeing things through another person's eyes may ease tensions when personal problems become overwhelming.

> Tomorrow, before asking anyone to put out a fire or buy your product or contribute to your favorite charity, why not pause and close your eyes and try to think the whole thing through from another person's point of view.
> Ask yourself: "Why should he or she want to do it?"
> True, this will take time, but it will avoid making enemies and will get better results, and with less friction.

> I would rather walk the sidewalk in front of a person's office for two hours before an interview than step into that office without a clear idea of what I was going to say and what that person, from my knowledge of his or her interests and motives, was likely to answer.

#### Principle 8: Try honestly to see things from the other person's point of view.

### What Everybody Wants

Here is a magic phrase that:
- Stops arguments
- Eliminates ill feeling
- Creates good will
- Makes the other person listen attentively

> I don't blame you one bit for feeling as you do. If I were you I would undoubtedly feel just as you do.

You deserve very little credit for being what you are.
Remember, the people who come to you irritated, bigoted, unreasoning, deserve very little discredit for being what they are.
Feel sorry for them; pity them and sympathize with them.
Say to yourself: "There, but for the grace of God, go I."
Most of the people you will ever meet are hungering and thirsting for sympathy.
Give it to them, and they will love you.

#### Principle 9: Be sympathetic with the other person's ideas and desires

### An Appeal That Everybody Likes

A person usually has two reasons for doing a thing: one that sounds good, and a real reason.

> People are honest and want to discharge their obligations.
> The exceptions to that rule are comparatively few, and I am convinced that the individuals who are inclined to chisel will in most cases react favorably if you make them feel that you consider them honest, upright and fair.

#### Principle 10: Appeal to the nobler motives

### The Movies Do It, TV Does It, Why Don't You Do It?

Merely stating a truth isn't enough. The truth has to be made vivid, interesting and dramatic. You have to use showmanship.

#### Principle 11: Dramatize your ideas.

### When Nothing Else Works, Try This

Charles Schwab had a mill manager whose people weren't producing their quota of work...

> The way to get things done is to stimulate competition. I do not mean in a sordid, money-getting way, but in the desire to excel.

#### Principle 12: Throw down a challenge
