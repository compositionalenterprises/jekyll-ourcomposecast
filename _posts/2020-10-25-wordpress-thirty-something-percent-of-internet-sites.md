---
title: Wordpress, Thirty-Something Percent of Internet Sites
service: Wordpress
tagline: Jack goves over Wordpress for whoever hasn't been on the internet for the past two decades, and Andrew reherses his talk about Ansible for Ohio Linuxfest.
chapters:
  - start: '00:00:00'
    title: 'Introduction'
  - start: '00:00:57'
    title: 'News/Community Updates'
  - start: '00:14:00'
    title: 'OurCompose Developments'
  - start: '00:14:20'
    title: 'Integration Discussion - Wordpress - Overview'
  - start: '00:27:21'
    title: 'Grab Bag - Delta Ansible: Keeping Up With Changes and Deprecations'
---

## News / Community Updates

### [Bitwarden 1.17.0](https://github.com/dani-garcia/bitwarden_rs/releases/tag/1.17.0)

### [Firefly-III Updates on Mastadon](https://fosstodon.org/@ff3)

### [Kanboard 1.2.16](https://github.com/kanboard/kanboard/releases)

### [Nextcloud 20](https://www.techrepublic.com/article/nextcloud-20-new-features-and-improvements/)

### [FSF Turns 35](https://www.fsf.org/blogs/community/fsf-at-35-join-us-in-celebrating-the-incredible-community)

### [Bootstrap 5 Switches from Jekyll to HuGo](https://blog.getbootstrap.com/2020/06/16/bootstrap-5-alpha/)

## OurCompose Developments

## [Integration Discussion](https://compositionalenterprises.ourcompose.com/bookstack/books/wordpress/page/overview)

## Grab Bag - Delta Ansible: Keeping Up With Changes and Deprecations
