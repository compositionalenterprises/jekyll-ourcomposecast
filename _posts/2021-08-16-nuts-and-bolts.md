---
title: Nuts and Bolts
service: Bitwarden
tagline: Despite 99 reasons to NOT get stuff done, the guys show off their latest accomplishments, as well as those in the community, and how scrum purports to provide a framework for all of it.
chapters:
  - start: '00:00:45'
    title: 'Introduction'
  - start: '00:25:15'
    title: 'News/Community Updates'
  - start: '00:27:15'
    title: 'OurCompose Developments'
  - start: '00:38:37'
    title: 'Integration Discussion - Bitwarden - Initial Configuration'
  - start: '01:07:53'
    title: 'Grab Bag - Scrum, An Agile Implementation'
---

## Intro
- [Hundreds of ways to get S#!+ Done - and we still don't](https://www.wired.com/story/to-do-apps-failed-productivity-tools/)
- [Rework - Before Basecamp](https://share.transistor.fm/s/b5da595a)
- [Bitcoin Cash’s sidechain, smartBCH, is launching with support from BCH community and miners](https://smartbch.medium.com/bitcoin-cashs-sidechain-smartbch-is-launching-with-support-from-bch-community-and-miners-95bbbc5dccbe)

## News / Community Updates

- [Rundeck 3.4.2](https://docs.rundeck.com/docs/history/3_4_x/version-3.4.2.html#enterprise-updates)
- [Widgets in Wordpress and Beyond](https://wordpress.org/news/2021/08/widgets-in-wordpress-5-8-and-beyond/)

## OurCompose Developments
- Cleaned up space in CE instance by setting up [Rundeck Execution Cleanup](https://docs.rundeck.com/docs/history/3_1_x/version-3.1.0.html#execution-cleaner)
- [Commands Receivable Docker Container](https://compositionalenterprises.ourcompose.com/kanboard/project/12/task/1951)
- [Semi-localized health checks in Portal via Commands Receivable](https://gitlab.com/compositionalenterprises/portal/-/merge_requests/127)
- [Expose Roadmap publicly](https://compositionalenterprises.ourcompose.com/kanboard/public/board/245d06ce519a4deba6131ea58a65afa9efc0e6de584e04b08a81c4115322)

## Integration Discussion - Bitwarden - [Initial Configuration](https://compositionalenterprises.ourcompose.com/bookstack/books/bitwarden/chapter/initial-configuration)

## Grab Bag - Scrum

### Feedback Loops, The Government's $110M Mistake

#### Government contracts, 1000 page papers no one reads, and a plan

Everything looks better on paper.. But what happens when the plan goes awry?

FBI had a plan to modernize information sharing. After three years and millions of dollars later they didn't bother looking at the process, but rather signed another contract with a contractor. Of course as defects arose, and when they were far behind they decided to look at a new approach to the project.

Scrum reduces the feedback cycle, and forces a continuous inspection and adaption cycle forcing the question "why" at every task verifying the need and/or how it can be improved.

Basically Asking:
- How can we work better?
- What were the impedements that needed to be removed?
- What is slowing us down?

### Lessons great companies have

- Multifunctional teams, with varied skills and experiences, with the autonomy to manage themselves;
- Leaders focused on removing impedements to facilitate progress
- Flexible and Fast development of projects in stages that increment
- Clear Objective and Purpose for every member of the organization

“A team has to demand greatness from itself.”

### One Task at a time

Something that is half done isn't done. It's essentially all or nothing when it comes to tasks. You can't have something that exists as 50% complete as complete.

Do one thing exclusively before moving on to another project.

Do It Right the First Time. When you make a mistake, fix it right away. Stop everything else and address it. Fixing it later can take you more than twenty times longer than if you fix it now. 

Only Plan What You Need To. Don’t try to project everything out years in advance. Just plan enough to keep your team busy.

It has been proven humans are terrible at planning in hours.

### Communication

Everyone knows Everything. Communication saturation accelerates work.

One Meeting a Day. When it comes to team check-ins, once a day is enough. Get together for fifteen minutes at the Daily Stand-up, see what can be done to increase speed, and do it.

### Roles

Development Team: every team has members with the necessary skills to design, build, and test the product, and everyone participates in the development process from the beginning to the end. Teams are multifunctional.

Scrum Master: Someone familiar with the Scrum structure, acting as an advisor and as a facilitator, with the goal to help the team identify and deal with impedements that arise through the project.

Product Owner: in contact with the stakeholders, receiving opinions, and feedback from customers. Role exists to relay vision and decisions from customer point of view

### Priorities

The key is what you decide to do first:
- Big impact?
- Important to customer?
- Make money?
- Easiest to implement?

Aim for REVENUE first, figure out the 20% of input that yields 80% of the output.

What is something that takes 20% to ship to the customer we can build the other 80% later?

Thinking most value for the least effort
