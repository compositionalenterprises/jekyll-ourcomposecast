---
title: Vaultwarden Securing Your Passwords
service: Bitwarden
tagline: Secure in their logins, the guys search around to find their space within text editors alongside many new updates.
chapters:
  - start: '00:00:00'
    title: 'Introduction'
  - start: '00:15:44'
    title: 'News/Community Updates'
  - start: '00:23:06'
    title: 'OurCompose Developments'
  - start: '00:26:32'
    title: 'Integration Discussion - Bitwarden - Application Interface'
  - start: '00:56:44'
    title: 'Grab Bag - Text Editor Features'
---

## Intro

- Jack over Wire
- [Microsoft left Linux users in the cold for almost an entire day](https://www.windowscentral.com/microsoft-left-linux-users-cold-almost-entire-day)
- [Four Useful `fzf` tricks for your terminal](https://pragmaticpineapple.com/four-useful-fzf-tricks-for-your-terminal/)
    - [File Hierarchy Preview](https://gist.github.com/Lanny/61afcb3c747a9c0f858e7460c77b464d)

## News / Community Updates

- [Vaultwarden 1.22](https://github.com/dani-garcia/vaultwarden/releases/tag/1.22.0)
- [Kanboard 1.2.20](https://github.com/kanboard/kanboard/releases/tag/v1.2.20)
- [Firefly III 5.5.12 Vulnerability Fix](https://github.com/firefly-iii/firefly-iii/releases/5.5.12)
- [Nextcloud IOS App brings on more improvements!](https://nextcloud.com/blog/nextcloud-files-for-ios-is-here-bringing-status-setting-chunking-file-modifications-more-security-and-endless-improvements-to-the-users/)


## OurCompose Developments

- Concierge Form for easy signup (for Admins)

## Integration Discussion - [Bitwarden - Application Interface](https://compositionalenterprises.ourcompose.com/bookstack/books/bitwarden/chapter/application-interface)

## Grab Bag - Text Editor Features

### Vim-story
- Word
- CLI for local Openbox text files - intro to vim
- Vanilla Vim
    - [vim + tmux - OMG!Code](https://www.youtube.com/watch?v=5r6yzFEXajQ)
    - [Mastering the Vim Language](https://www.youtube.com/watch?v=wlR5gYd6um0)
- Plugins & Config
- Papercuts / I forgot all of my customizations
- Spacevim & Neovim
- Natty

### Editor Features
- Multiple Files
- Syntax Highlighting
- Auto-complete & bracket-matching
- Git Integration
- Plugin Management
- Compilation & Syntax checking
- Snippets
- Definitions
- GOTO definitions / Tagging
- Project Management
- Command/feature discoverability
