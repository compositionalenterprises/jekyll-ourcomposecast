---
title: Managing Users
service: Nextcloud
tagline: The guys go over how to manage users without arousing resentment, or starting any more datacenter fires.
chapters:
  - start: '00:00:00'
    title: 'Introduction'
  - start: '00:07:30'
    title: 'News/Community Updates'
  - start: '00:20:34'
    title: 'OurCompose Developments'
  - start: '00:23:28'
    title: 'Integration Discussion - Nextcloud - User Management'
  - start: '00:58:20'
    title: 'Grab Bag - Be A Leader: How To Change People Without Giving Offense or Arousing Resentment'
---

## Intro

- [More Datacenter Fires](https://www.theregister.com/2021/06/11/aws_eu_central_1_incident/)
- [AWS eu-central-1 Ambient temperature increase](https://twitter.com/quinnypig/status/1403100781646028808)
- [Fastly CDN Outage](https://www.fastly.com/blog/summary-of-june-8-outage)

## News / Community Updates

- [Finding the Good in Disagreement](https://wordpress.org/news/2021/06/episode-10-finding-the-good-in-disagreement/)
- [Nextcloud 1.2.20](https://github.com/kanboard/kanboard/releases/tag/v1.2.20)
- [Bookstack v21.05](https://www.bookstackapp.com/blog/bookstack-release-v21-05/)

## OurCompose Developments

- Portal 1.7.1 and Portal 1.8.0
  - Ability to update Environment
  - Ability to view log files
- [Optimize Nextcloud](https://gitlab.com/compositionalenterprises/role-compositional/-/merge_requests/74)

## [Integration Discussion - Nextcloud - User Management](https://compositionalenterprises.ourcompose.com/bookstack/books/nextcloud/page/user-management)

## Grab Bag - Be A Leader: How to Change People Without Giving Offense or Arousing Resentment

### If you must find fault, this is the way to begin

It is always easier to listen to unpleasant things after we have heard some praise of our good points.

Bronze Subcontractor who was behind on shipment said he couldn't make the shipment.. Ended up fufilling the shipment after a visit from the project leader visited the location where the bronze is maid. 

Principle 1: Begin with praise and honest appreciation.


### How to Criticize... and Not Be Hated for It

Charles Schwab was walking a factory and two gentlmen were smoking under a no smoking sign. He handed them cigars and asked them to smoke them outside. They had known they had broken the rules. 

Principle 2: Call attention to people's mistakes indirectly 


### Talk about your own mistakes first

Admitting your own mistakes can help convince somebody to change his behavior. 

Principle 3: Talk about your own mistakes before criticizing the other person.


### No One Likes to Take Orders

Principle 4: Ask questions instead of giving orders


### Let the Other Person Save Face

"Firing employees is not much fun. Getting fired is even less fun."

GE Example of Charles Steinmetz who was brilliant engineer, but terrible head of accounting department.
They ended up re-assigning him with a new title where he was happy and the officers of GE were happy. 
He got to work with a new title for the work he was already doing, and the officers got to replace him with someone else to lead the department.

Even if we are right and the other person is definitely wrong, we only destroy ego by causing someone else to lose face.

Principle 5: Let the other person save face


### How To Spur People onto Success

The good things people do will be reinforced and the poorer things will atrophy for lack of attention. 

We all crave appreciation and recognition, and will do almost anything to get it. But nobody wants insincerity. Nobody wants flattery.

"Let me repeat: The principles taught in this book will work only when they come from the heart. Im not advocating a bag of tricks. Im talking about a new way of life."

Principle 6: Praise the slightest improvement and praise every improvement. Be "hearty in your approbation and lavish in your praise."


### Give a Dog a Good Name

"The average person can be lead readily if you have his or her respect and you show that person you respect that person for some kind of ability."

In short, if you want to improve a person in a certain aspect, act as though that particular trait were already one of his or her outstanding characteristics.

Give them a fine reputation to live up to, and they will make prodigious efforts rather than see you disillusioned.

Principle 7: Give the other person a fine reputation to live up to.


### Make the fault seem easy to correct

Basically there was this really bad kid that teachers really complained about. He was really smart, but picked fights. The fourth grade teacher pointed at him and made him responsible for being a leader in the class. She re-enforced this 3/4 times in the first few weeks and after that he was better in the class room. she enabled him to do better things and act more responsibly. 

Principle 8: Use Encouragement, Make the fault seem easy to correct. 


### Making people glad to do what you want

Example of Woodrow Wilson picking his friend over someone more qualified to go overseas in an attempt to bring peace to Europe.
Bryan was disappointed when he heard he wasn't going, but he was told "the President thought it would be unwise for anyone to do this officially, and that his going would attract a great deal of attention and people would wonder why he was there..."

Pear tree. Had his kid pick up every pear. Paid him $1 for every basket full, but took one away for every dollar on the ground.

Principle 9: Make the other person happy about doing the thing you suggest
