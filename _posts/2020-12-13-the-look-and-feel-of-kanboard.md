---
title: The Look and Feel of Kanboard
service: Kanboard
tagline: Andrew takes Python shots seriously, some applications get new versions, Andrew starts to dive into Kanboard in earnest, and Jack starts hypothetizing about what marketing that doesn't suck would look like.
chapters:
  - start: '00:00:00'
    title: 'Introduction'
  - start: '00:04:25'
    title: 'News/Community Updates'
  - start: '00:18:33'
    title: 'OurCompose Developments'
  - start: '00:21:11'
    title: 'Integration Discussion - Kanboard - Application Interface'
  - start: '00:51:35'
    title: 'Grab Bag - Permission Marketing'
---

## Intro

### Python Is Not Pseudo Code: [f-strings](https://miguendes.me/73-examples-to-help-you-master-pythons-f-strings)

## News / Community Updates

### [Gitea 1.13.0 is released with Kanban Board](https://blog.gitea.io/2020/12/gitea-1.13.0-is-released/)

### [Reddit reveals daily active user count for the first time](https://www.theverge.com/2020/12/1/21754984/reddit-dau-daily-users-revealed)
- [OurCompose subreddit](https://www.reddit.com/r/ourcompose/)

### [Introducing another free CA as an alternative to Let's Encrypt](https://scotthelme.co.uk/introducing-another-free-ca-as-an-alternative-to-lets-encrypt/)
- [Official Certbot Installation Recommends to Install via Snap](https://certbot.eff.org/lets-encrypt/ubuntubionic-nginx)

### [Docker 20.10.0](https://docs.docker.com/engine/release-notes/)

### [Introducing another free CA as an alternative to Let's Encrypt](https://scotthelme.co.uk/introducing-another-free-ca-as-an-alternative-to-lets-encrypt/)
- [Official Certbot Installation Recommends to Install via Snap](https://certbot.eff.org/lets-encrypt/ubuntubionic-nginx)

### [Docker 20.10.0](https://docs.docker.com/engine/release-notes/)

## OurCompose Developments

### Service Version Bumps

- Bookstack: `0.29.3`
- Portal: `1.4.2`
- CommandCenter: `1.7.3`

### [Jekyll Build Timeout Increase](https://gitlab.com/compositionalenterprises/role-compositional/-/commit/eafc679e252bf2328d8a43f56ec301f68eaec02b)

## [Integration Discussion - Kanboard - Application Interface](https://compositionalenterprises.ourcompose.com/bookstack/books/kanboard/chapter/application-interface)

- [Dashboard](https://compositionalenterprises.ourcompose.com/bookstack/books/kanboard/page/dashboard)
- [Main Board View](https://compositionalenterprises.ourcompose.com/bookstack/books/kanboard/page/main-board-view)
- [New Task Prompt](https://compositionalenterprises.ourcompose.com/bookstack/books/kanboard/page/new-task-prompt)
- [Task Detail Page](https://compositionalenterprises.ourcompose.com/bookstack/books/kanboard/page/task-detail-page)
- [Mobile](https://compositionalenterprises.ourcompose.com/bookstack/books/kanboard/page/mobile)

## Grab Bag - Permission Marketing - Turning Strangers into Friends and Friends into Customers

### Overview

> In this book I’d like to challenge your preconceived notions about what marketing and advertising is and should be and put it back together in a way that works in our new networked world. The concepts are pretty simple, but they are by no means obvious.

You're not paying attention. Nobody is.
It's not your fault. It's just physically impossible for you to pay attention to everything the markets expect you to.

Someone in an airport walks up to you and asks you where gate 7 is. Obviously you weren't hoping for, or expecting, someone to come up and ask this question, but since he looks nice enought, you point him in the right direction. Now imagine the same airport, but its three in the afternoon and youre late for your flight. Youve already been approached 5 times, odds are your response will be a lot different. You might ignore them all together. What if he's the 5th, 10th, 50th, 100th person to do this. Sooner or later it all becomes background noise.

The problem is, that marketers have responded to this problem with the single worst cure possible. To deal with the clutter and the diminished effectieness of Interruption Marketing, they are interrupting us even more. 

#### Consumers are spending less time seeking alternative solutions

People don't need to care as much as they used to. The quality of products has increased dramatically. Ninety years ago we made stuff, we didn't buy it. 

#### Problems with interruption Marketing
- Human beings have a finite amount of attention
- Human beings have a finite amount of money
- The more products offered, the less money there is to go around
- In order to capture more attention and more money, Interruption Marketers must increase spending
- But this increase in marketing costs big money
- Spending more and more money in order to get bigger returns leads to even more clutter
- Catch22 The more they spend, the less it works. The less it works the more they spend.


### Permissive Marketing

But in today's free market, there are plenty of factories, plenty of brands, and way too many choices.
With just a little effort and a little savings, we can get almost anything we want. 

Need two things in order to have an economy: people who want things, and scarcity of the thing they want. 

Permissive marketing: offers the consumer an opportunity to volunteer to be marketed to. It allows marketers to tell their story more calmly and succinctly without fear of being interrupted by competitors or Interruption Marketers. It serves both consumers and marketers in a symbiotic exchange.

The five steps to dating your customer:
- Offer the prospect an incentive to volunteer
- Using the attention offered by the prospect, offer a curriculum over time, teaching the consumer about your product or service.
- Reinforce the incentive to guarantee that the prospect maintains the permission.
- Offer additional incentives to get even more permission from the customer.
- Over time, leverage the permission to change consumer behavior toward profits.

Permission Marketing requires patience.

### Random

A single ad is not enough to sell a product. (5)

Marketing guru Jay Levinson figures you have to run an ad 27 times against one individual before it has its desired impact. Why? Because only one out of nine sinks in, and youve got to see if at least three times before it sinks in.
