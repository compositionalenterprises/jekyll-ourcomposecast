---
title: Firefly-III for Personal Finances
service: Firefly
tagline: The guys light up talking about Firefly-III, and then take a deep dive into the four freedoms.
chapters:
  - start: '00:00:00'
    title: 'Introduction'
  - start: '00:08:29'
    title: 'News/Community Updates'
  - start: '00:09:29'
    title: 'OurCompose Developments'
  - start: '00:15:23'
    title: 'Integration Discussion - Firefly-III - Overview'
  - start: '00:36:30'
    title: "Grab Bag - Free as in..."
---

## Intro

### [Frontman](https://github.com/algolia/frontman)

> Frontman is a static site generator written in Ruby, optimized for speed. It helps you convert your content to static HTML files, so you can focus on your content instead of maintaining servers.

### Memberful is Everywhere, or how Andrew might be suffering from the [Baader-Meinhof Phenomenon](https://science.howstuffworks.com/life/inside-the-mind/human-brain/baader-meinhof-phenomenon.htm)

## News / Community Updates

## OurCompose Developments

### OurComposeCast Updates
- Check out our new darkmode!
- [Youtube Channel](https://www.youtube.com/channel/UCAndVtCCBBxbztJLiGYBfTQ) also goes live!
- [Ourcomposecast.com](https://ourcomposecast.com)

### [OurCompose Services Get Health Checks](https://gitlab.com/compositionalenterprises/role-compositional/-/merge_requests/42)

## [Integration Discussion - Firefly-III](https://compositionalenterprises.ourcompose.com/bookstack/books/firefly-iii/page/overview)

## Grab Bag - Free as in...

_Putting the 'F' in FLOSS_

### Why Freedom?

![Futurama Freedom Song and Dance](https://i.redd.it/34ihwbxlhj7z.jpg)

Because of the reasons for [Voluntaryism](https://en.wikipedia.org/wiki/Voluntaryism):
- [Self-Ownership](https://en.wikipedia.org/wiki/Self-ownership)
- [Non-Aggression](https://en.wikipedia.org/wiki/Non-aggression_principle)

### [The Ambiguity of Free](https://en.wikipedia.org/wiki/Gratis_versus_libre)

- Libre
- Gratis

### [Richard Stallman's Four Freedoms](https://www.gnu.org/philosophy/free-sw.html)

- The freedom to run the program as you wish, for any purpose (freedom 0).
- The freedom to study how the program works, and change it so it does your computing as you wish (freedom 1). Access to the source code is a precondition for this.
- The freedom to redistribute copies so you can help others (freedom 2).
- The freedom to distribute copies of your modified versions to others (freedom 3). By doing this you can give the whole community a chance to benefit from your changes. Access to the source code is a precondition for this.

### [Stewart Brand's Information Wants to be Free](https://en.wikipedia.org/wiki/Information_wants_to_be_free)

> On the one hand information wants to be expensive, because it's so valuable. The right information in the right place just changes your life. On the other hand, information wants to be free, because the cost of getting it out is getting lower and lower all the time. So you have these two fighting against each other.

### [Free Software vs Open Source](https://www.digitalocean.com/community/tutorials/Free-vs-Open-Source-Software)

> For the “free software” camp, though, “open source” doesn’t fully convey the importance of the movement and the potential long-term social problems caused by proprietary software.

### OurCompose Retires Manager

> [Manager is not open source software. Just because the desktop edition is free does not make the software open source.](https://forum.manager.io/t/cant-see-where-to-download-the-source-code/9683/2)

- [Remove Manager from Compositional Role](https://gitlab.com/compositionalenterprises/role-compositional/-/merge_requests/43)
- [Remove Manager from Playbooks and Scripts](https://gitlab.com/compositionalenterprises/play-compositional/-/merge_requests/11)

_Coming soon..._ [Akaunting](https://akaunting.com/blog/its-a-new-dawn-new-day-new-version-akaunting-20)
