---
title: Boards, Boards Everywhere
service: Kanboard
tagline: Andrew and Jack start to discuss Nextcloud, and wrap up their discussio on Docker.
chapters:
  - start: '00:00:00'
    title: 'Introduction'
  - start: '00:00:23'
    title: 'News/Community Updates'
  - start: '00:08:40'
    title: 'OurCompose Developments'
  - start: '00:16:30'
    title: 'Integration Discussion - Kanboard - Initial Configuration - Nextcloud - Deck'
  - start: '00:30:37'
    title: 'Grab Bag - Docker'
---

## News / Community Updates

### Service Versions

| Service       | latest        | master        | stable-2.4    | stable-2.3      | stable-2.2      |
|---------------|---------------|---------------|---------------|-----------------|-----------------|
| database      | 10.5.4        |               |               |                 |                 |
| manager       | 20.8.41       | 20.8.41       | 20.8.41       | 19.7.71         | 19.7.71         |
| jekyll        | 4.1.0         | 4.0           | 4.0           | 3.8             | 3.8             |
| bookstack     | 0.29.3        | 0.28.3        | 0.28.3        | 0.27.5          | 0.27.5          |
| bitwarden     | 1.16.3        | 1.15.1        | 1.15.1        | 1.14.1-alpine   | 1.14.1-alpine   |
| kanboard      | v1.2.15       | v1.2.15       | v1.2.15       | v1.2.8          | v1.2.8          |
| nextcloud     | 19.0.1        | 18.0          | 18.0          | 18              | 18              |
| wordpress     | 5.4.2         | 5.3           | 5.3           | 5.2.2           | 5.2.2           |
| firefly       | release-5.3.3 | release-5.2.8 | release-5.2.8 | release-4.8.0.3 | release-4.8.0.3 |
| rundeck       | 3.3.0         | 3.2.9         | 3.2.9         | 3.1.4           | 3.1.4           |
| mysql         | 10.5.4        |               |               |                 |                 |
| portal        | 1.3.0         | 1.3.0         | 1.3.0         | 1.2.2           |                 |
| commandcenter | 1.4.0         | 1.4.0         | 1.4.0         |                 |                 |

### [Have I Been Pwned Code Base is being Open Sourced](https://www.troyhunt.com/im-open-sourcing-the-have-i-been-pwned-code-base/)

### [Jitsi-Meet is now on Flathub](https://flathub.org/apps/details/org.jitsi.jitsi-meet)

[Under the Hood -- Flatpak documentation](https://docs.flatpak.org/en/latest/under-the-hood.html)

## OurCompose Developments

### [Static assets are now being served by the frontend proxy](https://medium.com/kokster/mount-volumes-into-a-running-container-65a967bee3b5)

### [When migrating, only the proxy and the DB are instantiated before loading the data](https://gitlab.com/compositionalenterprises/play-compositional/-/commit/83c680ce274e17f54b0f696299a993a360173e90)

## Integration Discussion

### [Kanboard - Initial Configuration](https://compositionalenterprises.ourcompose.com/bookstack/books/kanboard/chapter/initial-configuration)

### [Nextcloud - Deck](https://compositionalenterprises.ourcompose.com/bookstack/books/nextcloud/page/deck-a66)

## Grab Bag -- Docker (2/2)

Docker, a wrapper tool for containers. 

### Images and Containers

Dockerfiles are just the start of the docker world.
Dockerfiles describe how to assemble a private filesystem for a container, and describe the metadata on how to run a container based on an image. 

When I think of creating a docker image, there are a few things that immediately jump to my mind:

  - [`FROM ALPINE`](https://alpinelinux.org/about/)
  - `COPY . /usr/src/app`
  - `RUN`
  - `EXPOSE`
  - `CMD`

### Storage: Volumes, Bind Mounts, Tempfs mounts, named pipes
  - Volumes: Sharing Data among Multiple Containers, volumes help decouple configure the configuration of the dockerhost from the container, when you want to store container data on a remote host rather than locally,
  - Bind Mounts: Sharing configuration files from the host machine to the container (i.e. `/etc/resolv.conf`), sharing source code between the host and the container, when the file and directory structure is guaranteed to be consistent with the bind mounts the containers require
  - Tempfs Mounts: when you do not want data to be persistent on the host or the container, maybe for security reasons, or to protect performance


### Networking
  - **User-defined bridge networks** are best when you need multiple containers to communicate on the same Docker host.
  - **Host networks** are best when the network stack should not be isolated from the Docker host, but you want other aspects of the container to be isolated.
  - **Overlay networks** are best when you need containers running on different Docker hosts to communicate, or when multiple applications work together using swarm services.


### Docker at OurCompose

Diving a little into our current infrastructure, we spin up VMs from a cloud provider and then run Docker Containers of requested services on each instance. The base reproducibility allows us to easily spin up new instances of services for every new customer that joins the platform.

With the use of Docker volumes we are also able to easily manage storage among our containers and keep data persistent.

Using the docker network, we are able to spin up an NGINX container as a frontend server, call it our proxy server to our other containers and use the internal Docker network to communicate among containers. 

This means we are able to run all our applications off of port `80` and `443` at subfolders such as `/nextcloud` and `/kanboard`.

#### Images and Pipelines

[Rails Example Dockerfile](https://gitlab.com/compositionalenterprises/commandcenter/-/blob/master/Dockerfile)

With this Rails image, after I defined the container requirements from a runtime perspective I was able to use Gitlab pipelines to continuously test any new commits to the repo testing for breaking changes. 

Essentially on every commit to the project we have pipelines build the image so we can use it in our next deploy. If the build fails, rather than deploying the failed image to production we are able to look at what went wrong and fix our issue.

#### Slimming down an Image with a MultiStage Builds

With multi-stage builds, you use multiple FROM statements in your Dockerfile. Each FROM instruction can use a different base, and each of them begins a new stage of the build. You can selectively copy artifacts from one stage to another, leaving behind everything you don’t want in the final image.

### Conclusion

Docker isn't going anywhere.
We are only going to see more usage of containers across environments in all sized organizations.
