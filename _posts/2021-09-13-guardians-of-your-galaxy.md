---
title: Guardians of Your Galaxy
service: Bitwarden
tagline: The guys drop more news than ever, and go over how to get rid of users, whether you're ProtonMail or Bitwarden, in an Agile manner.
chapters:
  - start: '00:00:17'
    title: 'Introduction'
  - start: '00:22:38'
    title: 'News/Community Updates'
  - start: '00:34:31'
    title: 'OurCompose Developments'
  - start: '00:45:59'
    title: 'Integration Discussion - Bitwarden - User Management'
  - start: '01:06:21'
    title: 'Grab Bag - Agile Manifesto'
---

## Intro

- [Protonmail Legal Request](https://twitter.com/tenacioustek/status/1434604102676271106)
  - [ProtonMail deletes 'we don't log your IP' boast from website after climate activist reportedlty arrested](https://www.theregister.com/2021/09/07/protonmail_hands_user_ip_address_police/)
- [Docker Updating Product Subscriptions](https://www.docker.com/blog/updating-product-subscriptions/)
- [30 years of Linux: OS was successful because of how it was licensed, says Red Hat](https://www.theregister.com/2021/08/25/30_years_of_linux_red_hat/)

## News / Community Updates

- [Nextcloud Sync brings 10x faster syncing](https://nextcloud.com/blog/nextcloud-sync-2-0-brings-10x-faster-syncing/)
- [FireflyIII Move](https://www.reddit.com/r/FireflyIII/comments/pjsbp1/yay_weve_moved_please_find_firefly_iii_support/)
- [Bookstack Security Release](https://www.bookstackapp.com/blog/bookstack-release-v21-08-2/)
- [AnsibleFest 2021 September 29-30](https://www.ansible.com/ansiblefest)

## OurCompose Developments

- [Local/Rundeck Execution Abstraction](https://gitlab.com/compositionalenterprises/portal/-/merge_requests/131)
- [Maintain Demo Instance of Latest Stable](https://demo.ourcompose.com)
  - [Roadmap Task](https://compositionalenterprises.ourcompose.com/kanboard/project/10/task/1299)
- [Portal logs should not show health checks](https://gitlab.com/compositionalenterprises/portal/-/merge_requests/132)
- Amazon Podcasts
- [Completed Complexity configurable DOW to end on](https://github.com/kanboard/website/pull/286)
- [OurCompose Integration Sessions - Bitwarden Collaboration](https://youtu.be/eo8DH75XI40)

## Integration Discussion - Bitwarden - [User Management](https://compositionalenterprises.ourcompose.com/bookstack/books/bitwarden/chapter/user-management)

## Grab Bag - Agile Manifesto?

### [The Agile Manifesto](http://users.jyu.fi/~mieijala/kandimateriaali/Agile-Manifesto.pdf)

> Facilitating change is more effective than attempting to prevent it. Learn to trust in your ability to respond to unpredictable events; it's more important than trusting in your ability to plan for disaster.

### [Agile Manifesto](https://agilemanifesto.org/):

> Manifesto for Agile Software Development
> We are uncovering better ways of developing software by doing it and helping others do it. Through this work we have come to value:
>
> - *Individuals and interactions* over processes and tools
> - *Working software* over comprehensive documentation
> - *Customer collaboration* over contract negotiation
> - *Responding to change* over following a plan
>
> That is, while there is value in the items on the right, we value the items on the left more.

### [Principles Behind the Manifesto](https://agilemanifesto.org/principles.html):

> Our highest priority is to satisfy the customer through early and continuous delivery of valuable software.
>
> Welcome changing requirements, even late in development. Agile processes harness change for the customer's competitive advantage.
>
> Deliver working software frequently, from a couple of weeks to a couple of months, with a preference to the shorter timescale.
>
> Business people and developers must work together daily throughout the project.
>
> Build projects around motivated individuals. Give them the environment and support they need, and trust them to get the job done.
>
> The most efficient and effective method of conveying information to and within a development team is face-to-face conversation.
>
> Working software is the primary measure of progress.
>
> Agile processes promote sustainable development. The sponsors, developers, and users should be able to maintain a constant pace indefinitely.
>
> Continuous attention to technical excellence and good design enhances agility.
>
> Simplicity--the art of maximizing the amount of work not done--is essential.
>
> The best architectures, requirements, and designs emerge from self-organizing teams.
>
> At regular intervals, the team reflects on how to become more effective, then tunes and adjusts its behavior accordingly.


### [Is the Agile Manifesto Still a thing?](https://www.atlassian.com/agile/manifesto)

When something as culturally important as the Manifesto comes along, you might be able to reinterpret it, but there’s nothing quite like the original. So, instead of trying to officially update it, maybe it’s best to figure out how to apply it to yourself, your team, or your organization.

“In a lot of ways, the Manifesto is a basis of a conversation,” Wortham said. “Here’s how I interpret it. How do you interpret it? All right, let’s figure out how to work together.”

In that vein, perhaps what's important isn't one blessed document that everyone can agree on, but whether or not a group of people (from a team to an entire organization) can apply the ideas in the Manifesto to their specific situation without losing sight of its spirit. And if we can do that well, the possibilities are unlimited.

“I think if we do it right, the world is in a position to be amazing. We can solve cancer. My children will probably live to be 150, to 175,” West said. “I think that we can do it, and I think that we will.”
