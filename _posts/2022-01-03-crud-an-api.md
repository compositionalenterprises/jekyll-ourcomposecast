---
title: CRUD an API
service: Rundeck
tagline: This week we discuss self hosting and updates around the ecosystem, the Rundeck API, and coming back from the holidays. 
chapters:
  - start: '00:00:00'
    title: 'Intro - News - Updates'
  - start: '00:35:38'
    title: 'Integration Discussion - Rundeck - API'
  - start: '00:50:06'
    title: 'Grab Bag - Bouncing Back from the Holidays'
---

## Intro

- [Meet the Self-Hosters, Taking Back the Internet One Server at a Time](https://www.vice.com/en/article/pkb4ng/meet-the-self-hosters-taking-back-the-internet-one-server-at-a-time)
- [Minimum viable action – how to advance things that are stuck](https://www.eficode.com/blog/minimum-viable-action-how-to-advance-things-that-are-stuck)

## News / Community Updates

- [SuiteCRM - Maintenance Patch](https://suitecrm.com/maintenance-patch-suitecrm-8-7-12-7-10-released/)
- [Bookstack Release v21.12](https://www.bookstackapp.com/blog/bookstack-release-v21-12/)
- [Firefly-III CSV Importer 3.0.0](https://github.com/firefly-iii/csv-importer/releases/3.0.0)
- [Kanboard 1.2.21](https://github.com/kanboard/kanboard/releases)
- [Rundeck - Secuirty Advisories](https://docs.rundeck.com/docs/history/CVEs/#log4shell-cves)
- [Rundeck - Papadum gold globe](https://docs.rundeck.com/docs/history/3_4_x/version-3.4.9.html)
## OurCompose Developments

### Instance Features
- Portal Application Views - Logs
- [Jekyll Full-Site Bind Mountpoints](https://gitlab.com/compositionalenterprises/ansible-collection-compositionalenterprises.ourcompose/-/merge_requests/60/diffs)
- [A Reboot Borks the Instance](https://gitlab.com/compositionalenterprises/ansible-collection-compositionalenterprises.ourcompose/-/merge_requests/64/diffs)

### Service Resiliancy
- Ongoing Log4j Remediation [Rundeck 3.17](https://docs.rundeck.com/docs/history/3_3_x/version-3.3.17.html)

### Engagement

### Administrative
- Portal & CC New Branches

## Integration Discussion - [Rundeck- API](https://compositionalenterprises.ourcompose.com/bookstack/books/rundeck-d07/page/api)

## Grab Bag - Bouncing Back from the Holidays

### What qualifies?

- Vacations
- Slacking off

### Types of work

When bouncing back from a vacation, you have to identify:
1. Skippable
2. Unskippable

### Kanban vs Calendar - How to schedule for success

Calendar is for skippable events.

Kanban is for unskippable scoped work.
