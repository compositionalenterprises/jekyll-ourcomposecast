---
title: Rubber Hose Attack Vector
service: Nextcloud
tagline: Caring for people is a must, whether it's caring where their telemetry goes, what crypto they use, or encryption they use for their files.
chapters:
  - start: '00:00:00'
    title: 'Introduction'
  - start: '00:30:30'
    title: 'News/Community Updates'
  - start: '00:33:15'
    title: 'OurCompose Developments'
  - start: '00:38:15'
    title: 'Integration Discussion - Nextcloud - Encryption'
  - start: '01:23:45'
    title: 'Grab Bag - Ways to Make People Like You'
---

## Intro

- [Audacity opts to add telemetry capture](https://www.theregister.com/2021/05/07/audacity_telemetry/)
- [What is Bitcoin Cash?](https://www.cnn.com/2021/05/06/investing/bitcoin-cash-explainer/index.html)

## News / Community Updates

- [WP Briefing](https://wordpress.org/news/feed/podcast)

## OurCompose Developments

- [Add nginx timeout settings for nextcloud large files](https://gitlab.com/compositionalenterprises/role-compositional/-/merge_requests/65)
- Allow local environment to be displayed in Portal

## [Integration Discussion - Nextcloud - Encryption](https://compositionalenterprises.ourcompose.com/bookstack/books/nextcloud/page/encryption)

- [Encryption and hardening](https://nextcloud.com/encryption/)

## Grab Bag - How to Win Friends and Influence People - Part Two

### Ways to make people like you

### Do this and You'll be welcome anywhere

Showing a genuine interest in others not only wins friends for you, but may develop in its customers a loyalty to your company. 

If we want to make friends, let’s greet people with animation and enthusiasm. When somebody calls you on the telephone use the same psychology. Say “Hello” in tones that bespeak how pleased YOU are to have the person call. Many companies train their telephone operators to greet all callers in a tone of voice that radiates interest and enthusiasm. The caller feels the company is concerned about them. Let’s remember that when we answer the telephone tomorrow.

We are interested in others when they are interested in us.

**Principle 1- Become genuinely interested in other people.**

### A simple way to make a good first impression

The effect of a smile is powerful - even when it's unseen. "Your smile comes through in your voice."

> The chairman of the board of directors of one of the largest rubber companies ‘in the United States told me that, according to his observations, people rarely succeed at anything unless they have fun doing it.

> had a rip-roaring good time conducting their business. Later, I saw those people change as the fun became work. The business had grown dull, They lost all joy in it, and they failed.” You must have a good time meeting people if you expect them to have a good time meeting you.

You don’t feel like smiling? Then what? Two things. First, force yourself to smile. If you are alone, force yourself to whistle or hum a tune or sing. Act as if you were already happy, and that will tend to make you happy. Here is the way the psychologist and philosopher William James put it:

“Action seems to follow feeling, but really action and feeling go together; and by regulating the action, which is under the more direct control of the will, we can indirectly regulate the feeling, which is not. 

“Thus the sovereign voluntary path to cheerfulness, if our cheerfulness be lost, is to sit up cheerfully and to act and speak as if cheerfulness were already there. . . .”

Every body in the world is seeking happiness - and there is one sure way to find it. That is by controlling your thoughts. Happiness doesn’t depend on outward conditions. It depends on inner conditions.

**Principle 2 - Smile**

### If you don't do this you are headed for trouble

**PRINCIPLE 3 -Remember that a person’s name is to that person the sweetest and most important sound in any language.**

### An easy way to be a good conversationalist

I've never been to Africa...

A person’s toothache means more to that person than a famine in China which kills a million people. A boil on one’s neck interests one more than forty earthquakes in Africa. Think of that the next time you start a conversation.

**PRINCIPLE 4 -Be a good listener. Encourage others to talk about themselves.**

### How to interest people

Everyone who was ever a guest of Theodore Roosevelt was astonished at the range and diversity of his knowledge. Whether his visitor was a cowboy or a Rough Rider, a New York politician or a diplomat, Roosevelt knew what to say. And how was it done? The answer was simple. Whenever Roosevelt expected a visitor, he sat up late the night before, reading up on the subject in which he knew his guest was particularly interested.

For Roosevelt knew, as all leaders know, that the royal road to a person’s heart is to talk about the things he or she treasures most.

**Principle 5 - Talk in terms of the other person's interestes.**

### How to make people like you instantly

Dog Breeder Example. Guy won all these prizes for breeding. Man continued to ask questions, and more questions, and eventually the breeder asked if the man wanted a dog for his son.

Make the other person feel important, and do it sincerely.
