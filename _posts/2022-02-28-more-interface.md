---
title: More Interface
service: Firefly-III
tagline: This week we discuss Project Zero Security regarding Open Source, FireflyIII's Application Interface, and wrap up with an interview on Salesforce Administration.
chapters:
  - start: '00:00:00'
    title: 'Intro - News - Updates'
  - start: '00:14:50'
    title: 'Integration Discussion - Firefly-III - Application Interface, Other'
  - start: '00:32:38'
    title: 'Grab Bag - Interviews of Change - Jared Kirk - Salesforce'
---

## Intro
- [Contributing to Bookstack (And Open Source)](https://www.bookstackapp.com/blog/contributing-to-open-source/)
- [A walk through Project Zero metrics](https://googleprojectzero.blogspot.com/2022/02/a-walk-through-project-zero-metrics.html)

## News / Community Updates
- [Wordpress 5.9 Josephine](https://wordpress.org/news/2022/01/josephine/)
- [SuiteCRM 7.12.4](https://suitecrm.com/suitecrm-7-12-4-released/)
- [Nextcloud 23.0.2, 22.2.5, 21.0.9](https://nextcloud.com/blog/update-now-23-0-2-22-2-5-and-21-0-9/)
- [Kanboard 1.2.22](https://github.com/kanboard/kanboard/releases/tag/v1.2.22)
- [Firefly-III 5.6.14](https://github.com/firefly-iii/firefly-iii/releases/5.6.14)
- [Dolibarr ERP & CRM V15 Released](https://www.dolibarr.org/dolibarr-erp-crm-v15-has-been-released.php)

## OurCompose Developments

### Content

### Portal UI/UX

### Administrative
- Journal logs are disabled on boot
- Logical backups are being deleted all but one from mysql
- Deploys fail because of cloud-init
- Stable-4.1

## [Integration Discussion - Firefly-III - Application Interface, Other](https://compositionalenterprises.ourcompose.com/bookstack/books/firefly-iii/page/others)

## Interviews of Change - Jared Kirk - Salesforce
