---
title: Decking Unemployment
service: Rundeck
tagline: Heavy hitters hit the deck while OurCompose automation runs both on localhost and on Rundeck up and down the yellow brick road with the OZ Principle!
chapters:
  - start: '00:00:45'
    title: 'Introduction'
  - start: '00:15:07'
    title: 'News/Community Updates'
  - start: '00:20:48'
    title: 'OurCompose Developments'
  - start: '00:30:27'
    title: 'Integration Discussion - Rundeck - Jobs'
  - start: '01:01:15'
    title: 'Grab Bag - The OZ Principle'
---

## Intro

- [Sinch to acquire Mailgun](https://www.mailgun.com/blog/sinch-to-acquire-pathwire/)
- [Docker Compose v2 Engine, Go](https://github.com/docker/compose/releases/tag/v2.0.1)
- [Understanding How Facebook Disappeared from the Internet](https://blog.cloudflare.com/october-2021-facebook-outage/)
- [Twitch confirms major data breach after its source code and secrets leak out](https://www.theverge.com/2021/10/6/22712365/twitch-data-leak-breach-security-confirmation-comments)

## News / Community Updates

- [Firefly-III 5.6.1](https://github.com/firefly-iii/firefly-iii/releases/5.6.1)
- [Jekyll 4.2.1](https://jekyllrb.com/news/2021/09/27/jekyll-4-2-1-released/)
- [Fresh updates here: 22.2.0, 21.0.5 and 20.0.13. Desktop client 3.3.5 is also out!](https://nextcloud.com/blog/fresh-updates-here-22-2-0-21-0-5-and-20-0-13/)

## OurCompose Developments

- [Portal Build Pipeline - Change MTU](https://gitlab.com/compositionalenterprises/portal/-/merge_requests/134)
- Streaming input from Rundeck Data
- [OurCompose instance running on localhost](https://compositionalenterprises.ourcompose.com/kanboard/project/10/task/1937)
  - [Project](https://gitlab.com/compositionalenterprises/ansible-project-ourcompose_management/-/merge_requests/13)
  - [Portal](https://gitlab.com/compositionalenterprises/portal/-/merge_requests/137)
  - [Collection](https://gitlab.com/compositionalenterprises/ansible-collection-compositionalenterprises.ourcompose/-/merge_requests/22)
  - [Quickstart](https://gitlab.com/compositionalenterprises/ansible-collection-compositionalenterprises.ourcompose/-/tree/master#quickstart)

## [Integration Discussion - Rundeck - Jobs](https://compositionalenterprises.ourcompose.com/bookstack/books/rundeck-d07/page/jobs)

## Grab Bag - The OZ Principle, Getting Results Through Individual and Organizational Accountability

> Victor Hugo once said, There is one thing stronger than all the armies in the world, and that is an idea whose time has come.

Accountable - rising above the circumstances and doing whatever it takes to get the results desired
### Above the line and Below the line:

A thin line separates success from failure, the great companies from the ordaniry ones. Below the line lies excuse making, blaming others, confusion, and an attitude of helplessness, while above that line lies a sense of reality, ownership, commitment, solutions to problems, and determined action.

Above the line:
<br>
See it, Own it, Solve it, Do it
<br>
Below the line:
<br>
Ignore/Deny, It's not my job, finger pointing, confusion and tell me what to do, cover your tail, wait and sit

People and Organizations find themselves thinking and behaving below the line whenever they consciously or uncounsciously avoid accountability for individual or collective results. 

### Part 1: The Oz Principle: Getting Results Through Accountability

Like all powerful literature, The Wizard of Oz continues to enthrall audiences because its plot strikes a nerve in people. The book recounts a journey toward awareness, and, from the beginning of their journey, the story’s main characters gradually learn that they possess the power within themselves to get the results they want. Until the end, they think of themselves as victims of circumstance, skipping down the yellow brick road to the Emerald City where the supposedly all-powerful Wizard will grant them the wisdom, heart, courage, and means to succeed. The journey itself empowers them, and even Dorothy, who could have clicked her red slippers and returned home at any time, must travel the yellow brick road to gain full awareness that only she herself can achieve her desires. People relate to the theme of a journey from ignorance to knowledge, from fear to courage, from insensitivity to caring, from paralysis to powerfulness, from victimization to accountability, because it seems so true. Unfortunately, even the most ardent admirers of the story often fail to learn its simple lesson, never getting off the yellow brick road, blaming others for their circumstances, and waiting for wizards to wave their magic wands and make all the problems disappear. 

In fact, the temptation to feel and act like victims has become so popular in America that it has created a very real crisis.

The majority of people in organizations today, when confronted with poor performance or unsatisfactory results, immediately begin to formulate excuses, rationalizations, and arguments for why they cannot be held accountable, or, at least, not fully accountable for the problems.

The latest, most up-to-date management concepts and techniques won’t help if you’ve neglected the basic principles that empower people and organizations to turn in exceptional performances.

#### Victimization vs Accountability

The greatest destruction visited upon our society by the current cult of victimization stems from its subtle dogma that people cannot become what they desire to become because of their circumstance. 

In essence, this attitude of victimization prevents a person from growing and developing.

In fact, many people in American organizations, wanting to feel good about themselves when results don’t materialize, would rather offer excuses for why they didn’t get the expected results than find ways to overcome the obstacles keeping them from those results.

People tend to remain in the victim cycle because they find certain comforting, if not self-defeating, rewards below-the-line. Such rewards include "I dont have to admit I was wrong", "I wont lose face", I dont have to do anything differently in the future" and "I can justify my lack of performance and growth."

Those obsessed with the past are ignorant of the future.

### Part 2: The Power of Individual Accountability: Moving yourself above the line

See it- involves recognizing and and acknowledging the full reality of a situation (hardest step/greatest hurdle because it's so hard to undertake an honest self-appraisal and acknowledge that you can do more to get results)
<br>
Own it- means accepting responsibility for the experiences and realities you create for yourself and others. (this step paves the road to action)
<br>
Solve it- entails changing reality by finding and implementing solutions to problems that you may not have thought of before, while avoiding the trap of falling below the line
<br>
Do it- entails mustering the commitment and courage to follow-through with the solutions you have identified, even if those solutions involve a lot of risk.

Success in business boils down to one simple principle: you can either get stuck or get results. Period. Case Closed.

### Part 3: Results Through Collective Accountability: Helping Your Organization Perform Above the Line

> You can be sure our plan was perfect - it's just that the assumptions were wrong.
