---
title: Akaunting For a New Service
service: Akaunting
tagline: With a few new services added to the suite, we are taking a quick break from Rundeck to cover Akaunting and a dive into The Righteous Mind.
chapters:
  - start: '00:00:00'
    title: 'Introduction'
  - start: '00:17:58'
    title: 'News/Community Updates'
  - start: '00:32:28'
    title: 'OurCompose Developments'
  - start: '00:33:37'
    title: 'Integration Discussion - Akaunting - Overview'
  - start: '00:51:37'
    title: 'Grab Bag - The Righteous Mind'
---

## Intro

- [DST Root CA X3 Expiration](https://letsencrypt.org/docs/dst-root-ca-x3-expiration-september-2021/)
  - [HTTPS Is Actually Everywhere](https://www.eff.org/deeplinks/2021/09/https-actually-everywhere)
- [What your web browser says about you](https://www.news.com.au/technology/online/what-your-web-browser-says-about-you/news-story/c577c19e272aadaa18bc82fe2a456957)

## News / Community Updates

- [Nextcloud Now Officially Supported on TrueNAS](https://nextcloud.com/blog/nextcloud-now-officially-supported-on-truenas/)
- [Bookstack Services we Use](https://www.bookstackapp.com/blog/services-we-use/)
- [Bookstack Security Release](https://www.bookstackapp.com/blog/bookstack-release-v21-08-5/)
- [Firefly-III 5.6.2](https://github.com/firefly-iii/firefly-iii/releases/tag/5.6.2)
- [Rundeck 3.4.5](https://docs.rundeck.com/docs/history/3_4_x/version-3.4.5.html#overview)

## OurCompose Developments

- [SuiteCRM](https://suitecrm.com)
- [Akaunting](https://akaunting.com)

## Integration Discussion - [Akaunting - Overview](https://compositionalenterprises.ourcompose.com/bookstack/books/akaunting/page/overview)

## Grab Bag - The Righteous Mind

> I could have titled this book _The Moral Mind_ ...
> But I chose _The Righteous Mind_ to convey the sense that human nature is not just intrinsically moral,
> it's also intrinsically moralistic, critical, and judgemental.

We can seek to get out of this how to persuade and influence people (for their own benefit) as a result of understanding how people think.

### Part 1 - Intuitions Come First, Strategic Reasoning Second

> Intuitions come first, strategic reasoning second

- Social Intuitionist Model
  - Moral reasons are the tail wagged by the intuitive dog
- Imagine yourself as a small rider on a very large elephant
  - Brains evaluate instantly and constantly
  - Social and political judgements depend heavily on quick intuitive flashes
  - Intuitions can be shaped by reasoning, especially when reasons are embedded in a friendly conversation or an emotionally compelling story.
  - When the elephant leans, the rider looks around for a way to support such a move.
- David Hume - Introduced intuitionism as an escape from rationalism
  - Believed that reason was (and was only fit to be) the servant of the passions
- Glaucon - The overriding importance of reputation and other external constraints for moral order
  - Ring of Gyges (The One Ring): _"His actions would be in no way different from those of an unjust person, and both would follow the same path."_
  - "Design institutions in which real human beings, always concerned about their reputations, will behave more ethically."
- Anyone who values truth should stop worshipping reason
  - Our in-house press secretary automatically justifies everything
- Motivated Reasoning
  - When we _want_ to believe something, we ask ourselves, "_Can_ I believe it?"
  - When we _don't_ want to believe something, we ask ourselves, "_Must_ I believe it?"

### Part II - There's More to Morality than Harm and Fairness

> There's more to morality than harm and fairness

- Perceiving additional moral concerns
  - Framed-line task (square with a line drawn inside it)
- Moral pluralism always works contextually rather than moral monism or moral relativism.
- Metaphor that the righteous mind is like a tongue with six taste receptors
  - The True Taste restaurant: sweet receptor produced maximum dopamine, therefore most efficient units of pleasure per calorie, better than salt-tasting.
- Richard Shweder - The moral domain
  - Three clusters of moral themes; Autonomy, Community, and Divinity
- Moral Foundations Theory
  - Care/harm (Liberal)
  - Fairness/cheating (Liberal)
  - Loyalty/betrayal (Conservative)
  - Authority/subversion (Conservative)
  - Sanctity/degradation (Conservative)
  - Liberty/oppression (Conservative)
- Emile Durkheim - Many people value the binding foundations of loyalty, authority and sanctity
  - When someone in a moral community desecrates one of the sacred pillars supporting the community, the reaction is sure to be swift, emotional, collective, and punitive.

### Part III - Morality Binds and Blinds

> Group selection is controversial among evolutionary theorists, some of whom think that anything resembling group-related adaption will turn out to be an adaptation for helping individuals outcompete their neighbors.

- Multilevel selection turned us into _"Homo Duplex"_
  - We are selfish at the same time as we are groupish
  - Human nature is mostly selfish, but with a groupish overlay that resulted from the fact that natural selection works at multiple levels simultaneously.
  - We are 90% chimp and 10% bee
- Religion played a crucial role in our evolutionary history
  - Our religious minds coevolved with our religious practices to create ever-larger moral communities
  - The function of religious beliefs and practices is ultimately to create a community.
- Why some people gravitate to the left and the right.
- How to turn on the hive switch
  - We have the capacity to transcend self-interest and become simply a part of a whole
  - It's the portal to many of life's most cherished experiences
  - These group rituals that evoke collective emotions pull humanity fully but temporarily into the higher of our two realms, the realm of the sacred, where the self disappears and collective interests predominate.
    - Awe in Nature/vastness and a need for accommodation (assimilating experiences into our existing mental structions)
    - Durkheimogens
    - Raves
  - Happiness comes from getting the right relationships between yourself and others, yourself and your work, and yourself and something larger than yourself.
  - To understand the miracle of moral communities that grow ever-larger, we must look not just at people, and not just at the relationships among people, but at the _complete environment_ within which those relationships are embedded, and which makes those people more virtuous.

> The human mind is a story processor, not a logic processor.

When you do bring up issues of morality, try to start with some praise, or with a sincere expression of interest.
