---
title: CommandCenter, OurCompose Style
service: CommandCenter
tagline: Exhaused after a lot of Alpha prep and hard work, Jack kicks off the relaxation by talking about his home-grown CommandCenter, and Andrew has nothing better to do than talk about what he's talking into.
chapters:
  - start: '00:00:00'
    title: 'Introduction'
  - start: '00:02:50'
    title: 'News/Community Updates'
  - start: '00:13:30'
    title: 'OurCompose Developments'
  - start: '00:20:42'
    title: 'Integration Discussion - CommandCenter - Overview'
  - start: '00:32:21'
    title: 'Grab Bag - A/V Rigs'
---

## Intro

- Andrew's not blind!

## News / Community Updates

### [Docker Hub Image Retention Policy Delayed, Subscription Updates](https://www.docker.com/blog/docker-hub-image-retention-policy-delayed-and-subscription-updates/)

### [Nextcloud lets Certificates Expire](https://github.com/nextcloud/server/issues/23853)

## OurCompose Developments

### [Compositional Role 2.5](https://gitlab.com/compositionalenterprises/role-compositional/-/tags?utf8=✓&search=v2.5)

|               | latest        | master        | stable-2.5    | stable-2.4    | stable-2.3      |
|---------------|---------------|---------------|---------------|---------------|-----------------|
| database      | 10.5.7        |               |               |               |                 |
| jekyll        | 4.1.0         | 4.0.1         | 4.0.1         | 4.0           | 3.8             |
| bookstack     | 0.30.4        | 0.28.3        | 0.28.3        | 0.28.3        | 0.27.5          |
| bitwarden     | 1.17.0        | 1.16.3        | 1.16.3        | 1.15.1        | 1.14.1-alpine   |
| kanboard      | v1.2.16       | v1.2.16       | v1.2.16       | v1.2.15       | v1.2.8          |
| nextcloud     | 20.0.1        | 19.0          | 19.0          | 18.0          | 18              |
| wordpress     | 5.5.3         | 5.4           | 5.4           | 5.4           | 5.2.2           |
| firefly       | version-5.4.6 | version-5.4.5 | version-5.4.5 | release-5.2.8 | release-4.8.0.3 |
| rundeck       | 3.3.0         | 3.2.9         | 3.2.9         | 3.2.9         | 3.1.4           |
| mysql         | 10.5.7        |               |               |               |                 |
| portal        | 1.4.1         | 1.4.1         | 1.4.1         | 1.3.2         | 1.2.2           |
| commandcenter | 1.7.1         | 1.7.1         | 1.7.1         | 1.4.3         |                 |


[Playooks Repository User Admins](https://gitlab.com/compositionalenterprises/play-compositional/-/merge_requests/12/diffs)

### [OurCompose Alpha Enrollment](https://ourcompose.com/commandcenter/pricing)

## [Integration Discussion - CommandCenter - Overview](https://compositionalenterprises.ourcompose.com/bookstack/books/commandcenter/page/overview)

## Grab Bag - A/V Rigs

[Podcast Engineering School](https://podcastengineeringschool.com/category/show/)

Gear:
- Andrew:
    - Microphone: [MXL 990](http://www.mxlmics.com/microphones/studio/990/)
    - Audio Interface: [Presonus Audiobox USB 96](https://www.presonus.com/products/AudioBox-USB-96)
    - Studio Monitors: [JBL 3-Series (305P 5")](https://jblpro.com/en-US/products/305p-mkii)
    - Headphones:
        - [Aftershokz Air Bone Conduction](https://us.aftershokz.com/products/air)
        - [AudioTechnica ATH-WS55BRD Over-Ear](https://www.newegg.com/p/0TH-0045-004J0)
- Jack:
    - Microphone: [Shure SMB-7](https://www.shure.com/en-US/products/microphones/sm7b)
    - Audio Interface: [Focusrite scarlet solo](https://focusrite.com/en/usb-audio-interface/scarlett/scarlett-solo)
    - Headphones: [AudioTechnica ATH-M20X](https://www.audio-technica.com/en-us/ath-m20x)

Software:
- [OBS](https://obsproject.com)
- [Audacity](https://www.audacityteam.org)
- [Shotcut](https://www.shotcut.org)
- [Jitsi](https://jitsi.org)
