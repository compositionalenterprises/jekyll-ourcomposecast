---
title: Taking This Nextcloud Show On The Road
service: Nextcloud
tagline: With the best audio quality they can muster, Jack and Andrew prevent accidently deleting files with Nextcloud, and determine how to ask your mom the right questions about Open Source Software.
chapters:
  - start: '00:00:00'
    title: 'Introduction'
  - start: '00:16:00'
    title: 'News/Community Updates'
  - start: '00:28:00'
    title: 'OurCompose Developments'
  - start: '00:33:00'
    title: 'Integration Discussion - Nextcloud - Mobile'
  - start: '00:51:00'
    title: 'Grab Bag - The Mom Test'
---

## Intro

[High Quality Audio Makes you sound Smarter](https://tips.ariyh.com/p/good-sound-quality-smarter)

[Free software becomes a standard in Dortmund, Germany - The Document Foundation Blog](https://blog.documentfoundation.org/blog/2021/04/02/free-software-becomes-a-standard-in-dortmund-germany/)

[Victoria University of Wellington accidentally nukes files on all desktop PCs Ars Technica](https://arstechnica.com/gadgets/2021/03/university-of-wellington-accidentally-deletes-files-on-all-desktop-pcs/)

## News / Community Updates

### [BookStack Release v21.04](https://www.bookstackapp.com/blog/bookstack-release-v21-04/)

### [Firefly-III Release 5.5.7](https://github.com/firefly-iii/firefly-iii/releases/5.5.7)

### [Jekyll 3.9.1 Released](https://jekyllrb.com/news/2021/04/08/jekyll-3-9-1-released/)

### [Kanboard 1.2.19 Released](https://github.com/kanboard/kanboard/releases)

### [Nextcloud 21 Released](https://nextcloud.com/blog/first-21-update-is-out-as-are-minor-20-and-19-releases/)

## OurCompose Developments

### [Portal v1.6.0](https://gitlab.com/compositionalenterprises/portal/-/commit/9ce6e1a18a92002741fa4aa907ce1828314fad59)
- Fix first-run healthcheck issues

## [Integration Discussion - Nextcloud - Mobile](https://compositionalenterprises.ourcompose.com/bookstack/books/nextcloud/page/mobile)

## Grab Bag - The Mom Test

"Trying to learn from customer conversations is like excavating a delicate archaeological site. The truth is down there somewhere, but it’s fragile. While each blow with your shovel gets you closer to the truth, you’re liable to smash it into a million little pieces if  you use too blunt an instrument."

People say you shouldn’t ask your mom whether your business is a good idea. That’s technically true, but it misses the point. You shouldn’t ask anyone whether your business is a good idea. At least not in those words. Your mom will lie to you the most (just ‘cuz she loves you), but it’s a bad question and invites everyone to lie to you at least a little. It’s not anyone else’s responsibility to show us the truth. It’s our responsibility to find it. We do that by asking good questions.

1) Talk about their life instead of your idea
2) Ask about specifics in the past instead of generics or opinions about the future
3) Talk less and listen more


### There are 3 types of Bad data:
1. Compliments
  - Deflect Compliments, you want facts and commitments, not compliments 
  - Avoid them completely by not mentioning your idea
2. Fluff
  - The world's most deadly fluff is: "I would definitely buy that."
  - As a founder you want to believe its money in the bank, but folks are wildly optimistic about what they would do in the future. They are always more positive, excited, and willing to pay in the imagined future than they are once it arrives.
  - Worst kind of question is "Would you ever..?" Of course they might.. That doesn't mean they will.
  - Ask for concrete examples.. instead of "do you ever X?" ask "When's the last time that happened?"
3. Ideas
  - Write them down but don't rush to add them to the "todo list" 
  - When you hear a request, it's your job to understand the motivations which led to it. Dig.

To get toward the truth, you need to reject their generic claims, and fluffy promises. Instead, anchor them towards the life they already lead and the actions they are already taking. 

Selling a pain-killer (solves problem now) vs a Vitamin (nice to have). Asking "Does -this-matter-problem" questions:
  - "How serious do you take this?"
  - "Do you make money from it?"
  - "How much time do you spend per week on this"
  - "Which tools/services do you use for it?"
  - "What are you doing to improve it?"


### Prepare your list of 3

Always pre-plan the 3 most important things you want to learn from any given type of person. 

Focus on the three questions which seem murkiest or most important right now. 


Learning from customers doesn't mean you have to be wearing a suit and drinking boardroom coffee. Asking the right questions is fast and touches on topics people find quite interesting. 

*Rule of Thumb:* If it feels like they're doing you a favor by talking to you, it's probably too formal. 

Early conversations are very fast. The chats grow longer as you move from the early broad questions "Is this a real problem" toward more specific product and industry issues "Which other software do we have to integrate with to close the sale?"

