---
title: Staring at Boards All Day
service: Kanboard
tagline: More Python jabs are thrown, more updates discussed as the boys get deeper into Kanboard. They end with a quick trip down to discuss Bitcoin fundamentals.
chapters:
  - start: '00:00:00'
    title: 'Introduction'
  - start: '00:05:21'
    title: 'News/Community Updates'
  - start: '00:24:26'
    title: 'OurCompose Developments'
  - start: '00:25:42'
    title: 'Integration Discussion - Kanboard - Everyday Usage'
  - start: '00:43:30'
    title: 'Grab Bag - The Basics of Bitcoin - Creating an Elevator Pitch about Bitcoin Fundamentals'
---

## Intro

- [Less known bits of the Python Standard Library](https://medium.com/@vmsp/less-known-bits-of-the-python-standard-library-46dc88490115)

## News / Community Updates

### [Jekyll 4.2.0 Released](https://jekyllrb.com/news/2020/12/14/jekyll-4-2-0-released/)

### Nextcloud Updates
- [Easy migration to Nextcloud from insecure and privacy-unfriendly platforms now available](https://nextcloud.com/blog/easy-migration-to-nextcloud-from-insecure-and-privacy-unfriendly-platforms-now-available/)
- [SalesAgility and Nextcloud announce SuiteCRM integration collaboration](https://nextcloud.com/blog/salesagility-and-nextcloud-announce-suitecrm-integration-collaboration/)

### WordPress Updates
- [WordPress 5.6 "Simone" Released](https://wordpress.org/news/2020/12/simone/)
- [Introducing "Learn WordPress"](https://wordpress.org/news/2020/12/introducing-learn-wordpress/)

### Solarwinds Hack
- [Password Strength](https://pcproactive.co/solarwinds-the-worlds-biggest-security-failure-and-open-sources-better-answer/)
- Supply Chain Attacks
- [Open Source](https://pcproactive.co/solarwinds-the-worlds-biggest-security-failure-and-open-sources-better-answer/)

### [How Long Can a Company Thrive Doing Just One Thing?](https://hbr.org/2020/12/how-long-can-a-company-thrive-doing-just-one-thing)

## OurCompose Developments

### [Randomize Backup Times](https://gitlab.com/compositionalenterprises/role-compositional/-/merge_requests/51)

## [Integration Discussion - Kanboard - Everyday Usage](https://compositionalenterprises.ourcompose.com/bookstack/books/kanboard/chapter/everyday-usage)

## Grab Bag - The Basics of Bitcoin - Creating an Elevator Pitch about Bitcoin Fundamentals

### Sources
- [Bitcoin Whitepaper](https://bitcoin.org/bitcoin.pdf)
- [Bitcoin: A Technical Introduction](https://vimeo.com/27177893)
    - [slides](http://www.lothar.com/presentations/bitcoin-brownbag/master.html)
- [Squaring the Triangle: Secure, Decentralized, Human-Readable Names - Aaron Swartz](http://www.aaronsw.com/weblog/squarezooko)

### Technical Trajectory
- Sending TX
    - Wallet Addresses
    - Mempool
- Hash
    - Cryptographic Function
    - Properties:
        - one way function
        - deterministic
        - random output
- Block
    - Difficulty Target
    - Proof-of-Work
    - Contents
        - List of transactions
        - Previous Block's Hash
        - Random number nonce
- Chain
    - Ledger/Scroll
    - Trivial to verify
    - Append-only

### Problems
- [The (Two) Byzantine Generals Problem](https://www.youtube.com/watch?v=IP-rGJKSZ3s)
    - How do they coordinate to agree upon the correct time?
    - Byzantine Fault Tolerant System
        - Able to resist the class of failures derived from the problem above
        - A system able to fail if some nodes in the system:
            - Fail to communicate
            - Act Maliciously
    - [Satoshi's Email](https://www.mail-archive.com/cryptography@metzdowd.com/msg09997.html)

### Implementation Details
- Block Rewards
    - Initialized at 50 per block
    - Halving every 210,000 blocks (appx. 4 years)
    - 20,999,999.9769 bitcoins ever to be in circulation
    - Newly-created transactions
- Difficulty Adjustment
    - 10-minute block average
- Transaction Fees
    - Block Size
- Hard Forks
    - Most Difficult (Longest) Chain Wins

### Elevator Pitch

Bitcoin is a ledger-based system where transactions are recorded based off of the solution to a cryptographic function.
Each time a solution is found that takes into account all of the transactions the solution is posted publicly for anyone to see.
This allows the users to tell if their transactions have been confirmed or not.
Because it requires a massive amount of work from everyone working on the same problem, it guarantees that everyone agrees on which transactions should be in the ledger, and which ones shouldn't.
The miners that are solving this problem are rewarded with any extra output from both any user transactions and a standard reward for solving the problem.
This standard reward introduces new entries into the ledger, which can be spent as normal.
As a system, the Bitcoin network has agreed that the total rewards ever given out will approach but never exceed 21 million.
As the miners continue to collect rewards by solving these cryptographic problems, the ledger, also known as the Blockchain, will continue to grow.
By using this system, the Bitcoin network provides a way to transact electronically without relying on any model of trust.

### Other Areas for discussion
- Economic & Societal
    - Deflationary
    - Predictable Supply
    - Decentralized
    - Block Size
    - Ecosystem
    - Exchanges
- Characteristics of Money
    - Divisibility
    - Fungibility
    - Transportability
    - Verifiability
    - Durability
- History
    - Genesis Block
    - Early Transactions and Exchanges
    - Hard Forks
    - Notable Contributors
    - Price
- Attacks and Performance Improvements
    - Attacks:
        - [Double-spending](https://en.bitcoin.it/wiki/Irreversible_Transactions)
            - Mitigated by waiting for a set number of confirmations
            - The probability of success is a function of the attacker's hashrate (as a proportion of the total network hashrate) and the number of confirmations the merchant waits for
        - [Majority (51%) Attack](https://en.bitcoin.it/wiki/Majority_attack)
            - The attacker can generate blocks faster than the rest of the network
            - Waiting for confirmations does increase the aggregate resource cost of performing the attack
    - Performance Improvements
        - DAA
        - Block Propogation
        - Ordered Transactions
        - OP Codes
        - ASICs
        - Blocksize
