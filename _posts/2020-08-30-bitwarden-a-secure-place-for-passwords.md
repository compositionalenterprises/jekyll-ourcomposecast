---
title: Bitwarden, A Secure Place for Passwords
service: Bitwarden
tagline: A lot happened in the News! Andrew and Jack go over it, and introduce Bitwarden as a password manager, and touch on the book What's Best Next?!
chapters:
  - start: '00:00:00'
    title: 'Introduction'
  - start: '00:03:10'
    title: 'News/Community Updates'
  - start: '00:18:00'
    title: 'OurCompose Developments'
  - start: '00:21:23'
    title: 'Integration Discussion - Bitwarden - Overview'
  - start: '00:39:37'
    title: "Grab Bag - What's Best Next"
---

## News / Community Updates

### [Bookstack turns 5!!!](https://www.bookstackapp.com/blog/5-years-of-bookstack/)

### [WordPress 5.5 Eckstine](https://wordpress.org/news/2020/08/eckstine/)

### [Dockerhub to delete 4.5 petabytes of images](https://www.techradar.com/news/docker-could-be-set-to-delete-all-your-files-if-you-havent-accessed-them-for-a-while)

[Scaling Docker to Serve Millions More Developers: Network Egress](https://www.docker.com/blog/scaling-docker-to-serve-millions-more-developers-network-egress/)

[Scaling Docker's Business to Serve Millions More Developers: Storage](https://www.docker.com/blog/scaling-dockers-business-to-serve-millions-more-developers-storage/)

### [GMail goes down for several hours](https://www.google.com/appsstatus#hl=en&v=issue&sid=1&iid=a45de3b26d6c5872f4cfe8e3424d7a82)

### [Floccus 4.3.0](https://github.com/marcelklehr/floccus/releases)

## OurCompose Developments

### Updated Contact Info

### GnuCash Setup

## [Integration Discussion - Bitwarden Overview](https://compositionalenterprises.ourcompose.com/bookstack/books/bitwarden/page/overview)

## [Grab Bag - What's Best Next](https://www.whatsbestnext.com/resources/book-extras/the-book/)

What’s Best Next first shows us how to understand our productivity, work, and the things we do every day in relation to God’s purposes, and then gives a practical approach for improving our productivity in all areas of life. This approach integrates its own unique insights together with the best thinking from resources like David Allen’s Getting Things Done and Stephen Covey’s The Seven Habits of Highly Effective People, on a gospel-centered foundation.

### (Ch. 17) Delegation

We don't have all the time we need to do what needs to get done. But we have all the resources that we need to get everything done. We need other people too!

Delegation won't just get rid of unwanted tasks. Important tasks need to be delegated too.

Delegation is a way of serving those you delegate to by building them up. This is stewardship delegation vs gopher delegation. Stewardship includes an area of responsibility, where gopher is a wait until told state.

5 Components of effective delegation:

- Desired Results
- Guidelines
- Resources
- Accountability
- Consequences

Saying that some can't handle stewardship is selling them short.

### (Ch. 20) Managing Email and Workflow

The amount of info we have to process can overwhelm us.

#### Step 1: Collect

Have a capture tool everywhere. And make sure it's one that you like to use.

#### Step 2: Process

- Process in order (of time rec'd)
- Process one item at a time
- Never put anything back into your inbox

The two questions to ask:

- What is this
- What's the next action

#### Step 3: Organize and act

There are 5 possible things to do with a source of input:

- Delete it
- File it
- Do it
- Delegate it
- Defer it

1-2 are when no action is required. 3 is when it can be done quickly (90 seconds or less). 4-5 are when further action is required.

The problem with the two-minute rule:

>If done on a continual basis, there is a nonzero chance that you will receive 2-minute input faster than you can process it. This can be combatted by instituting time blocking and turning off email. As mentioned before, this should be in 90 minute increments. Once per day should be enough.

See: [What is Inbox Zero, and How Can You Achieve It?](https://www.howtogeek.com/413507/what-is-inbox-zero-and-how-can-you-achieve-it/)

### (Ch. 18) Harnessing the Time Killers

Eliminate or harness them.

#### Multitasking: Killing it

Multitasking is actually switchtasking. Switch tasking incurs context switching costs. Background tasking requires one or none of the tasks use focus. Rapid refocusing is completing tasks in quick succession that only require seconds to complete.

#### Procrastination: Harnessing it

Love what you do. The three components of motivation are:

- Autonomy
- Mastery
- Purpose

If you're still procrastinating, try:

- Identifying missing information or precedent tasks
- Breaking it down into smaller chunks
- Do nothing (literally, for a period of time)

#### Interruptions: Making them Beneficial

Minimize them when possible. Otherwise have uninterruptible periods of solo work time, and making yourself available outside of those times. Consider interruptions little gifts from God.
