---
title: Kanboard, a Kanban Board
service: Kanboard
tagline: A gentle introduction to the versitile Kanboard is prefaced by a slew of news items from Andrew. Jack rounds out the show with part 1 of 2 on how we work with containers.
chapters:
  - start: '00:00:00'
    title: 'Introduction'
  - start: '00:00:45'
    title: 'News/Community Updates'
  - start: '00:06:26'
    title: 'OurCompose Developments'
  - start: '00:11:30'
    title: 'Integration Discussion - Kanboard - Overview'
  - start: '00:32:35'
    title: 'Grab Bag - Intro to Containers'
---

## News / Community Updates

### [Riot.im changes its name to Element](https://element.io/blog/welcome-to-element/)

### [Bitwarden_rs update to 1.16.0](https://github.com/dani-garcia/bitwarden_rs/releases/tag/1.16.0)

### [Bitwarden Security Audit 2020](https://cdn.bitwarden.com/misc/Bitwarden%20Network%20Security%20Assessment%20Report%20-%202020.pdf)

> During the tests performed by the Insight Risk Consulting team, no exploitable vulnerabilities were discovered and two issues of moderate severity were highlighted. These results are very positive, especially given the extensive size and complexity of Bitwarden’s overall infrastructure.

### [Nextcloud Conference 2020](https://nextcloud.com/blog/nextcloud-conference-2020-save-the-date/)

Bonus: [Introducing: the Nextcloud Podcast](https://nextcloud.com/blog/introducing-the-nextcloud-podcast/)

## OurCompose Developments

### Cron jobs unable to execute without explicitly setting the root user's full path

> When you normally run a shell script, it runs under the context of your user profile's shell settings. So it knows which shell executor to use, the programs that are available in your PATH environment variable etc. However, when you run the same script with crontab, you may have a very different context/environment variables. It's best to specify these explicitly so that others and your future self can understand your state of mind and thinking if they every look at it.
>
>  -- [DEV.to](https://dev.to/shuaib/setting-up-cron-jobs-to-run-bash-scripts-n5n)

> The default path is set to PATH=/usr/bin:/bin. If the command you are executing is not present in the cron specified path, you can either use the absolute path to the command or change the cron $PATH variable. You can’t implicitly append :$PATH as you would do with a regular script.
>
> -- [Linuxize](https://linuxize.com/post/scheduling-cron-jobs-with-crontab/)

### [Service Version Review script](https://gitlab.com/compositionalenterprises/play-compositional/-/blob/master/bin/version_review.py)

## Integration Discussion

### [Kanboard - Overview](https://compositionalenterprises.ourcompose.com/bookstack/books/kanboard/page/overview)

## Grab Bag -- Intro to Containers (1/2)

### What are containers?

Containers have become increasingly common components in many developers’ toolkits. The goal of containerization, at its core, is to offer a better way to create, package, and deploy software across different environments in a predictable and easy-to-manage way.

Containers are an operating system virtualization technology used to package applications and their dependencies and run them in isolated environments. They provide a lightweight method of packaging and deploying applications in a standardized way across many different types of infrastructure.

The container format also ensures that the application dependencies are baked into the image itself, simplifying the hand off and release processes. Because the hosts and platforms that run containers are generic, infrastructure management for container-based systems can be standardized.

### Containers vs VMs

Containers take a different approach. Rather than virtualizing the entire computer, containers virtualize the operating system directly. They run as specialized processes managed by the host operating system’s kernel, but with a constrained and heavily manipulated view of the system’s processes, resources, and environment. Containers are unaware that they exist on a shared system and operate as if they were in full control of the computer.

### What makes this technology possible - Linux Cgroups and Namespaces

Cgroups are a kernel feature that allow processes and their resources to be grouped, isolated, and managed as a unit. cgroups bundle processes together, determine which resources they can access, and provide a mechanism for managing and monitoring their behavior.

Namespaces are a feature of the Linux kernel that partitions kernel resources such that one set of processes sees one set of resources while another set of processes sees a different set of resources.

### What is Docker?

Docker is a set of tools that allow users to create container images, push or pull images from external registries, and run and manage containers in many different environments.

### Benefits

  - Lightweight Virtualization
  - Environment Isolation
  - Standardized Packaging and Runtime Environment
  - Scalability
