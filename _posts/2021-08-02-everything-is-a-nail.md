---
title: Everything is a Nail
service: Bitwarden
tagline: As the guys report ramping up their production on OurCompose, they get long-winded talking about Organizational Debt and their new Kanboard setup after taking a dive into Bitwarden's Tools and Reports.
chapters:
  - start: '00:00:00'
    title: 'Introduction'
  - start: '00:28:30'
    title: 'News/Community Updates'
  - start: '00:34:13'
    title: 'OurCompose Developments'
  - start: '00:44:04'
    title: 'Integration Discussion - Bitwarden - Tools and Reports'
  - start: '01:04:37'
    title: 'Grab Bag - On Evolving How We Use Our Kanban Boards'
---

## Intro

- [Organizational debt is like technical debt - but worse](https://venturebeat.com/2015/05/19/organizational-debt-is-like-technical-debt-but-worse/)
    - [Hiring, Firing and Retiring](https://usefathom.com/above-board/hiring-firing-retiring)
- [Element raises $30M as Matrix explodes!](https://element.io/blog/element-raises-30m-as-matrix-explodes/)

## News / Community Updates

- [Vaultwarden 1.22.2](https://github.com/dani-garcia/vaultwarden/releases/tag/1.22.2)
- [Six Years of Bookstack](https://www.bookstackapp.com/blog/6-years-of-bookstack/)
- [Firefly III Vulnerable to "Improper Restriction of Excessive Authentication Attempts"](https://twitter.com/Firefly_III/status/1419350810606063623?s=20)
- [Wordpress 5.8 Tatum](https://wordpress.org/news/2021/07/tatum/)

## OurCompose Developments

- [Portal Logon Page](https://gitlab.com/compositionalenterprises/portal/-/merge_requests/125)
- [Commands Receivable](https://gitlab.com/compositionalenterprises/role-compositional/-/merge_requests/78)

## Integration Discussion - Bitwarden - [Tools](https://compositionalenterprises.ourcompose.com/bookstack/books/bitwarden/page/tools) & [Reports](https://compositionalenterprises.ourcompose.com/bookstack/books/bitwarden/page/reports)

## Grab Bag - On Evolving How We Use Our Kanban Boards

#### Or, An Honest _Totally_ Unbiased Discussion On How Andrew Kicks The Scrum Can Down The Road

### Problems We're Evolving to Solve
- Having a board that lies about the tasks that we're currently doing and their magnitude
- Not being able to visibly represent progress in a big task
- Lack of focus on the big picture

### Metrics: Why Even Complexity?
- Kanboard: where the metrics are made-up and the points don't matter
- What even is complexity?
    - Non-time-based, which is necessary for knowledge-work
    - Fibonacci sequence
    - Analogous to "Story Points"
- Completed Complexity
    - Per Team, not per person (yet?)
    - Eventually consistent
    - Last Quarter's Average vs. Weekly Maintenance Tasks

### Implementation Mechanisms
- Meetings
    - Priority & Complexity
        - Check for any tasks that have not been assigned a complexity
        - Split & Recomplexitize incomplete tasks
        - Replenish tasks from Backlog for 10 total complexity per person between Improvement/Incident swimlanes
    - Retro & Review
        - Review Completed Complexity report
        - Review completed improvement tasks & their Roadmap parents
        - Review overdue tasks (conversation starter)

- Do due dates do dates well?
    - Grace Period
    - Is there any scenario to not split?
    - Is re-complexitizing recurring tasks subject to forgetting?
        - "I'll just remember to..."

- Forced upkeep
    - Splitting/Recomplexitizing
    - Reviewing Roadmap & Goals Doc
        - Why do we have a Goals doc?
        - Completed Tasks --> Roadmap task closure during R&R meeting
        - Depleted backlog --> Roadmap for available tasks --> Goals doc for priority

- Starting with a clean slate for the quarter

- Task Linking
    - Roadmap status at-a-glance
    - Why don't we use any other types other than parent-child?

- What necessitated "Other Assignees"
