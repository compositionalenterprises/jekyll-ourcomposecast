---
title: Living As An Inmate
service: Bitwarden
tagline: Avoiding crippling technical debt, Nexcloud developments charge ahead much like the OurCompose project as Q3 2021 is brought up along with an in-depth look into Bitwarden's application interface!
chapters:
  - start: '00:00:00'
    title: 'Introduction'
  - start: '00:27:04'
    title: 'News/Community Updates'
  - start: '00:43:20'
    title: 'OurCompose Developments'
  - start: '00:45:40'
    title: 'Integration Discussion - Bitwarden - Application Interface'
  - start: '01:18:18'
    title: 'Grab Bag - Active Recall and Spaced Repetition, Keeping Skills Sharp'
---

## Intro
- [Stepsize: Engineers waste 1 day a week on technical debt](https://venturebeat.com/2021/07/07/stepsize-engineers-waste-1-day-a-week-on-technical-debt/)
- [Canon M3 and CHDK](https://www.youtube.com/watch?v=RGnpfjFoUT0)
    - [Linux Method](https://chdk.fandom.com/wiki/Prepare_your_SD_card#Linux)

## News / Community Updates
- [Nextcloud 22 Introduces Knowledge Management](https://nextcloud.com/blog/nextcloud-22-introduces-knowledge-management/)
- [Nextcloud Hub 22 introduces approval workflows, integrated knowledge management, and decentralized group administration](https://nextcloud.com/blog/nextcloud-hub-22-introduces-approval-workflows-integrated-knowledge-management-and-decentralized-group-administration/)

## OurCompose Developments

- Concierge Sign-Up
- Quarterly Planning

## [Integration Discussion - Bitwarden - Application Interface](https://compositionalenterprises.ourcompose.com/bookstack/books/bitwarden/chapter/application-interface)

## Grab Bag - Active Recall and Spaced Repetition, Keeping Skills Sharp

Active Recall is retrieving information from memory through testing yourself at every stage of the revision process.
The very act of retrieving information and data from our brains not only strengthens our ability to retain information but also improves connections in our brains between different concepts.

Spaced Repetition involves spacing your revision and reviewing topics, ideally by active recall, at specific intervals over a period of time.

It can be explained by the 'forgetting curve' - the idea that over time we forget things at an exponential rate - similar to the half-life of radioactive substances for a scientific analogy.

The way the cycle can be broken is by reviewing material at spaced intervals.

The idea behind spaced repetition is that you allow your brain to forget some of the information to ensure that the active recall process is mentally taxing. 
The psychology literature suggests that the harder your brain has to work to retrieve information, the more likely that information will be encoded.

This process allows us to forget information, and then forces us to actively use our brain to remember it/re-encode it.

> By spacing our repetition by a day, 3 days, then a week, we allow ourselves to forget some of the information such that when we revise the topic – through active recall – it takes active brain power. Rereading, on the other hand, has low utility because it is a passive exercise - just testing yourself once has been shown to be more effective than rereading the same passage four times.


- [Spaced Repetition](https://aliabdaal.com/spaced-repetition/)
- [Active Recall](https://aliabdaal.com/activerecallstudytechnique/)
- [Active Recall and Spaced Repetition](https://johnkennyweb.wordpress.com/2020/05/11/how-to-help-students-remember-active-recall-and-spaced-repetition/)
- [What is Active Recall and Spaced Repetition](https://revisingrubies.com/active-recall-and-spaced-repetition/)
- [Anki](https://www.ankiapp.com/)
