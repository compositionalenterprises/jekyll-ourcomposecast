---
title: What The FAQ Kanboard
service: Kanboard
tagline: As we wrap up Kanboard, we stop to showcase our browser plugins and more!
chapters:
  - start: '00:00:00'
    title: 'Introduction'
  - start: '00:00:50'
    title: 'News/Community Updates'
  - start: '00:08:15'
    title: 'OurCompose Developments'
  - start: '00:13:15'
    title: 'Integration Discussion - Kanboard - Frequently Asked Questions'
  - start: '00:38:00'
    title: 'Grab Bag - Browser Plugins'
---

## Intro

## News / Community Updates

### [Free Software - It's about much more than zero cost - The Document Foundation Blog](https://blog.documentfoundation.org/blog/2021/02/14/free-software-its-about-much-more-than-zero-cost/)

### [Changes to LastPass Free Tier](https://blog.lastpass.com/2021/02/changes-to-lastpass-free/)

## OurCompose Developments

### Developing Automation to fix unhealthy containers (1288)

### Allow Kanboard to install plugins from the WebGUI (1483)

## Integration Discussion - Kanboard - Frequently Asked Questions

- Does Kanboard work with my existing Calendar?
- What is the best way to track productivity on Kanboard?
- Why should I track my tasks using a board instead of a list?
- What should my first board look like?
- What is the first task I should put in my board?
- Are there any open source alternatives to kanboard?
- Should I use kanboard to track bugs?
- How can I setup notifications in Kanboard?
- Will I ever run into performance issues with Kanboard?
- How can I add plugins to kanboard?
- What is the best plugin?
- What is the worst plugin?
- Is kanboard a good way to document work as compared to a knowledge base?

## Grab Bag - Browser Plugins

- [JSON Viewer](https://github.com/tulios/json-viewer)
- [Vimium](http://vimium.github.io)
- [Dark Reader](https://darkreader.org)
- [uBlock Origin](https://github.com/gorhill/uBlock)
- [uMatrix](https://github.com/gorhill/uMatrix/wiki)
- [HTTPS Everywhere](https://www.eff.org/https-everywhere)
- [Cookie Autodelete](https://github.com/Cookie-AutoDelete/Cookie-AutoDelete)
- [QR Code Generator](https://high-qr-code-generator.com/)
- [Floccus](https://floccus.org)
- [Bitwarden](https://bitwarden.com/browser-start/)

### Bonus: Tab Management

- [How to manage too many browser tabs in Chrome, Firefox, Brave, and Vivaldi](https://vivaldi.com/blog/manage-too-many-browser-tabs-chrome-firefox-brave-vivaldi/)
- [Vivaldi takes tabs to the next level, literally](https://vivaldi.com/blog/desktop/desktop-releases/vivaldi-tabs-two-level/)
- [Opera Workspaces](https://www.opera.com/features/workspaces)
