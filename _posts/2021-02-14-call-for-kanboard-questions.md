---
title: Call for Kanboard Questions
service: Kanboard
tagline: Hello everyone! This upcoming episode we will be trying out doing a recap on Kanboard that involves us answering questions from you, our audience!
chapters:
  - start: '00:00:00'
    title: 'Call for Questions'
---

## Call for Questions

Hello everyone!
This upcoming episode we will be trying out doing a recap on Kanboard that involves us answering questions from you, our audience!

This will be a Question and Response episode, where we will be taking your questions(!), and answering them on the podcast.
Please submit your questions before Wednesday, February 17, 2021, so we can address them in our recording as appropriate.

To submit a question, please fill out the contact form linked in the header of the website, or send an email directly to the address mentioned in the corresponding audio.
We look forward to trying this out together with you all, and to many more episodes like this!
