---
title: Nextcloud Hash of Third Party Apps
service: Nextcloud
tagline: 'Our fearless digital experts ramp up production on the OurCompose product, and discuss the foundations of how to win friends and influence people.'
chapters:
  - start: '00:00:00'
    title: 'Introduction'
  - start: '00:24:30'
    title: 'News/Community Updates'
  - start: '00:28:50'
    title: 'OurCompose Developments'
  - start: '00:35:50'
    title: 'Integration Discussion - Nextcloud - Third Party Apps'
  - start: '00:51:30'
    title: 'Grab Bag - Part One: Fundamental Techniques in Handling People'
---

## Intro

### [Errata: Why Munich is shifting back from Microsoft to open source - again](https://www.zdnet.com/article/linux-not-windows-why-munich-is-shifting-back-from-microsoft-to-open-source-again/)

### [Words to strike fear into admins' hearts: One in five workers consider themselves 'digital experts' these days](https://www.theregister.com/2021/04/27/gartner_trends/)

### [Podcasting Manageable Goals](https://www.reddit.com/r/podcasting/comments/n12i8q/manageable_goals/)

## News / Community Updates

### [Bitwarden_rs 1.21.0 Release and Name Change to Vaultwarden](https://github.com/dani-garcia/vaultwarden/releases/tag/1.21.0)

## OurCompose Developments

### [Newsletter on ourcompose.com](https://compositionalenterprises.ourcompose.com/kanboard/project/10/task/1631)
- [OurCompose.com](https://ourcompose.com)

### [Allow Portal to clone env to disk](https://compositionalenterprises.ourcompose.com/kanboard/project/10/task/1607)
- [Gitlab - Portal !120](https://gitlab.com/compositionalenterprises/portal/-/merge_requests/120)

### [The Value of OurCompose](https://www.youtube.com/watch?v=Vm3wczm-DaA)

### [Compositional Role 2.7 Release](https://gitlab.com/compositionalenterprises/role-compositional/-/merge_requests/64/diffs)
- [Stable 2.7 Branch](https://gitlab.com/compositionalenterprises/role-compositional/-/tree/stable-2.7)

## Integration Discussion - Nextcloud - Third Party Apps

- [Checksum](https://compositionalenterprises.ourcompose.com/bookstack/books/nextcloud/page/checksum)
- [EPUB Reader](https://compositionalenterprises.ourcompose.com/bookstack/books/nextcloud/page/epub-reader)
- [Only Office](https://compositionalenterprises.ourcompose.com/bookstack/books/nextcloud/page/only-office)

## Grab Bag - Part One: Fundamental Techniques in Handling People

### If You Want to Gather Honey, Don't Kick Over the Beehive

How do people regard themselves?

> Under my coat is a weary heart, but a kind one - one that would do nobody any harm
>
> - Francis "Two Gun" Crowley

> I have spent the best years of my life giving people the lighter pleasures, helping them have a good time, and all I get is abuse, the existence of a hunted man.
>
> - Al Capone

Why is criticism dangerous?
- Wounds a person's precious pride
- Hurts his sense of importance
- Arouses resentment

This results in:
- Puts the person on the defensive
- Makes him strive to justify himself

Fundamentally, it does not convince people that they were wrong, or need to change.

See: Lincoln's Unsent Letter to Meade

> Any fool can criticize, condemn and complain -- and most fools do. But it takes character and self-control to be understanding and forgiving
>
> - Benjamin Franklin

### Principle 1: Don't criticize, condemn, or complain.

## The Big Secret of Dealing with People

There is only one way to get anybody to do anything; and that is by making the other person want to do it.

> The deepest urge in human nature is the desire to be important
>
> - Dr. John Dewey

> There is nothing else that so kills the ambitions of a person as criticisms from superiors.
> I never criticize anyone. I believe in giving a person incentive to work.
> So I am anxious to praise but loath to find fault.
> If I like anything, _I am hearty in my approbation and lavish in my praise_.
>
> - Charles Schwab

A note on flattery:

> It seldom works with discerning people.
> It is shallow, selfish and insincere.
> It ought to fail and usually does.

### Principle 2: Give honest and sincere appreciation

This is all about setting expectations and developing a standard.
This can only be provided after the fact, and is not a way to proactively make the other person want to do it.
Rather, this is a long-term strategy to encourage an environment in which excellence receives honest and sincere appreciation.

## He Who Can Do This Has the Whole World with Him. He Who Cannot Walks a Lonely Way

> I often went fishing up to Maine during the summer.
> Personally, I am very fond of strawberries and cream, but I have found that for some strange reason, fish prefer worms.

...fill in the rest.

> Why talk about what we want?
> That is childish and absurd.
> Of course you are interested in what you want; you are eternally interested in it.
> **But no one else is**.
> The rest of us are just like you: we are interested in what we want.

The only way on earth to influence other people is to:
- Talk about what _they_ want
- Show them how to get it

> Action springs out of what we fundamentally desire.
> And the best piece of advice which can be given to would-be persuaders, whether in business, in the home, in the school, in politics, is: First, arouse in the other person an eager want.
> He who can do this has the whole world with him.
> He who cannot walks a lonely way.
>
> - Harry A. Overstreet

> If there is any one secret of success, it lies in the ability to get the other person's point of view and see things from that person's angle as well as from your own.
>
> - Owen D. Young

### Principle 3: Arouse in the other person an eager want

Here we get to some actionable, practical, implementable advice.
However, it is much harder than it sounds on the surface.
This is not something that can be automated, or is really even cookie-cutter.
This is not something that can be gleaned from a 5-minute surface discussion.
This advice is best implemented for short-term convincing between two people who are able to take the time and energy to dive deep into each other's wants and needs.
Additionally, this cannot be treated as a simple carrot and stick, as was described above, the fundamental want in people is to be important.
This will not work simply with material things, but rather the want must arise from the deep-seated convictions of what is already there.
