---
title: Not Enough Information Yet
service: Firefly-III
tagline: This week we discuss Google's latest pulled service, Firefly-III's Application interface, and The 4 Hour Work Week. 
chapters:
  - start: '00:00:00'
    title: 'Intro - News - Updates'
  - start: '00:15:50'
    title: 'Integration Discussion - Firefly-III - Application Interface'
  - start: '00:29:46'
    title: 'Grab Bag - The 4 Hour Work Week'
---

## Intro
- [Feds arrest married couple, seize $3.6 billion in hacked bitcoin funds](https://www.washingtonpost.com/national-security/2022/02/08/bitfinex-hack-bitcoin-arrests/)
- [Google relents: Legacy G Suite users will be able to migrate to free accounts](https://arstechnica.com/gadgets/2022/01/google-relents-legacy-g-suite-users-will-be-able-to-migrate-to-free-accounts/)
  - [Products | Google Workspace](https://workspace.google.com/features/)
  - [“Deprioritized” Google Stadia to pivot to “Google Stream” white-label service](https://arstechnica.com/gadgets/2022/02/deprioritized-google-stadia-to-pivot-to-google-stream-white-label-service/)
- [Why open source projects should embrace operational transparency](https://leaddev.com/agile-other-ways-working/why-open-source-projects-should-embrace-operational-transparency)

## News / Community Updates

- [Vaultwarden 1.24.0](https://github.com/dani-garcia/vaultwarden/releases)
- [Dolibarr 15.0](https://www.dolibarr.org/release-of-dolibarr-v15-delayed.php)
- [FireflyIII Data Importer](https://github.com/firefly-iii/data-importer/releases/0.8.0)
- [The Month in Wordpress](https://wordpress.org/news/2022/02/the-month-in-wordpress-january-2022/)

## OurCompose Developments

### Content

### Portal UI/UX

### Administrative

## Integration Discussion - [Firefly-III - Application Interface](https://compositionalenterprises.ourcompose.com/bookstack/books/firefly-iii/chapter/application-interface)

## Grab Bag - 4 Hour Work Week by Tim Ferriss

### New Rich
- Money is multiplied in practical value depending on the number of W's you control in your life: **what** you do, **when** you do it, **where** you do it, and with **whom** you do it?
- 100K 80hr work week vs 40k 4 hr work week; Who makes more?

### Challenge the Status Quo;
- Having an unusually large goal is an adrenaline infusion that provides the endurance to overcome the inevitable trials and tribulations that go along with any goal. Realistic goals, goals restricted to the average ambition level, are uninspiring and will only fuel you through the first or second problem, at which point you throw in the towel.
- If the potential payoff is mediocre or average, so is your effort.

### Dreamlining
- Similar to Goalsetting, but differs in several respects:
1) The goals shift from ambiguous wants to defined steps
2) The goals have to be unrealistic to be effective
3) It focuses on activities that will fill the vacuum created when work is removed. Living like a millionaire requires doing interesting things and not just owning enviable things.

Creating 6-12 month timelines;

### Progressively Challenge yourself to step outside of your comfort zone
- There is a direct correlation between an increased sphere of comfort and getting what you want.

### Increasing Productivity
- After you have defined what you want to do with your time, now you have to free that time
- Effectiveness is doing the things that get you closer to your goals. Efficiency is performing a given task (whether important or not) in the most economical manner possible
- Doing something unimportant well doesn't make it important
- A task requiring a lot of time does not make it important
- 80/20; 
1) Limit tasks to the important to shorten work time (80/20).
2) Shorten work time to limit tasks to the important (Parkinson's Law)

### Automation and Outsourcing
- Don't throw people at the problem if they don't need to be thrown at it, that will likely just add more confusion
- Direct them, by telling them explicitly what you want from them; "My assistant is an idiot" - took 23 hrs to book an interview;
- Don't provide abstract goals for outsourcing work

### The Top 13 New Rich Mistakes (Skipping in Podcast, but worth to have in show notes)

- Losing sight of dreams and falling into work for work's sake
- Micromanaging and e-mailing to fill time
- Handling problems your outsourcers or co-workers can handle
- Helping outsourcers or co-workers with the same problem more than once, or with noncrisis problems
- Chasing customers, particularly unqualified or international prospects, when you have sufficient cash flow to finance your nonfinancial pursuits
- Answering e-mail that will not result in a sale or that can be answered by a FAQ or auto-responder
- Working where you live, sleep, or should relax
- Not performing a thorough 80/20 analysis every two to four weeks for your business and personal life
- Striving for endless perfection rather than great or simply good enough, whether in your personal or professional life
- Blowing minutiae and small problems out of proportion as an excuse to work
- Making non-time-sensitive issues urgent in order to justify work
- Viewing one product, job, or project as the end-all and be-all of your existence
- Ignoring the social rewards of life

### My Take on this book.

### What do you want?
- "What do you want?" is too imprecise to produce a meaningful and actionable answer. Forget about it.
- "What are your goals?" is similarly fated for confusion and guesswork. To rephrase the question, we need to take a step back and look at the bigger picture.
- Excitement is the more practical synonym for happiness, and it is precisely what you should strive to chase. It is the cure-all.
