---
title: Automatic Repeatable Boards
service: Kanboard
tagline: Automation is the name of this game! Kanboard is no exception, and gets a fair going-over by Andrew, which Jack discusses how to get better at, well... anything.
chapters:
  - start: '00:00:00'
    title: 'Introduction'
  - start: '00:09:10'
    title: 'News/Community Updates'
  - start: '00:25:10'
    title: 'OurCompose Developments'
  - start: '00:31:40'
    title: 'Integration Discussion - Kanboard - Reocurring Tasks and Automated Actions'
  - start: '00:52:00'
    title: 'Grab Bag - The Five-Stage Model of Adult Skill Acquisition'
---

## Intro

### [Rundeck - Quoting arguments to steps](https://docs.rundeck.com/docs/manual/node-steps/builtin.html#quoting-arguments-to-steps)

## News / Community Updates

### [Bookstack Blog Replacing Google Analytics & Mailchimp](https://www.bookstackapp.com/blog/replacing-ga-and-mailchimp/)

### Default Updates
- [Kanboard 1.2.17](https://github.com/kanboard/kanboard/releases/tag/v1.2.17)
- [Bitwarden 1.17.0](https://github.com/dani-garcia/bitwarden_rs/releases/tag/1.17.0)
- [Wordpress 5.5](https://wordpress.org/support/wordpress-version/version-5-5/)

## OurCompose Developments

### [Variablize Ansible's 'no_log' parameter for tasks](https://gitlab.com/compositionalenterprises/role-compositional/-/merge_requests/52)
- [I spent way too much time over-engineering this before stumbling on this stupidly-simple sanity check](https://www.reddit.com/r/ansible/comments/dj6b5c/advice_about_toggling_no_log/)

## [Integration Discussion - Kanboard - Reocurring Tasks and Automated Actions](https://compositionalenterprises.ourcompose.com/bookstack/books/kanboard/page/reoccurring-tasks-and-automated-actions)

## Grab Bag - [The Five-Stage Model of Adult Skill Acquisition](http://www.bumc.bu.edu/facdev-medicine/files/2012/03/Dreyfus-skill-level.pdf)

Broken down into a table of skill level, components, perspective, decision, and commitment.

From Novice to Expert:
- Novice - Reliance on Recipes - Reaction to Errors is to blame the process
- Advanced Beginner - Able to recognize the "aspects" of a situation - Instead of blaming the process, look for a new process
- Competence - A better sense of what is relevant - Not to beat self up when things go wrong at this stage, but to use this information as data
- Proficiency - An intuitive sense of what the goal should be given the situation - intuiting the goal, you are more invested in whether or not that was the right goal
- Expertise - Operates based on intuition - knows what the goal should be, what to do about it, and what should happen as a result


| Skill Level       | Components (Context) | Perspective | Decision   | Commitment                                            |
|-------------------|----------------------|-------------|------------|-------------------------------------------------------|
| Novice            | Context Free         | None        | Analytical | Detached                                              |
| Advanced Beginner | Situational          | None        | Analytical | Detached                                              |
| Competence        | Situational          | Chosen      | Analytical | Detached Understanding and Deciding; Involved Outcome |
| Proficiency       | Situational          | Experienced | Analytical | Involved understanding; detached deciding             |
| Expertise         | Situational          | Experienced | Analytical | Involved                                              |



Distribution of Skill Level:
Novice - 20%
Advanced Beginner - 40%
Competent - 30%
Proficient - 8%
Expert - 2%

"Before you assume that you're the expert, remember that you're probably an advanced beginner."


Many programmers and other knowledge workers never advance past the Advanced Beginner Stage primarily because **they never accept the emotional consequences for their decisions.**


### Zero to Expert

At this point it's beneficial to focus on collecting recipes. If you only read about the recipes it's impossible to move past zero to the novice state. 

- 0 -> Novice :: Collect Recipes & Apply them
- Novice -> Advanced Beginner :: Break away from the fixed recipes
- AB -> Competent :: Limiting data and concentrating focus to develop better intuition
- Competent -> Proficient :: Still a choice on what to focus on -> Still developing intuition on what to focus on
- Proficient -> Expert :: Everything must feel intuitive; Everything is by feel and subconscious reason; Practice by following intuition

- [Dreyfus Model of Sill Acquisition](https://en.wikipedia.org/wiki/Dreyfus_model_of_skill_acquisition)
- [Dreyfus Five-Stage Model of Adult Skill Acquisition](http://www.bumc.bu.edu/facdev-medicine/files/2012/03/Dreyfus-skill-level.pdf)
