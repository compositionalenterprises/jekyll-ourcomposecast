---
title: Stacking Books
service: Bookstack
tagline: What if you had a bookshelf on your computer? Jack has the answer, and Andrew goes over Web3 Tech.
chapters:
  - start: '00:00:00'
    title: 'Introduction'
  - start: '00:01:18'
    title: 'News/Community Updates'
  - start: '00:07:44'
    title: 'OurCompose Developments'
  - start: '00:09:25'
    title: 'Integration Discussion - Bookstack - Overview'
  - start: '00:19:11'
    title: 'Grab Bag - Web3 Tech'
---

## Intro

[Nextcloud RSS Feed Application](https://github.com/nextcloud/news)

## News / Community Updates

[June 6th - Firefly-III implements opt-in telemetry](https://old.reddit.com/r/FireflyIII/comments/gxwl6j/update_post/)

> My hope is that ultimately, these kinds of statistics help me really get to know how everybody's using Firefly III.

[June 24th - Jekyll 4.1.1 Released](https://jekyllrb.com/news/2020/06/24/jekyll-4-1-1-released/)

> ...these seemingly benign changes had unexpected adverse side-effects which did not figure in our tests.

> Another known issue with page-excerpts is that an infinite loop is created in certain use-cases such as any construct that involves iterating through site.pages directly within a Jekyll::Page instance.

[July 1st - Rundeck 3.3.0 is now available](https://docs.rundeck.com/news/2020/07/01/rundeck-3.3.0.html)

- Docker base image updated to 18.04 LTS
- New Log Viewer
    - Light/Dark theme options
    - URLs that link directly to specific lines in the output

## OurCompose Developments

### Tooling

- Teardown Playbooks
- Restoration Playbooks

### Infrastructure

- Basic Skeleton Documentation
    - Placeholders
    - Upstream Links

## Integration Discussion: Bookstack - Overview

### Documentation

> The purpose of documentation is to:
> - Describe the use, operation, maintenance, or design of software or hardware through the use of manuals, listings, diagrams, and other hard- or soft-copy written and graphic materials.
> - Assign responsibilities and establish authority for business processes and practices (pertains mostly to policies and procedures).
> - Standardize business practices.
> - Reduce/eliminate fraud, waste, and abuse.
> - Comply with federal, state, and local regulations.
> - Comply with customer requirements.
> - Comply with contractual requirements.
> - Train new employees.

[Documentation needs 4 things](https://documentation.divio.com):
  - Tutorials
  - How to guides
  - Explanation
  - Technical Reference

### Navigation Concepts

- Shelves
- Books
- Chapters
- Pages

## Grab Bag: Web3 Tech

> Web3 is the vision of the serverless internet, the decentralised web. An internet where users are in control of their own data, identity, and destiny.
>
> [Web3 Technology Stack](https://web3-technology-stack.readthedocs.io/en/latest/)

> Web 3 Foundation believes in an internet where:
> - Users own their own data, not corporations
> - Global digital transactions are secure
> - Online exchanges of information and value are decentralized
>
> https://web3.foundation

### Impetus for change

- Content Monetization
    - Your content can be copied
    - Your content is being monetized by others
- Data Ownership
    - Your data can be snooped on
    - Your data is subject to only be displayed on a permissioned basis
- Data Permanence
    - Your data is not promised to be around forever
        - [From 2003 to 2012, MySpace was the premier place for bands to post their music. In 2018, it lost it all in a botched server migration.](http://lostmyspace.com/?q=)
    - Hyperlinks are not promised to be around forever

### Web 3.0 Projects

- [Bitcoin](https://bitcoin.org/en/bitcoin-paper)
- [IPFS](https://ipfs.io/)
- [Matrix](https://matrix.org/)
    - [Matrix and Riot confirmed as the basis for France's Secure Instant Messenger app](https://matrix.org/blog/2018/04/26/matrix-and-riot-confirmed-as-the-basis-for-frances-secure-instant-messenger-app)
- NextCloud
