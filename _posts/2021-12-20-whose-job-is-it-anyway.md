---
title: Whose Job is it Anyway
service: Rundeck
tagline: This week's episode we discuss tech ranging everywhere from Vulnerabilities to Rundeck User Management and ACLs, and we wrap up with NFTs. 
chapters:
  - start: '00:00:00'
    title: 'Intro - News - Updates'
  - start: '00:26:22'
    title: 'Integration Discussion - Rundeck - User Authentication and ACLs'
  - start: '00:37:27'
    title: "Grab Bag - Neal Stephenson's Metaverse is not a use-case for NFTs"
---

## Intro

- [Log4j](https://www.cygenta.co.uk/post/log4shell-in-simple-terms)
- [Nextcloud 3.4.0 Desktop Continues to Sync Files](https://github.com/nextcloud/desktop/issues/4016)
- [How to write idempotent Bash scripts](https://arslan.io/2019/07/03/how-to-write-idempotent-bash-scripts/)
  - Creating files that don't exist
  - Create directories recursively and ones that already exist
  - Creating symbolic links that already exist (see [Nextcloud](https://gitlab.com/compositionalenterprises/ansible-collection-compositionalenterprises.ourcompose/-/blob/fd872346bae1742689e704a4cdd0a4a905b88563/roles/kanboard/tasks/present.yml#L85-86))
  - Ignore non-existent files when removing them
  - Grep before you append
  - Bash tests
  - Formatting only unformatted disks
  - Run mountpoint to check mounts before mounting
- [American productivity fell by the largest amount since 1960](https://www.cnn.com/2021/12/07/economy/labor-productivity-third-quarter-revised/index.html)

## News / Community Updates

- [Vaultwarden 1.23.1](https://github.com/dani-garcia/vaultwarden/releases/tag/1.23.1)
- [Bookstack Security Release](https://www.bookstackapp.com/blog/bookstack-release-v21-11-2/)

## OurCompose Developments

### Instance Features

### Service Resiliancy

### Engagement

- Content Walkthrough Playlists
  - Rundeck
  - Bitwarden
  - Kanboard
  - Nextcloud

### Administrative

## Integration Discussion - Rundeck - [User Authentication](https://compositionalenterprises.ourcompose.com/bookstack/books/rundeck-d07/page/user-authentication) and [ACLs](https://compositionalenterprises.ourcompose.com/bookstack/books/rundeck-d07/page/acls)

## Grab Bag - Neal Stephenson's Metaverse is not a use-case for NFTs

### Snow Crash

Snow Crash established Stephenson as a major science fiction writer of the 1990s. The book appeared on Time magazine's list of 100 all-time best English-language novels written since 1923.

#### Summary

> Not-too-distant future - a world where the Mafia controls pizza delivery, the United States exists as a patchwork of corporate-franchise city-states, and the Internet - incarnate as the Metaverse - looks something like last year's hype would lead you to believe it should.
> 
> In reality, Hiro Protagonist delivers pizza for the CosaNostra.
> But in the Metaverse, he's a warrior prince, last of the solo hackers, and the greatest sword fighter in the world.
> Now, he's racing along the neon-lit streets, the skirts of his black leather kimono flapping, on a search-and-destroy mission for the shadowy virtual villain threatening to bring about infocalypse!
> When his best friend fries his brain on a new designer drug called Snow Crash and his beautiful, brainy ex-girlfriend asks for his help, what's a guy with a name like that to do? He rushes to the rescue.

- Similar to Cyber(bug) 2077, most territory is owned by private organizations and entrepreneurs.
- The main character is "Hiro Protagonist" who is a pizza delivery boy by day, and the originator of the Metaverse at night.
- His friend receives a letter (in the metaverse) that causes his computer to crash and suffer brain damage in the real world.
- He has to find the origins of this virus and stop the evil men from using it for evil.

#### Economics

>  Hyperinflation has sapped the value of the US dollar to the extent that trillion-dollar bills—Ed Meeses—are nearly disregarded, and the quadrillion-dollar note—the Gipper—is the standard "small" bill.
>  This hyperinflation was created by the government overprinting money, due to loss of tax revenue, as people increasingly began to use electronic currency, which they exchanged in untaxable encrypted online transactions.
>  For physical transactions, most people resort to alternative currencies such as yen or "Kongbucks" (the official currency of Mr. Lee's Greater Hong Kong).
>  Hyperinflation has also negatively affected much of the rest of the world (with some exceptions like Japan), resulting in waves of desperate refugees from Asia, who cross the Pacific in rickety ships hoping to arrive in North America.

#### Metaverse

> The Metaverse, a phrase coined by Stephenson as a successor to the Internet, constitutes Stephenson's vision of how a virtual reality–based Internet might evolve in the near future.
> Resembling a massively multiplayer online game (MMO), the Metaverse is populated by user-controlled avatars, as well as system daemons.
> Although there are public-access Metaverse terminals in Reality, using them carries a social stigma among Metaverse denizens, in part because of the poor visual representations of themselves as low-quality avatars.
> Status in the Metaverse is a function of two things: access to restricted environments such as the Black Sun, an exclusive Metaverse club, and technical acumen, which is often demonstrated by the sophistication of one's avatar.

### Ready Player One

> In the year 2045, the world is gripped by an energy crisis and global warming, causing widespread social problems and economic stagnation.
> The primary escape for most people is a virtual universe called the OASIS, which is accessed with a visor and haptic gloves.
> It functions both as an MMORPG and as a virtual society, with its currency being the most stable currency in the world.

### Similar Metaverse Games

- [IMVU](https://about.imvu.com) (as discussed in [Episode #11's Grab Bag - The Lean Startup](https://ourcompose.com/jekyll//episodes/2020/episode-11-minimal-viable-portal))
- [Second Life](https://secondlife.com)
- [Facebook's Metaverse](https://about.facebook.com/meta/)

### NFTs

- [What Are NFTs and How Can They Be Used in Decentralized Finance? DEFI Explained](https://www.youtube.com/watch?v=Xdkkux6OxfM)

Aren’t they basically just JPEGs you can right-click save?

> Not all NFTs are stored in the same way.
> One of the main features of NFTs is their provable ownership, verifiability and provenance.
> As we know this is achieved by creating NFTs on Decentralized Blockchains such as Ethereum, but here comes a problem.
> Although the beautiful, high-resolution pictures of apes or penguins are cool, there is no way the image files representing them can be stored on the blockchain itself.
> This leads to a situation when most NFTs only include a link to the actual art and its metadata which is then stored off-chain.
>
> There is a whole range of options when storing data off-chain.
> Some projects use centralized servers, others try to improve the situation by uploading their metadata and art to IPFS.
> Other solutions like Arweave are also gaining traction.
>
> Generative art and certain low-resolution NFTs are some of the exceptions here as they can be entirely stored on-chain.
-- [NFT Mania - Hype Or A New Paradigm? CryptoPunks, BAYC, Generative Art, Loot Explained](https://www.youtube.com/watch?v=WOxYlBTRncY&t=586s)

#### PFP NFTs

> The first NFT collection that started the PFP trend is the Crypto Punks.
> Owners of a Punk put up their precious NFT that only contained a few pixels up as their avatar on Twitter.
> While in the meantime creating a pseudonymous identity online.
> A digital identity, without your real face, and sometimes without your real name.
> But under which you can freely post your opinion and engage in conversations online.
>
> While this is great and all, can’t people just save your image and use it as their Twitter profile pic?
> Well yes, they can. But since NFTs are on the blockchain, it’s easy to prove who is the original owner.
>
> Moreover, Twitter developers announced that they are working on a verification tool, so Twitter can verify ownership of the NFT.
> The owner will get a small Ethereum logo on their PFP, similar to the blue checkmark icon that influencers have.
>
> This way, everyone can spot with the naked eye if the social media account truly owns the NFT.
-- [Profile Picture / PFP NFTs – All You Need to Know](https://blogs.airdropalert.com/profile-picture-pfp-nfts-all-you-need-to-know/)

#### NFTs need a social, digital marketplace

> But which blockchain?
> There are many blockchains.
> What is to prevent someone from uploading the same NFT on multiple blockchains (less of an issue for now because Ethereum seems to run all the NFTs)?
> What is to prevent someone, once having uploaded an NFT on a given blockchain, to promise not to upload anymore copies, but then to break the promise and, in effect, increase the print run?

> We have no technological guarantees that Dorsey’s genesis tweet will not be retokenized by him as an NFT.
> Instead, the only constraint on proliferation is Dorsey’s promise not to do otherwise and the social norms that would hold him to that promise.

> Unless my signing over of the digital photo on the Ethereum blockchain is combined with a parallel signing over of actual copyright in the real marketplace of our society at large, all you can do is negotiate/trade this digital photo (i.e., NFT) on the blockchain where it was uploaded and signed over to you.
> So you’re in effect stuck on the blockchain where the NFT resides.
> Perhaps laws will at some point be changed so that points of sale in the virtual marketplace of the Ethereum blockchain are treated as points of sale in the real marketplace of the society at large, conferring all the rights and guarantees of the real marketplace.
> But that’s not the case now, and there’s no prospect for that becoming the case anytime soon.

> Having saturated Rarible with my NFT of the Iowa Democratic Primary, I could go to any other NFT marketplace that requires no prior approval of its users, and do the same exercise there, uploading the png of the Iowa Democratic Primary there as an NFT.
> Granted, word of my unbridled proliferation of the same NFT might get around and ruin any chance of turning a profit with it.
> But that’s not the point.
>
> The point is that such unbridled proliferation, with its assault on scarcity, has no technological solution.

> As a consequence, all the actual content of an NFT—in other words, what makes it collectible, artworthy, or of any interest—will typically reside off the Etherium blockchain.
> But where?
> In the case of the NFT I created at Rarible, it will reside (big surprise) at Rarible, with what’s on the Ethereium blockchain simply registering the NFT and pointing to Rarible for the png that’s at the heart of this NFT.
>
> So my NFT, to exist at all, must reside in two places, namely, the Ethereum blockchain AND the Rarible website. If either fails, I lose my NFT.

> That it was signed over to him on the Ethereium blockchain, or any blockchain for that matter, seems unessential.
> Christie’s and Sundaresan, for instance, could each have set up a private/public cryptographic key for themselves, and Christie’s could then have signed over Everydays from its key to Sundaresan’s.
> The public keys and the signing over of the artwork using private keys would have been noted far and wide given all the public attention to this sale, and Sundaresan could have paid for it in any currency mutually agreed upon by the buyer and auction house.
> Blockchain could thus have been sidelined.

> ownership on the Ethereum blockchain is not ownership in the real world.
> Real world ownership is ownership with the force of law behind it.
> Ethereum blockchain-based ownership is purely conventional ownership.
> It applies only within the Ethereum ecosystem, and nowhere outside.

-- [How Non-Fungible Tokens Work: NFTs Explained, Debunked, and Legitimized](https://www.expensivity.com/how-non-fungible-tokens-work/)

### TL;DR

NFTs are legitimized to the extent that their utilization is confined to a socially-defined ecosystem whose rules are centrally enforced.
