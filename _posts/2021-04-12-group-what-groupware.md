---
title: Group What Groupware
tagline: Jack goes over Ruby gems and Nextcloud, while Andrew takes a closer look at why it's necessary to care about other people's feelings.
service: Nextcloud
chapters:
  - start: '00:00:00'
    title: 'Introduction'
  - start: '00:31:38'
    title: 'News/Community Updates'
  - start: '00:38:00'
    title: 'OurCompose Developments'
  - start: '00:38:42'
    title: 'Integration Discussion - Nextcloud - Groupware'
  - start: '00:55:20'
    title: 'Grab Bag - Primal Leadership'
---

## Intro

### [The NEW Match-Case Statement in Python 3.10](https://www.infoworld.com/article/3609208/how-to-use-structural-pattern-matching-in-python.html)

### [Be More Realistic About the Time You Have](https://hbr.org/2021/03/be-more-realistic-about-the-time-you-have)

### [FreeBSD 13's Close Call](https://arstechnica.com/gadgets/2021/03/buffer-overruns-license-violations-and-bad-code-freebsd-13s-close-call/)

## News / Community Updates

### [Ruby off the Rails](https://www.theregister.com/2021/03/25/ruby_rails_code/)

### Releases
- [Bitwarden_rs 1.20.0](https://github.com/dani-garcia/bitwarden_rs/releases/tag/1.20.0)
- [Firefly-III 5.5.1 (LOTS OF CHANGES!!!)](https://github.com/firefly-iii/firefly-iii/releases/tag/5.5.1)
- [Wordpress 5.7 "Esperanza"](https://wordpress.org/news/2021/03/esperanza/)

## OurCompose Developments

### Q1 Wrapup

## [Integration Discussion - Nextcloud - Groupware](https://compositionalenterprises.ourcompose.com/bookstack/books/nextcloud/page/groupware)

## Grab Bag - Primal Leadership

> _Learning to Lead With Emotional Intelligence_

### Summaries:

#### [Heather Farran's Write-up and Evaluation](https://farran.abwe.org/uploads/9/7/1/2/97128822/primal_leadership.pdf)

> “the fundamental task of leaders is to prime good feeling in those they lead.
> That occurs when a leader creates resonance; a reservoir of positivity that frees the best in people.
> At its root, then, the primal job of leadership is emotional.

#### [The Power Moves](https://thepowermoves.com/primal-leadership/)

> In Primal Leadership (2001) Daniel Goleman argues that the number one trait of any good leader is his emotional intelligence, and his ability to connect and engage with his followers.
> The book is also famous for Goleman’s six types of leaders, whom he describes in detail.

### Leadership Types

> Daniel Goleman uses the term “resonant leaders” to describe leaders who understand human nature and leverage their emotional intelligence to connect with people and get the most out of them.
>
> Resonant leaders embrace emotions and humanity.
>
> Dissonant leaders instead don’t understand or actively try to cut out the human side of business.
>
> _[The Power Moves](https://thepowermoves.com/primal-leadership/)_

#### Resonant
- Visionary
- Affiliative
- Democratic
- Coaching

#### Dissonant
- Pacesetting
- Commanding

### Andrew's Emotional Intelligence Domains Takeaways

#### Social Awareness - Service

How we rollout change is just as important as the quality/excellence of the tool.
This is because people's emotions have a greater effect on the success of a rollout than people's intellect

To quote Jack from the [fifth episode of the podcast](https://youtu.be/fUeixBCAYuU?list=PLt92CQtzdbDKtFg9-GRBlovpWzb3DtbWd&t=44):

> What's a tool if you hate it? You're not gunna sign into Kanboard if you hate it!

This is more important than ever when adopting new tooling and changing the day-to-day functions of an organization, no matter how small it is!!!

When someone considers their task as completed, but to your point of view, it's only 1/5 completed, then you have a problem. Some recent examples:

- Network Engineer Spinning up Windows DB Server
- Why do we write documentation?

This is in no small part a lack of setting expectations. Using Kanboard, [you can do this using the description field of a task](https://compositionalenterprises.ourcompose.com/bookstack/books/kanboard/page/new-task-prompt).

This can also come back to [the three components of motivation](https://ourcompose.com/jekyll//episodes/2020/episode-5-bitwarden-a-secure-place-for-passwords):

- Autonomy
- Mastery
- Purpose

#### Relationship Management - Everything

In response to "It's like a physical tick for me to be unable to lie":

> Sometimes things need to be said in a diplomatic way, and not just... bleh, you know?
>
> [...]
>
> How do you decide, or _do_ you decide how this is going to make this person feel.
>
> [...]
>
> If I heard "There's probably some misunderstanding between us. Let me be as clear and precise as possible to clear away that misunderstanding", I would be like, ooh... too clear.
>
> [Just Chatting on Twitch](https://www.twitch.tv/pineapplehoops)

> People need to feel heard. They need to not feel this is just another top down thing.
> To get buy-in, people love to interact with ideas before it is set in stone.
> This will make them feel heard and valued.
> It will also allow us to gain valuable insights into potential misunderstandings before an official rollout.
> Finally, by drawing them into the process before completion they will feel an ownership of it because they touched it before it was finalized.
> (This remains true even if the tool is 99.9% complete.)
> Without buy-in, you are trying to lead people where they don’t want to go.
> Taking time to get buy-in is like taking time to show people a travel brochure and letting people get excited about the journey.
> If the goal is buyin, the extra time it takes is worth the years of positive movement.
> When it comes to change, efficient is not always effective.
> _[Heather Farran's Write-up and Evaluation](https://farran.abwe.org/uploads/9/7/1/2/97128822/primal_leadership.pdf)_

> Historically, RMS has been expressing his views in ways that upset many people.
> He is usually more focused on the philosophical underpinnings, and pursuing the objective truth and linguistic purism, while underemphasising people’s feelings on matters he’s commenting on.
> This makes his arguments vulnerable to misunderstanding and misrepresentation, something which we feel is happening in the open letter calling for his removal.
> His words need to be interpreted in this context and taking into account that more often than not, he is not looking to put things diplomatically.
>
> _[An Open Letter in Support of Richard M. Stallman](https://rms-support-letter.github.io)_
