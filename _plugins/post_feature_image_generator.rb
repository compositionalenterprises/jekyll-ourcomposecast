# Self-written
# Try to create the feature_image front-matter on the fly with some vars
module Jekyll
    class PostFeatureImage < Generator
        safe true
        priority :low
        def generate(site)
            site.posts.docs.each do |item|
                base_url = site.config['url'] + site.baseurl + 'assets/images/'
                episode_index = 'episode-' + item.data['index'].to_s + '-'
                slugified_title = Jekyll::Utils.slugify(item.data['title'])
                item.data['feature_image'] = base_url + episode_index + slugified_title + '.png'
            end
        end
    end
end
