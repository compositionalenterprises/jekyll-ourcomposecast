# From https://stackoverflow.com/questions/26073090/how-to-retrieve-the-current-post-index-number-in-jekyll/26081837#26081837
module Jekyll
    class PostIndex < Generator
        safe true
        priority :highest
        def generate(site)
            site.posts.docs.each_with_index do |item, index|
                item.data['index'] = index + 1
            end
        end
    end
end
