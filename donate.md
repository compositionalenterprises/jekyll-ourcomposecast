---
title: Donate!
indexing: false
sitemap: false
---

If you enjoy the podcast, donate to the show!

Donating to us goes directly back into production, and ensures quality on our end. 
Please note we use <a href="https://compositionalenterprises.memberful.com" class="darkmode-ignore">Memberful</a> to accept donations.

<h2>For one Time Donations:</h2>

<form action="https://compositionalenterprises.memberful.com/checkout" method="get" class="darkmode-ignore">
  <input type="hidden" name="plan" value="53360" />
  <input type="number" name="price" placeholder="Choose what you pay" required="required" min="5.00" step="0.01" />
  <input type="submit" value="Contribute now" />
</form>

<h2>For Monthly Donations:</h2>
<form action="https://compositionalenterprises.memberful.com/checkout" method="get" class="darkmode-ignore">
  <input type="hidden" name="plan" value="53361" />
  <input type="number" name="price" placeholder="Choose what you pay" required="required" min="5.00" step="0.01" />
  <input type="submit" value="Contribute now" />
</form>

