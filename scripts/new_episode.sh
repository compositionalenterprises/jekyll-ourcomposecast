#!/bin/bash

#
# 2020-05-02
# Andrew Cziryak (acziryak@acziryak.com)
# new_episode.sh
#
# This script creates a skeleton of a new episode page.
#

# Set constant vars and initialize prompted vars
SCRIPTS_DIR=$(dirname $0)

# Check OS
# Get the date for this episode
episode_date=''
case "$OSTYPE" in
  solaris*) echo "SOLARIS" ;;
  darwin*) 
    while [[ "${episode_date}" == '' ]]; do
        if [[ "${episode_date}" == '' ]]; then
                read -p "Episode Date (YYYY-MM-DD): " episode_date
            else
                echo -e "\n#\n# ERROR:\n#\tCould not parse ${episode_date}, as an Episode Date please try again...\n#"
                read -p "Episode Date (YYYY-MM-DD): " episode_date
            fi
        done
    ;;
  linux*)
    while [[ "${episode_date}" == '' || ! $(date -d "${episode_date}") ]]; do
        if [[ "${episode_date}" == '' ]]; then
                read -p "Episode Date (YYYY-MM-DD): " episode_date
            else
                echo -e "\n#\n# ERROR:\n#\tCould not parse ${episode_date}, as an Episode Date please try again...\n#"
                read -p "Episode Date (YYYY-MM-DD): " episode_date
            fi
        done
    ;;
  bsd*)     echo "BSD" ;;
  msys*)    echo "WINDOWS" ;;
  *)        echo "unknown: $OSTYPE" ;;
esac

# Get the title for this episode
episode_title='Change Me'
read -p "Episode Title (with correct capitalization and spacing): " episode_title
if [[ "${episode_title}" == '' ]]; then
        echo -e "\n#\n# ERROR:\n#\tEpisode Title cannot be blank, please try again...\n#"
        read -p "Episode Title (with correct capitalization and spacing): " episode_title
fi

# Get the service for this episode
episode_service='Change Me'
read -p "Episode Service (with correct capitalization): " episode_service
if [[ "${episode_service}" == '' ]]; then
        echo -e "\n#\n# ERROR:\n#\tEpisode Service cannot be blank, please try again...\n#"
        read -p "Episode Service (with correct capitalization): " episode_service
fi

# Format the title for creating the filename
episode_filename_title=$(sed 's/[^a-zA-Z0-9]/-/g' <<< "${episode_title}" | tr '[:upper:]' '[:lower:]')

# Format the episode path
episode_path="${SCRIPTS_DIR}/../_posts/${episode_date}-${episode_filename_title}.md"
touch "${episode_path}"
episode_path="$(realpath ${episode_path})"

# Echo the skeleton (including the yaml header into the file in the right directory
cat <<EOF > "$(realpath ${episode_path})"
---
title: ${episode_title}
service: ${episode_service}
tagline: WE'LL FIX IT IN POST
chapters:
  - start: '00:00:00'
    title: 'Intro - News - Updates'
  - start: '00:00:00'
    title: 'Integration Discussion - <Program> - <Section Name>'
  - start: '00:00:00'
    title: 'Grab Bag - <Title>'
---

## Intro

## News / Community Updates

## OurCompose Developments

### Content

### Portal UI/UX

### Administrative

## Integration Discussion - <Program> - <Section Name>

## Book Bag - <Title>
EOF

echo -e "#\n# Created ${episode_path}\n#"
