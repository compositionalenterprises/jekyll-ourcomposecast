#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import argparse
import time
import frontmatter
import subprocess


def create_clips(video, all_timestamps):
    path = os.path.dirname(video) + '/'
    for clip in ['first', 'second', 'third']:
        # Create the first clip
        first_cut_cmd = ['ffmpeg', '-i', video, '-ss']
        first_cut_cmd.append(all_timestamps[clip]['start']['time'])
        first_cut_cmd.append('-to')
        first_cut_cmd.append(all_timestamps[clip]['cut1']['time'])
        first_cut_file = path + clip + '_first.mp4'
        first_cut_cmd.extend(['-c', 'copy', '-y', first_cut_file])
        subprocess.run(first_cut_cmd, check=True)

        # Fade the first clip
        first_fade_cmd = ['ffmpeg', '-i', first_cut_file, '-vf', "fade=t=in:st=0:d=1"]
        first_fade_file = path + clip + '_first_faded.mp4'
        first_fade_cmd.extend(['-c:a', 'copy', '-y', first_fade_file])
        subprocess.run(first_fade_cmd, check=True)

        # Create the second clip
        second_cut_cmd = ['ffmpeg', '-i', video, '-ss']
        second_cut_cmd.append(all_timestamps[clip]['cut1']['time'])
        second_cut_cmd.append('-to')
        second_cut_cmd.append(all_timestamps[clip]['cut2']['time'])
        second_cut_file = path + clip + '_second.mp4'
        second_cut_cmd.extend(['-c', 'copy', '-y', second_cut_file])
        subprocess.run(second_cut_cmd, check=True)

        # Create the third clip
        third_cut_cmd = ['ffmpeg', '-i', video, '-ss']
        third_cut_cmd.append(all_timestamps[clip]['cut2']['time'])
        third_cut_cmd.append('-to')
        third_cut_cmd.append(all_timestamps[clip]['end']['time'])
        third_cut_file = path + clip + '_third.mp4'
        third_cut_cmd.extend(['-c', 'copy', '-y', third_cut_file])
        subprocess.run(third_cut_cmd, check=True)

        # Fade the third clip
        third_fade_cmd = ['ffmpeg', '-i', third_cut_file, '-vf', "fade=t=out:st=4:d=1"]
        third_fade_file = path + clip + '_third_faded.mp4'
        third_fade_cmd.extend(['-c:a', 'copy', '-y', third_fade_file])
        subprocess.run(third_fade_cmd, check=True)

        # Write the batch file
        vidlist = path + clip + 'list.txt'
        with open(vidlist, 'w') as l:
            for ending in ['first_faded', 'second', 'third_faded']:
                l.write(f"file {clip}_{ending}.mp4\n")

        # Combine the three clips
        concat_cmd = ['ffmpeg', '-f', 'concat', '-i', vidlist, '-c', 'copy']
        concat_cmd.extend(['-y', path + clip + '_final.mp4'])
        subprocess.run(concat_cmd, check=True, cwd=path)
            

def sf(timef):
    hours = timef.split(":")[0]
    minutes = timef.split(":")[1]
    seconds = timef.split(":")[2]

    sfhours = int(hours) * 60 * 60
    sfminutes = int(minutes) * 60

    sftotal = sfhours + sfminutes + int(seconds)

    return sftotal


def get_cut_time(ctime, secs):
    cut_secs = sf(ctime) + secs
    cut_time = time.strftime("%H:%M:%S", time.gmtime(cut_secs))

    return {'time': cut_time, 'secs': str(cut_secs)}


def get_all_timestamps(post_timestamps, video_length):

    all_timestamps = {}
    cuts = [
        {
            'k': 'first',
            't1': '00:00:00',
            't2': post_timestamps['int']
        },
        {
            'k': 'second',
            't1': post_timestamps['int'],
            't2': post_timestamps['bag'],
        },
        {
            'k': 'third',
            't1': post_timestamps['bag'],
            't2': video_length
        }
    ]
    for c in cuts:
        all_timestamps[c['k']] = {}
        all_timestamps[c['k']]['start'] = {}
        all_timestamps[c['k']]['start']['time'] = c['t1']
        all_timestamps[c['k']]['start']['secs'] = str(sf(c['t1']))
        all_timestamps[c['k']]['cut1'] = get_cut_time(c['t1'], 5)
        all_timestamps[c['k']]['cut2'] = get_cut_time(c['t2'], -5)
        all_timestamps[c['k']]['end'] = {}
        all_timestamps[c['k']]['end']['time'] = c['t2']
        all_timestamps[c['k']]['end']['secs'] = str(sf(c['t2']))

    return all_timestamps


def get_video_length(video):
    mediainfo_cmd1 = 'mediainfo --Inform="Video;%Duration/String3%" '
    mediainfo_cmd2 = f"{video} | cut -d '.' -f 1"
    video_length = subprocess.run(mediainfo_cmd1 + mediainfo_cmd2, shell=True,
                                  check=True, capture_output=True, text=True)

    return video_length.stdout.strip()


def get_post_timestamps(post):
    post_frontmatter = frontmatter.load(post)
    post_timestamps = {}
    for chapter in post_frontmatter['chapters']:
        if 'Integration Discussion' in chapter['title']:
            post_timestamps['int'] = chapter['start']
        elif 'Grab Bag' in chapter['title']:
            post_timestamps['bag'] = chapter['start']

    return post_timestamps


def parse_args():
    """Parse the passed in arguments"""
    parser = argparse.ArgumentParser(description="Splits up an episode video")
    parser.add_argument('-p', '--post',
                        help='The full path to the post',
                        required=False)
    parser.add_argument('-v', '--video',
                        help='The full path to the video',
                        required=False)

    args = vars(parser.parse_args())

    while not args['post'] or not os.path.exists(args['post']):
        args['post'] = input("Full path to the post: ")
    while not args['video'] or not os.path.exists(args['video']):
        args['video'] = input("Full path to the video: ")

    return args


def main():
    # Get the args
    args = parse_args()

    post_timestamps = get_post_timestamps(args['post'])
    video_length = get_video_length(args['video'])
    all_timestamps = get_all_timestamps(post_timestamps, video_length)
    create_clips(args['video'], all_timestamps)

if __name__ == '__main__':
    #
    # Handle ^C without throwing an exception
    #
    try:
        main()
    except KeyboardInterrupt:
        raise SystemExit
